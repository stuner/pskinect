	package com.ifh.magnetometerapp.api14.Background;

public class ListItem {

	public int icon;
    public String title;
    public String path;
    public ListItem(){
        super();
    }
    
    public ListItem(int icon, String title,String p) {
        super();
        this.icon = icon;
        this.title = title;
        this.path =p;
    }
}

