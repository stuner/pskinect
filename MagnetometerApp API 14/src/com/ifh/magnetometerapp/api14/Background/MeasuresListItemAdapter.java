package com.ifh.magnetometerapp.api14.Background;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.ifh.magnetometerapp.api14.R;

import java.util.Date;

public class MeasuresListItemAdapter extends ArrayAdapter<MeasureListItem>{

    Context context; 
    int layoutResourceId;    
    MeasureListItem data[] = null;
    
    public MeasuresListItemAdapter(Context context, int layoutResourceId, MeasureListItem[] data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        Holder holder = null;
        
	    
	    
        
        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            
            holder = new Holder();
            holder.date = (TextView)row.findViewById(R.id.measureitem_date);
            holder.details = (TextView)row.findViewById(R.id.measureitem_details);
            
            row.setTag(holder);
        }
        else
        {
            holder = (Holder)row.getTag();
        }
        
        MeasureListItem item = data[position];
        
        
        holder.details.setText("Mode: "+item.mode+" , "+item.numOfRows+" Entries");
        Date date=new Date();
        date.setTime(item.date);
        holder.date.setText(date.toGMTString());
        
        return row;
    }
    
    static class Holder
    {
        TextView date;
        TextView details;
    }
}
