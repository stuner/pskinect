package com.ifh.magnetometerapp.api14.Background;

import android.app.Activity;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

public class TopExceptionHandler implements Thread.UncaughtExceptionHandler {

	private Thread.UncaughtExceptionHandler defaultUEH;

	private Activity app = null;

	private String extStoragePath;
	private File directory;

	public TopExceptionHandler(Activity app) {
		this.defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
		this.app = app;
	}

	public void uncaughtException(Thread t, Throwable e) {
		
		Log.v("philipba","unhandled Exception");
		
		StackTraceElement[] arr = e.getStackTrace();
		String report = e.toString() + "\n\n";
		report += "--------- Stack trace ---------\n\n";
		for (int i = 0; i < arr.length; i++) {
			report += "    " + arr[i].toString() + "\n";
		}
		report += "-------------------------------\n\n";

		// If the exception was thrown in a background thread inside
		// AsyncTask, then the actual exception can be found with getCause
		report += "--------- Cause ---------\n\n";
		Throwable cause = e.getCause();
		if (cause != null) {
			report += cause.toString() + "\n\n";
			arr = cause.getStackTrace();
			for (int i = 0; i < arr.length; i++) {
				report += "    " + arr[i].toString() + "\n";
			}
		}
		report += "-------------------------------\n\n";

		extStoragePath = Environment.getExternalStorageDirectory()
				.getAbsolutePath();

		directory = new File(Environment
				.getExternalStorageDirectory().toString() + "/Magnetometer/");
		boolean test = directory.mkdirs();

		Date date = new Date();

		String fileName = "/Stacktrace_" + date.getDate() + "_"
				+ (date.getMonth()+1) + "_" + (date.getYear()+1900) + date.getHours()
				+ "_" + date.getMinutes() + "_" + date.getSeconds() + ".txt";

		try {

			File file = new File(directory.getAbsolutePath() + fileName);
			file.createNewFile();
			BufferedWriter buf = new BufferedWriter(new FileWriter(file, true));
			buf.append(report);
			buf.newLine();
			buf.close();
		} catch (IOException ioe) {
			Log.v("philipba", "file not created");
			ioe.printStackTrace();
		}
		
		Log.v("philipba","file saved");

		defaultUEH.uncaughtException(t, e);
	}
}
