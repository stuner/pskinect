package com.ifh.magnetometerapp.api14.Background;

public class DataPackage {

	public String date;
	public String time;
	public String axis;
	public double valCal;
	public double valNotCal;
	public String gps;
	public int id;
	
	public DataPackage(String _date,String _time,String _axis, double _valCal,double _valNotCal, String _gps, int _id)
	{
		date=_date;
		time=_time;
		valCal=_valCal;
		valNotCal=_valNotCal;
		id=_id;		
		gps=_gps;
		axis=_axis;
	}

	public DataPackage()
	{
		date="";
		time="";
		valCal=0;
		gps="";
		id=0;	
		valNotCal=0;
		axis="";
	}
	
	
}
