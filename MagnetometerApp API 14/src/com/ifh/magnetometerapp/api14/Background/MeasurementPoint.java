package com.ifh.magnetometerapp.api14.Background;

import android.graphics.Paint;

public class MeasurementPoint {
	// Public Data:
	// The position
	public double latitude;
	public double longitude;
	public double altitude;
	public double accuracy;
	public long time;
	public double magneticFieldX;
	public double magneticFieldY;
	public double magneticFieldZ;
	public double magneticFieldS;
	public Paint color;

	public MeasurementPoint(double longi_, double latit_, double altitude_,
			double accuracy_, long time_) {
		longitude = longi_;
		latitude = latit_;
		altitude = altitude_;
		accuracy = accuracy_;
		time = time_;
	}

	// Return the longitude.
	public double getLongitude() {
		return longitude;
	}

	// Return the latitude.
	public double getLatitude() {
		return latitude;
	}

	// Return the latitude.
	public double getAccuracy() {
		return accuracy;
	}

	// Return the x component of magnetic field.
	public double getMagneticFieldX() {
		return magneticFieldX;
	}

	// Return the y component of magnetic field.
	public double getMagneticFieldY() {
		return magneticFieldY;
	}

	// Return the z component of magnetic field.
	public double getMagneticFieldZ() {
		return magneticFieldZ;
	}

	// Return the total magnetic field.
	public double getMagneticFieldS() {
		return magneticFieldS;
	}

	// Set the x component of the magnetic field.
	public void setMagneticFieldX(double magneticFieldX_) {
		magneticFieldX = magneticFieldX_;
	}

	// Set the y component of the magnetic field.
	public void setMagneticFieldY(double magneticFieldY_) {
		magneticFieldY = magneticFieldY_;
	}

	// Set the z component of the magnetic field.
	public void setMagneticFieldZ(double magneticFieldZ_) {
		magneticFieldZ = magneticFieldZ_;
		magneticFieldS = (float) Math.sqrt(magneticFieldX * magneticFieldX
				+ magneticFieldY * magneticFieldY + magneticFieldZ
				* magneticFieldZ);
	}

}