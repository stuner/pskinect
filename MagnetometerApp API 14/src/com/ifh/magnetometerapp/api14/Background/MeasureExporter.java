package com.ifh.magnetometerapp.api14.Background;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.util.Xml;
import android.widget.Toast;
import org.xmlpull.v1.XmlSerializer;

import java.io.*;

public class MeasureExporter {
    private String XML_EXPORT_FILE_NAME = "";
    private String CSV_EXPORT_FILE_NAME = "";

    private Context _ctx;
    private DatabaseAdapter dbAdapter;
    private SQLiteDatabase db;
    XmlSerializer serializer;
    FileOutputStream foutXML;
    FileWriter foutCSV;
    BufferedWriter foutCSVbuf;

    public MeasureExporter(Context ctx, DatabaseAdapter adapter, String tableName) {
        _ctx = ctx;
        dbAdapter = adapter;
        File exportsDirectory = new File(BackgroundService.EXPORTS_PATH);
        exportsDirectory.mkdirs();
        XML_EXPORT_FILE_NAME = BackgroundService.EXPORTS_PATH + tableName + ".xml";
        CSV_EXPORT_FILE_NAME = BackgroundService.EXPORTS_PATH + tableName + ".csv";

    }

    public void exportTableToXML(String tableName) throws IOException {
        try {

            File xmlFile = new File(XML_EXPORT_FILE_NAME);
            xmlFile.createNewFile();

            foutXML = new FileOutputStream(xmlFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        serializer = Xml.newSerializer();
        dbAdapter.open();
        db = dbAdapter.getDb();
        String sql = "select * from " + tableName;
        Cursor cur = db.rawQuery(sql, new String[0]);
        int numcols = cur.getColumnCount();

        cur.moveToFirst();

        serializer.setOutput(foutXML, "UTF-8");
        serializer.startDocument(null, Boolean.valueOf(true));
        serializer.setFeature(
                "http://xmlpull.org/v1/doc/features.html#indent-output", true);

        serializer.processingInstruction("xml-stylesheet type='text/css' href='layout.css'");

        serializer.startTag(null, "table");

        serializer.startTag(null, "title");
        serializer.text(tableName);
        serializer.endTag(null, "title");

        serializer.startTag(null, "headings");
        serializer.startTag(null, "row");
        String title;
        for (int idx = 0; idx < numcols; idx++) {
            title = cur.getColumnName(idx);
            serializer.startTag(null, "col");
            serializer.text(title);
            serializer.endTag(null, "col");
        }
        serializer.endTag(null, "row");
        serializer.endTag(null, "headings");

        serializer.startTag(null, "values");
        while (cur.getPosition() < cur.getCount()) {
            serializer.startTag(null, "row");
            String val;
            for (int idx = 0; idx < numcols; idx++) {
                val = cur.getString(idx);

                serializer.startTag(null, "col");
                serializer.text(val);
                serializer.endTag(null, "col");
            }
            serializer.endTag(null, "row");
            cur.moveToNext();
        }
        serializer.endTag(null, "values");
        serializer.endTag(null, "table");
        serializer.endDocument();
        serializer.flush();
        foutXML.close();
        cur.close();
        dbAdapter.close();
        Toast.makeText(_ctx,
                "'" + tableName + "' succesfully exported to: " + XML_EXPORT_FILE_NAME,
                Toast.LENGTH_LONG).show();
    }

    public void exportTableToCSV(String tableName) throws IOException {
        try {
            // create a file on the sdcard to export the
            // database contents to

            File csvFile = new File(CSV_EXPORT_FILE_NAME);
            csvFile.createNewFile();

            foutCSV = new FileWriter(csvFile);
            foutCSVbuf = new BufferedWriter(foutCSV);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        dbAdapter.open();
        db = dbAdapter.getDb();
        String sql = "select * from " + tableName;
        Cursor cur = db.rawQuery(sql, new String[0]);
        int numcols = cur.getColumnCount();

        cur.moveToFirst();

        //Write col titles
        String title;
        for (int idx = 0; idx < numcols; idx++) {
            title = cur.getColumnName(idx);
            foutCSVbuf.write(title);
            foutCSVbuf.write(",");

        }
        Log.v("philipba", "testtest");

        foutCSVbuf.write("\n");

        while (cur.getPosition() < cur.getCount()) {

            String val;
            for (int idx = 0; idx < numcols; idx++) {
                val = cur.getString(idx);
                foutCSVbuf.write(val);
                foutCSVbuf.write(",");
            }
            foutCSVbuf.write("\n");
            cur.moveToNext();
        }
        foutCSVbuf.close();
        foutCSV.close();
        cur.close();
        dbAdapter.close();
        Toast.makeText(_ctx,
                "'" + tableName + "' succesfully exported to: " + CSV_EXPORT_FILE_NAME,
                Toast.LENGTH_SHORT).show();
    }

}