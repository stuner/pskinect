package com.ifh.magnetometerapp.api14.Background;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ifh.magnetometerapp.api14.R;

public class ListItemAdapter extends ArrayAdapter<ListItem>{

    Context context; 
    int layoutResourceId;    
    ListItem data[] = null;
    
    public ListItemAdapter(Context context, int layoutResourceId, ListItem[] data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        Holder holder = null;
        
        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            
            holder = new Holder();
            holder.imgIcon = (ImageView)row.findViewById(R.id.listitem_image);
            holder.txtTitle = (TextView)row.findViewById(R.id.listitem_text);
            
            row.setTag(holder);
        }
        else
        {
            holder = (Holder)row.getTag();
        }
        
        ListItem weather = data[position];
        holder.txtTitle.setText(weather.title);
        holder.imgIcon.setImageResource(weather.icon);
        
        return row;
    }
    
    static class Holder
    {
        ImageView imgIcon;
        TextView txtTitle;
    }
}
