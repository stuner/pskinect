package com.ifh.magnetometerapp.api14.Background;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;
import com.ifh.magnetometerapp.api14.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

/**
 * This class does all the work for importing data of the calibration file
 * stored on SD-card. If a Bluetooth connection is set up, the number of the
 * device is extracted of the device name. For each device there is a matrix
 * stored in a ArrayList.
 */
public class CalibrationImport {

    private Context context;
    private int deviceNumb = 0;

    // Stores all the imported Data
    private ArrayList<ArrayList<Double>> ct = new ArrayList<ArrayList<Double>>();

    /**
     * Constructor. Prepares data import from SD-card
     *
     * @param con          Context of Activity which calls this class
     * @param deviceNumber Number of device which is connected to the phone via bluetooth
     */
    public CalibrationImport(Context con, int deviceNumber) {
        context = con;
        deviceNumb = deviceNumber;
        readTextFile();
    }

    /**
     * Opens text file and imports data of given device.
     */
    public void readTextFile() {
        Log.d("IFH Magnetometer", "Read and parse calibration file: start.");

        // Flag that indicates, that the used device has an entry in the 
        // calibration file.
        boolean deviceNumberFound = false;

        File sdcard = Environment.getExternalStorageDirectory();
        File file = new File(sdcard,
                context.getString(R.string.calibration_file));

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            // Regex pattern to extract the device number from a line in the calibration file. 
            String devicePattern = "^DEVICE(\\s+)\"(EWZ|PL|MZ|FS)(\\s+)(\\d+)\"";

            while ((line = br.readLine()) != null) {
                // Check if the line is the head of a dataset ...
                if (line.matches(devicePattern)) {
                    // ... if yes, check if the number in the line equals to the connected device nr. 
                    String deviceNrString = line.replaceAll(devicePattern, "$4");

                    if (Integer.parseInt(deviceNrString) == deviceNumb) {
                        // Flag that we found the used device; 
                        deviceNumberFound = true;

                        if ((line = br.readLine()) != null) {
                            for (int i = 0; i < 3; i++) {
                                ArrayList<Double> temp = new ArrayList<Double>();

                                if (line.equalsIgnoreCase("CHANNEL " + i)) {
                                    line = br.readLine();
                                    temp.add(Double.parseDouble(line));
                                    line = br.readLine();
                                    String[] temp2 = line.split(" ");
                                    for (int l = 0; l < 3; l++) {
                                        temp.add(Double.parseDouble(temp2[l]));
                                    }
                                    ct.add(temp);
                                    if ((line = br.readLine()) == null) {
                                        break;
                                    }
                                }
                            }
                            break;
                        } else {
                            break;
                        }
                    }
                }
            }
            br.close();
        } catch (Exception e) {
            Toast.makeText(context, "Calibration file not found!",
                    Toast.LENGTH_LONG).show();
        }

        // Send toast, if device has no entry in the calibration file.
        if (!deviceNumberFound) {
            Toast.makeText(context, "The connected Magnetometer has no entry in the calibration file.",
                    Toast.LENGTH_LONG).show();

        }
        Log.d("IFH Magnetometer", "Read and parse calibration file: stop.");
    }

    public ArrayList<Double> getChannel(int channel) {
        return ct.get(channel);
    }

    /**
     * @return Size of ArrayList with all the imported Data.
     */
    public int getSize() {
        return ct.size();
    }
}
