/*
 * Copyright (C) 2008 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.ifh.magnetometerapp.api14.Background;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

public class DatabaseAdapter {

    // Diary Table
    public static final String KEY_DATE = "date";
    public static final String KEY_TIME = "time";
    public static final String KEY_VALCAL = "valCal";
    public static final String KEY_VALNOTCAL = "valNotCal";
    public static final String KEY_AXIS = "axis";
    public static final String KEY_ROWID = "_id";
    public static final String KEY_GPS = "gps";

    private static final String TAG = "NotesDbAdapter";
    private DatabaseHelper mDbHelper;
    private SQLiteDatabase mDb;

    /**
     * Database creation sql statement
     */
    private static final String DATABASE_NAME = "database";
    private static final String TABLE = "table";
    private static final int DATABASE_VERSION = 1;

    private static final String TABLE_CREATE = " (_id integer primary key autoincrement, date text not null,"
            + "time text not null,axis test not null, valCal real not null,valNotCal real not null, gps text not null);";

    private final Context mCtx;

    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            onCreate(db);
        }
    }

    /**
     * Constructor - takes the context to allow the database to be
     * opened/created
     *
     * @param ctx the Context within which to work
     */
    public DatabaseAdapter(Context ctx) {

        this.mCtx = ctx;
    }

    /**
     * Open the notes database. If it cannot be opened, try to create a new
     * instance of the database. If it cannot be created, throw an exception to
     * signal the failure
     *
     * @return this (self reference, allowing this to be chained in an
     *         initialization call)
     * @throws SQLException if the database could be neither opened or created
     */
    public DatabaseAdapter open() throws SQLException {
        mDbHelper = new DatabaseHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        mDbHelper.close();
    }

    public void createEntry(DataPackage pack, String table) {


        ContentValues values = new ContentValues();
        values.put(KEY_DATE, pack.date);
        values.put(KEY_TIME, pack.time);
        values.put(KEY_AXIS, pack.axis);
        values.put(KEY_VALCAL, pack.valCal);
        values.put(KEY_VALNOTCAL, pack.valNotCal);
        values.put(KEY_GPS, pack.gps);


        mDb.insertOrThrow(table, null, values);


    }

    public void deleteAllTables(Context context) {
        this.deleteDatabase(context);
    }

    public void deleteTable(String tableName) {
        mDb.execSQL("DROP TABLE IF EXISTS " + tableName);
    }

    public boolean deleteEntry(long rowId, String table) {

        return mDb.delete(table, KEY_ROWID + "=" + rowId, null) > 0;
    }

    public void createTable(String name) {
        try {
            this.open();
            mDb.execSQL("create table " + name + TABLE_CREATE);
            this.close();
        } catch (Exception e) {
            Toast.makeText(this.mCtx, "Invalid table name " + name, Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    public Cursor fetchAllEntries(String tableName) {

        return mDb.query(tableName, new String[]{KEY_ROWID, KEY_DATE,
                KEY_TIME, KEY_AXIS, KEY_VALCAL, KEY_VALNOTCAL, KEY_GPS}, null, null, null, null, null);
    }

    public ArrayList<DataPackage> getAllEntries(String tableName) {
        mDb.beginTransaction();
        ArrayList<DataPackage> data = new ArrayList<DataPackage>();
        try {

            Cursor c = fetchAllEntries(tableName);

            c.moveToFirst();

            if (c.getCount() != 0) {

                while (!c.isAfterLast()) {
                    data.add(new DataPackage(c.getString(1), c
                            .getString(2), c.getString(3), c.getDouble(4), c.getDouble(5), c
                            .getString(6), c.getInt(0)));
                    c.moveToNext();
                }
                c.close();
            } else {
                c.close();
                data = null;
            }
        } catch (SQLException e) {
            data = null;
        }

        mDb.endTransaction();
        return data;

    }

    public SQLiteDatabase getDb() {
        return mDb;
    }

    public String[] getTableNames() {
        String[] result = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT name FROM sqlite_master ");
            sb.append("WHERE type IN ('table','view') AND name NOT LIKE 'sqlite_%' ");
            sb.append("UNION ALL ");
            sb.append("SELECT name FROM sqlite_temp_master ");
            sb.append("WHERE type IN ('table','view') ");
            sb.append("ORDER BY 1");

            Cursor c = mDb.rawQuery(sb.toString(), null);
            c.moveToFirst();

            result = new String[c.getCount() - 1];
            int i = 0;
            while (c.moveToNext()) {
                result[i] = c.getString(c.getColumnIndex("name"));
                i++;
            }
            c.close();
        } catch (SQLiteException e) {
            e.getMessage();
        }
        return result;
    }

    public boolean deleteDatabase(Context context) {
        // this.mDb.delete(DATABASE_NAME, null, null);
        return context.deleteDatabase(DATABASE_NAME);
    }
}
