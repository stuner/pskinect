package com.ifh.magnetometerapp.api14.Background;

public class MeasureListItem {

    public String tableName;
    public long date;
    public int numOfRows;
    public String mode;

    public MeasureListItem() {
        super();
    }

    public MeasureListItem(String name, int nOfRows) {
        super();
        tableName = name;
        mode = name.split("_")[1];
        date = Long.parseLong(name.split("_")[2]);
        numOfRows = nOfRows;
    }
}

