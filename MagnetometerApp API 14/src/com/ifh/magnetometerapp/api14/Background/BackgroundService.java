/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ifh.magnetometerapp.api14.Background;

/* All deviations from the original example have been marked [MM].
 * Everything concerning sensor data analysis is done in AnalyzeData.java
 * which alters the received stream before it is displayed by this application
 */

import android.app.ActivityManager;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.*;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;
import com.androidnatic.maps.model.HeatPoint;
import com.ifh.magnetometerapp.api14.Activities.Settings;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This is the main Activity that displays the current chat session.
 */
public class BackgroundService extends Service {

	public static final String MY_SERVICE = "philipba.backgroundservice";

	// Activity Manager to check which activity is in the foreground
	private ActivityManager activityManager;
	private String foregroundActivity = "";

	// Request ID's
	public static final int REQUEST_CONNECT_DEVICE = 1;
	public static final int REQUEST_ENABLE_BT = 2;

	// Messages from BluetoothService
	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;
	public static final int MESSAGE_WRITE = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_TOAST = 5;
	public static final String DEVICE_NAME = "device_name"; // Key names

	// Broadcast strings
	public static final String TOAST = "toast";
	public static final String OFFSETCAL_BROADCAST = "offsetcal_bc";
	public static final String BTCONNECTION_BROADCAST = "btconnection_bc";
	public static final String PLOTUPDATE_BROADCAST = "plotupdate_bc";
	public static final String PLOTUPDATE_VALUES = "plotupdate_values";
	public static final String PLOTUPDATE_AXIS = "plotupdate_axis";
	public static final String MAPVIEW_BROADCAST = "mapview_bc";
	public static final String MAPVIEW_VALUES = "mapview_values";
	public static final String MAPVIEW_AXIS = "mapview_axis";
	public static final String FFT_BROADCAST = "fft_bc";
	public static final String FFT_VALUES = "fft_values";
	public static final String FFT_FREQUENCY = "fft_frequency";
	public static final String FFT_AXIS = "fft_axis";
	public static final String WAVEFORM_BROADCAST = "waveform_bc";
	public static final String WAVEFORM_VALUES = "waveform_values";
	public static final String WAVEFORM_AXIS = "waveform_axis";

	// Microcontroller-Board values
	public static final int U_CONTROLLER_CPU_FREQ = 8000000;
	public static final int U_CONTROLLER_TIMER0_PRESCALER = 256;
	private static final int U_CONTROLLER_DEFAULT_BUTST_SAMPLING_RATE = 25;
	public int uControllerBurstSamplingRate = U_CONTROLLER_DEFAULT_BUTST_SAMPLING_RATE;
	public static final int GAIN_DEFAULT = 13;
	public int gain = GAIN_DEFAULT;
	public double samplingFrequency;

	// Bluetooth instances
	public int connectionStatus = 0; // 0=not connected ,
										// 1=connecting,2=connected
	public String mConnectedDeviceName = null;
	private BluetoothAdapter mBluetoothAdapter = null;
	private BluetoothService mChatService = null;

	// Database and Export instances
	public DatabaseAdapter dbAdapter;
	private static final String ROOT_PATH = Environment
			.getExternalStorageDirectory().toString() + "/Magnetometer/";
	public static final String SCREENSHOTS_PATH = ROOT_PATH + "Screenshots/";
	public static final String EXPORTS_PATH = ROOT_PATH + "Exports/";
	private List<ActivityManager.RunningTaskInfo> taskInfo;
	public boolean measureRunning = false;
	private String actualTableNameTrigger = "";
	private String actualTableNameContinous = "";
	private CalibrationImport calImport;
	private boolean offsetCal = false;
	public boolean offsetCalibrationData = false;
	private boolean temp = false;

	// Data from MC-Board
	private int bt_dataPackNo;
	private String btDate;
	private String btTime;
	private char btSensorConfig;
	private int btGain;
	private int[] bt_position = new int[3];
	private int btData0size;
	private double btData0value;
	private double[] data0list;
	private int btData1size;
	private int btData1value;
	private int btChecksum;
	public double[] values, valuesX, valuesY, valuesZ;
	public double offsetX = 0, offsetY = 0, offsetZ = 0;
	private double[] valuesFFT, valuesFFTX, valuesFFTY, valuesFFTZ;
	private double[] freqValues, freqValuesX, freqValuesY, freqValuesZ;

	// Other
	private SharedPreferences prefs;
	public ArrayList<HeatPoint> mapList;
	private final IBinder binder = new LocalBinder();
	private PowerManager pwManager;
	private PowerManager.WakeLock wakeLock;
	private PowerManager.WakeLock screenDim;

	// LifeCycle
	@Override
	public void onCreate() {
		super.onCreate();

		Log.v("philipba", "service created");
		dbAdapter = new DatabaseAdapter(this);

		pwManager = (PowerManager) getApplicationContext().getSystemService(
				Context.POWER_SERVICE);
		wakeLock = pwManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
				"MagnetometerWakeLock");
		screenDim = pwManager.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK,
				"MagnetometerScreenDim");

		samplingFrequency = U_CONTROLLER_CPU_FREQ
				/ U_CONTROLLER_TIMER0_PRESCALER
				/ (uControllerBurstSamplingRate + 1.0);

		// Get shared preferences (f.e. calibration status)
		prefs = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		activityManager = (ActivityManager) this
				.getSystemService(ACTIVITY_SERVICE);

		setWakeLock(prefs.getBoolean(Settings.WAKELOCK_KEY, false));

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (measureRunning) {
			stopMeasurement();
		}

		// Stop the Bluetooth chat services
		if (mChatService != null) {
			mChatService.stop();
			Log.v("philipba", "bluetoothservice destroyed");
		}
		Log.v("philipba", "service destroyed");
	}

	public void setWakeLock(boolean value) {
		if (value) {
			if (!wakeLock.isHeld()) {

				wakeLock.acquire();
			}
			if (!screenDim.isHeld()) {
				screenDim.acquire();
			}
			Log.v("philipba", "wake lock on");

		} else {
			if (wakeLock.isHeld()) {
				wakeLock.release();
			}
			if (screenDim.isHeld()) {
				screenDim.release();
			}
			Log.v("philipba", "wake lock off");
		}
	}

	// Binding

	public class LocalBinder extends Binder {
		public BackgroundService getService() {
			return BackgroundService.this;
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return binder;
	}

	@Override
	public boolean onUnbind(Intent intent) {
		return super.onUnbind(intent);
	}

	// The Handler that gets information back from the BluetoothChatService
	private final Handler mHandler = new Handler() {
		@Override
		public void handleMessage(final Message msg) {
			switch (msg.what) {
			case MESSAGE_STATE_CHANGE:
				switch (msg.arg1) {
				case BluetoothService.STATE_CONNECTED:
					connectionStatus = 2;
					btConnectionStatusBroadcast();
					getCalibrationData();
					writetoLogFile("Connected to " + mConnectedDeviceName);
					break;
				case BluetoothService.STATE_CONNECTING:
					connectionStatus = 1;
					btConnectionStatusBroadcast();
					break;
				case BluetoothService.STATE_LISTEN:
				case BluetoothService.STATE_NONE:
					connectionStatus = 0;
					btConnectionStatusBroadcast();
					writetoLogFile("Disconnected from " + mConnectedDeviceName);
					break;
				}
				break;
			case MESSAGE_WRITE:
				break;
			case MESSAGE_READ:
				int[] readBuf = (int[]) msg.obj;
				analyzeData(readBuf.clone());

				// Get Running activities and extract foreground activity
				taskInfo = activityManager.getRunningTasks(1);
				foregroundActivity = taskInfo.get(0).topActivity.getClassName();

				Date d = new Date();
				if ((d.getMinutes() % 2) == 0 && !temp) {
					writetoLogFile("Receiving Data");
					temp = true;
				} else {
					temp = false;
				}

				// Only send specific broadcast to current running activity
				if (foregroundActivity.contains("LiveView")) {
					updatePlotBroadcast(btData0value, btSensorConfig);

				} else if (foregroundActivity.contains("SpectralFilter")
						|| foregroundActivity.contains("SpectralView")
						|| foregroundActivity.contains("MapsView")
						|| foregroundActivity.contains("WaveformAnalysisView")) {
					editBurst(data0list, btSensorConfig);
					if (foregroundActivity.contains("SpectralFilter")
							|| foregroundActivity.contains("SpectralView")
							|| foregroundActivity.contains("MapsView")) {
						updateFFTViewBroadcast(btSensorConfig);
					} else if (foregroundActivity
							.contains("WaveformAnalysisView")) {
						updateWaveformBroadcast(btSensorConfig);
					}
				}

				if (offsetCal) {
					editBurst(data0list, btSensorConfig);
				}
				break;
			case MESSAGE_DEVICE_NAME:
				// save the connected device's name
				mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
				Toast.makeText(getApplicationContext(),
						"Connected to " + mConnectedDeviceName,
						Toast.LENGTH_SHORT).show();
				break;
			case MESSAGE_TOAST:
				Toast.makeText(getApplicationContext(),
						msg.getData().getString(TOAST), Toast.LENGTH_SHORT)
						.show();
				break;
			}
		}
	};

	// Connect to Device

	public void connectToDevice(String address) {

		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		// Set the Handler to the BluetoothChatService
		mChatService = new BluetoothService(this, mHandler);

		// Get the BLuetoothDevice object
		BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);

		// Attempt to connect to the device
		mChatService.connect(device);

	}

	// Set up all data required to apply a calibration.
	private void getCalibrationData() {
		// Get device number of the connected device
		// Log.v("Patrick BackgroundService","read calib start");
		String[] split = mConnectedDeviceName.split(" ");
		int deviceNumber = Integer.parseInt(split[split.length - 1]);

		// Import calibration data of the connected device
		calImport = new CalibrationImport(this, deviceNumber);
		// Log.v("Patrick BackgroundService","read calib stop");

	}

	// Broadcasts
	public void stopBluetooth() {
		if (mChatService != null) {
			mChatService.stop();
			Log.v("philipba", "bluetoothservice destroyed");
		}
	}

	private void btConnectionStatusBroadcast() {
		Intent broadcast = new Intent();
		broadcast.setAction(BTCONNECTION_BROADCAST);
		sendBroadcast(broadcast);
	}

	private void updatePlotBroadcast(double value, char axis) {
		// Save data to database if measure is running
		if (measureRunning) {
			DataPackage pack = new DataPackage(btDate, btTime, "" + axis,
					value, 0, "no data", 0);

			dbAdapter.createEntry(pack, actualTableNameContinous);
		}
		Intent broadcast = new Intent();
		broadcast.setAction(PLOTUPDATE_BROADCAST);
		broadcast.putExtra(PLOTUPDATE_VALUES, value);
		broadcast.putExtra(PLOTUPDATE_AXIS, axis);
		sendBroadcast(broadcast);
	}

	private void updateFFTViewBroadcast(char axis) {
		Intent broadcast = new Intent();
		// Apply calibrationmagnField.length;

		broadcast.setAction(FFT_BROADCAST);
		broadcast.putExtra(FFT_VALUES, valuesFFT);
		broadcast.putExtra(FFT_FREQUENCY, freqValues);
		broadcast.putExtra(FFT_AXIS, axis);

		sendBroadcast(broadcast);
	}

	private void updateWaveformBroadcast(char axis) {

		if (measureRunning) {
			for (double v : values) {
				DataPackage pack = new DataPackage(btDate, btTime, "" + axis,
						v, 0, "", 0);
				dbAdapter.createEntry(pack, actualTableNameTrigger);
			}

		}
		Intent broadcast = new Intent();
		broadcast.setAction(WAVEFORM_BROADCAST);
		broadcast.putExtra(WAVEFORM_VALUES, values);
		broadcast.putExtra(WAVEFORM_AXIS, axis);
		sendBroadcast(broadcast);
	}

	// Analyze Data received from MC-Board via Bluetooth
	private void analyzeData(int[] dataBuffer) {

		try {
			int posCount = 0;
			bt_dataPackNo = dataBuffer[posCount++] * 256
					+ dataBuffer[posCount++];

			String formatTwoDigitsLeadingZero = String.format("%%02d");
			btDate = dataBuffer[posCount++]
					+ "/"
					+ String.format(formatTwoDigitsLeadingZero,
							dataBuffer[posCount++])
					+ "/"
					+ String.format(formatTwoDigitsLeadingZero,
							dataBuffer[posCount++]);

			String hour = Integer.toString(dataBuffer[posCount++]);
			if (hour.length() == 1) {
				hour = "0" + hour;
			}
			String minute = Integer.toString(dataBuffer[posCount++]);
			if (minute.length() == 1) {
				minute = "0" + minute;
			}
			String second = Integer.toString(dataBuffer[posCount++]);
			if (second.length() == 1) {
				second = "0" + second;
			}
			String millisecond = Integer.toString(dataBuffer[posCount++]);
			if (millisecond.length() == 1) {
				millisecond = "00" + millisecond;
			} else if (millisecond.length() == 2) {
				millisecond = "0" + millisecond;
			}

			btTime = "" + hour + ":" + minute + ":" + second + "."
					+ millisecond;

			// Sensor configuration
			btGain = dataBuffer[posCount++];
			btSensorConfig = (char) (dataBuffer[posCount++]);

			// Position in space
			bt_position[0] = dataBuffer[posCount++] * 256
					+ dataBuffer[posCount++];
			bt_position[1] = dataBuffer[posCount++] * 256
					+ dataBuffer[posCount++];
			bt_position[2] = dataBuffer[posCount++] * 256
					+ dataBuffer[posCount++];

			// Data set 0, 16 bit values
			btData0size = dataBuffer[posCount++] * 256 + dataBuffer[posCount++];

			data0list = new double[btData0size / 2];

			for (int i = 0; i < btData0size / 2; ++i) {
				int temp1 = dataBuffer[posCount++];
				int temp2 = dataBuffer[posCount++];
				int temp3 = temp1 * 256 + temp2;

				boolean status = prefs.getBoolean(
						Settings.CALIBRATION_PREFERENCE_KEY, true);
				if (status) {

					switch (btSensorConfig) {
					case 'X': {
						data0list[i] = (temp3 - calImport.getChannel(0).get(0))
								* (calImport.getChannel(0).get(1)
										* Math.exp(-Math.abs(temp3
												- calImport.getChannel(0)
														.get(0))
												* calImport.getChannel(0)
														.get(2)) + calImport
										.getChannel(0).get(3));
					}
						break;
					case 'Y': {
						data0list[i] = (temp3 - calImport.getChannel(1).get(0))
								* (calImport.getChannel(1).get(1)
										* Math.exp(-Math.abs(temp3
												- calImport.getChannel(1)
														.get(0))
												* calImport.getChannel(1)
														.get(2)) + calImport
										.getChannel(1).get(3));
					}
						break;
					case 'Z': {
						data0list[i] = (temp3 - calImport.getChannel(2).get(0))
								* (calImport.getChannel(2).get(1)
										* Math.exp(-Math.abs(temp3
												- calImport.getChannel(2)
														.get(0))
												* calImport.getChannel(2)
														.get(2)) + calImport
										.getChannel(2).get(3));
					}
						break;
					default:
						break;
					}

				}
				// If calibration is disabled do nothing
				else {
					data0list[i] = temp3;
				}
			}

			if (data0list.length != 0) {
				btData0value = data0list[0];
			}

			btData1size = dataBuffer[posCount++];

			for (int varI = 0; varI < btData1size; ++varI) {
				btData1value = dataBuffer[posCount++] * 256
						+ dataBuffer[posCount++];
			}

			btChecksum = dataBuffer[posCount++];
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void editBurst(double[] magnField, char axis) {
		values = magnField.clone();

		// Save data only for offset calibration
		if (offsetCal) {
			switch (axis) {
			case 'X':
				valuesX = values.clone();
				for (int i = values.length - 1; i > values.length - 64; i--) {
					offsetX += valuesX[i];
				}
				offsetX /= 64;

				break;
			case 'Y':
				valuesY = values.clone();
				for (int i = values.length - 1; i > values.length - 64; i--) {
					offsetY += valuesY[i];
				}
				offsetY /= 64;
				break;
			case 'Z':
				valuesZ = values.clone();
				for (int i = values.length - 1; i > values.length - 64; i--) {
					offsetZ += valuesZ[i];
				}
				offsetZ /= 64;

				offsetCal = false;
				offsetCalibrationData = true;
				Toast.makeText(getApplicationContext(),
						"Offset Calibration successfull", Toast.LENGTH_SHORT)
						.show();
				btConnectionStatusBroadcast();
				// valuesFFTZ = valuesFFT.clone();
				// freqValuesZ = freqValues.clone();
				break;
			default:
				break;
			}

		} else if (offsetCalibrationData) {
			switch (axis) {
			case 'X':
				for (int i = 0; i < values.length; i++) {
					values[i] = values[i] - valuesX[i];
				}

				break;
			case 'Y':
				for (int i = 0; i < values.length; i++) {
					values[i] = values[i] - valuesY[i];
				}
				break;
			case 'Z':
				for (int i = 0; i < values.length; i++) {
					values[i] = values[i] - valuesZ[i];
				}
				break;
			default:
				break;
			}
		}

		freqValues = new double[magnField.length];
		valuesFFT = performFFT(values.clone(), freqValues);

	}

	public void offsetCalibration() {
		if (connectionStatus == 2) {
			Log.v("philipba", "BackgroundService start offset calibration");

			byte[] out = { 64 };
			sendMessage(out);

			// Get instance of Vibrator from current Context
			Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

			// Vibrate for 300 milliseconds
			v.vibrate(200);

		} else {
			Toast.makeText(getApplicationContext(), "No connection",
					Toast.LENGTH_SHORT).show();
		}

		offsetCal = true;
	}

	// Database Functions

	public void startMeasurement(String name1, String name2) {
		actualTableNameTrigger = name1;
		actualTableNameContinous = name2;
		dbAdapter.createTable(actualTableNameTrigger);
		dbAdapter.createTable(actualTableNameContinous);
		measureRunning = true;
		dbAdapter.open();
		dbAdapter.getDb().beginTransaction();
	}

	public void stopMeasurement() {
		measureRunning = false;
		dbAdapter.getDb().setTransactionSuccessful();
		dbAdapter.getDb().endTransaction();
		dbAdapter.close();
	}

	public void sendMessage(byte[] msg) {
		mChatService.write(msg);
	}

	// FFT
	private double[] performFFT(double[] data, double[] frequency) {
		final int nOfSamples = data.length;
		FFT fft = new FFT(nOfSamples);

		fft.realForward(data);

		double[] spectrum = new double[nOfSamples];
		for (int k = 0; k < data.length / 2 - 1; k++) {
			if (k >= -1) {

				spectrum[k] = 2
						* Math.sqrt((data[2 * k]) * (data[2 * k])
								+ (data[2 * k + 1]) * (data[2 * k + 1]))
						/ nOfSamples;

			}
		}

		for (int varI = 0; varI < nOfSamples; varI++) {
			frequency[varI] = varI * samplingFrequency / nOfSamples;
		}

		return spectrum;
	}

	private String extStoragePath;
	private File directory;

	private void writetoLogFile(String message) {
		extStoragePath = Environment.getExternalStorageDirectory()
				.getAbsolutePath();

		directory = new File(Environment.getExternalStorageDirectory()
				.toString() + "/Magnetometer/");
		boolean test = directory.mkdirs();

		String fileName = "MagnetometerLogFile.txt";

		try {

			File file = new File(directory.getAbsolutePath() + fileName);
			if (!file.exists()) {
				file.createNewFile();
			}
			BufferedWriter buf = new BufferedWriter(new FileWriter(file, true));
			buf.append(message);
			buf.newLine();
			buf.close();
		} catch (IOException ioe) {
			Log.v("philipba", "file not created");
			ioe.printStackTrace();
		}
	}

}