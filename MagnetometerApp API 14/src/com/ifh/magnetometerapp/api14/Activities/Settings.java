package com.ifh.magnetometerapp.api14.Activities;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemClock;
import android.preference.*;
import android.util.Log;
import android.widget.Toast;
import com.ifh.magnetometerapp.api14.Background.BackgroundService;
import com.ifh.magnetometerapp.api14.Background.SntpClient;

import java.util.Calendar;
import java.util.Date;

public class Settings extends PreferenceActivity {

    private BackgroundService syncService;
    private ProgressDialog pd;
    private String[] samplingFrequencies = {"1", "2", "3", "4"};
    public static final int SET_RTC_ID = 1;
    public static final int SET_GAIN_ID = 128;
    public static final int SET_SAMPLINGFREQUENCY_ID = 4;
    public static final String RTC_PREFERENCE_KEY = "RTC";
    public static final String GAIN_PREFERENCE_KEY = "GAIN";
    public static final String SAMPLING_PREFERENCE_KEY = "SAMPLING";
    public static final String CALIBRATION_PREFERENCE_KEY = "CALIBRATION";
    public static final String SATTELITEMAP_PREFERENCE_KEY = "SATELLITE";
    public static final String SPECTRALVIEW_MAX50_KEY = "SPEC_MAX50";
    public static final String SPECTRALVIEW_MAX100_KEY = "SPEC_MAX100";
    public static final String ABOUT_PREFERENCE_KEY = "ABOUT";
    public static final String LIVEVIEW_PERFORMANCE_PREFERENCE_KEY = "PERFORMANCE";
    public static final String SHOW_X_AXIS_KEY = "X_AXIS";
    public static final String SHOW_Y_AXIS_KEY = "Y_AXIS";
    public static final String SHOW_Z_AXIS_KEY = "Z_AXIS";
    public static final String LIVEVIEW_PROGRESS_KEY = "LIVEVIEW_PROGRESS_KEY";
    public static final String WAKELOCK_KEY = "WAKE_LOCK";
    public static final String SPECTRALVIEW_PROGRESS_KEY = "SPECTRUMVIEW_PROGRESS_KEY";
    private PreferenceScreen root;
    private ActionBar actionBar;
    private SpectralViewSelection max50;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Bind BackgroundService with this Activity
        getApplicationContext().bindService(
                new Intent(Settings.this, BackgroundService.class),
                serviceConnection, Context.BIND_AUTO_CREATE);
        actionBar = this.getActionBar();
        actionBar.setTitle("SETTINGS");
        actionBar.setDisplayShowHomeEnabled(false);
        setPreferenceScreen(createPreferenceHierarchy());
    }

    private PreferenceScreen createPreferenceHierarchy() {
        // Root
        root = getPreferenceManager().createPreferenceScreen(this);


        setUpLiveViewSettings();
        setUpSpectralFilterSettings();
        setUpMapViewSettings();
        setUpGeneralSettings();
        setUpAbout();

        return root;
    }

    public void setUpGeneralSettings() {
        // Inline preferences
        PreferenceCategory generalPrefCat = new PreferenceCategory(this);
        generalPrefCat.setTitle("General");
        root.addPreference(generalPrefCat);

        // Change RealTimeClock on MC-Board
        Preference setClockButton = new Preference(this);
        setClockButton.setKey(RTC_PREFERENCE_KEY);
        setClockButton.setTitle("Set RTC ");
        setClockButton
                .setSummary("Click to set RTC on MC-Board to actual date");
        generalPrefCat.addPreference(setClockButton);
        setClockButton
                .setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

                    public boolean onPreferenceClick(Preference preference) {

                        // Send Date: ddmmyyhhmmss
                        if (syncService.connectionStatus == 2) {

							/*
                             * pd = ProgressDialog.show(
							 * getApplicationContext(), "Loading...",
							 * "Please wait...", true, false);
							 */

                            Thread setRTC = new Thread() {
                                public void run() {

                                    try {

                                        Date date;
                                        boolean ntpConnection;
                                        SntpClient ntp = new SntpClient();
                                        long now;
                                        if (ntp.requestTime("pool.ntp.org", 0)) {
                                            now = ntp.getNtpTime()
                                                    + SystemClock
                                                    .elapsedRealtime()
                                                    - ntp.getNtpTimeReference();
                                            date = new Date(now);
                                            ntpConnection = true;
                                        } else {
                                            date = new Date();
                                            ntpConnection = false;

                                        }

                                        Calendar c = Calendar.getInstance();
                                        c.setTime(date);

                                        Log.v("ntp time", "" + date);

                                        byte trigger = (byte) SET_RTC_ID;

                                        byte year = (byte) c.get(Calendar.YEAR);
                                        byte month = (byte) c.get(Calendar.MONTH);
                                        byte day = (byte) c.get(Calendar.DAY_OF_MONTH);
                                        byte hour = (byte) c.get(Calendar.HOUR);
                                        byte minute = (byte) c.get(Calendar.MINUTE);
                                        byte second = (byte) c.get(Calendar.SECOND);

                                        byte[] arr = {trigger, year, month,
                                                day, hour, minute, second};

                                        syncService.sendMessage(arr);

                                        if (ntpConnection) {
                                            Toast.makeText(
                                                    getApplicationContext(),
                                                    "RTC succesfully set to NTP Time",
                                                    Toast.LENGTH_SHORT).show();

                                        } else {
                                            Toast.makeText(
                                                    getApplicationContext(),
                                                    "RTC succesfully set to Device Time \n(no internet access)",
                                                    Toast.LENGTH_SHORT).show();
                                        }

                                    } catch (Exception e) {
                                        Toast.makeText(getApplicationContext(),
                                                "Setting RTC failed!", Toast.LENGTH_SHORT)
                                                .show();
                                    }

                                }

                            };

                            setRTC.run();
                        } else {
                            Toast.makeText(getApplicationContext(),
                                    "No Connection to MC-Board", Toast.LENGTH_SHORT).show();
                        }

                        return true;
                    }
                });

        // Change Gain on MC-Board
        GainSelection gain = new GainSelection(this, null);
        gain.setTitle("Set Gain");
        gain.setKey(GAIN_PREFERENCE_KEY);
        gain.setSummary("Click to set Gain on MC-Board");
        gain.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

            public boolean onPreferenceClick(Preference preference) {
                if (syncService.connectionStatus != 2) {
                    Toast.makeText(getApplicationContext(),
                            "No Connection to MC-Board", Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });
        generalPrefCat.addPreference(gain);

        // Change Sampling Frequency on MC-Board
        SamplingFrequencySelection samplFreq = new SamplingFrequencySelection(
                this, null);
        samplFreq.setTitle("Sampling Frequency");
        samplFreq.setKey(SAMPLING_PREFERENCE_KEY);
        samplFreq.setSummary("Click to set Sampling Frequency on MC-Board");
        samplFreq
                .setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

                    public boolean onPreferenceClick(Preference preference) {
                        if (syncService.connectionStatus != 2) {
                            Toast.makeText(getApplicationContext(),
                                    "No Connection to MC-Board", Toast.LENGTH_SHORT).show();
                        }
                        return true;
                    }
                });
        generalPrefCat.addPreference(samplFreq);

        // Toggle Calibration
        CheckBoxPreference calibrationPreference = new CheckBoxPreference(this);
        calibrationPreference.setTitle("Calibration");
        calibrationPreference.setKey(CALIBRATION_PREFERENCE_KEY);
        calibrationPreference.setChecked(true);
        calibrationPreference.setSummaryOn("Calibration is activated");
        calibrationPreference.setSummaryOff("Calibration is deactivated");
        generalPrefCat.addPreference(calibrationPreference);

        // Toggle wakeLock
        SwitchPreference wakeLockPreference = new SwitchPreference(this);
        wakeLockPreference.setTitle("CPU wake lock");
        wakeLockPreference.setKey(WAKELOCK_KEY);
        wakeLockPreference.setChecked(false);
        wakeLockPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {

			public boolean onPreferenceChange(Preference arg0, Object arg1) {
				 boolean switched = ((SwitchPreference) arg0)
	                        .isChecked();
	                if (!switched) {
	                    syncService.setWakeLock(true);
	                } else {
	                    syncService.setWakeLock(false);
	                }

	                return true;
			}

           

        });
        generalPrefCat.addPreference(wakeLockPreference);
    }

    public void setUpLiveViewSettings() {
        // Inline preferences
        PreferenceCategory liveViewCatPreference = new PreferenceCategory(this);
        liveViewCatPreference.setTitle("Live View");
        root.addPreference(liveViewCatPreference);

        // Toggle Calibration
        LiveViewNumberPicker liveViewPreference = new LiveViewNumberPicker(this, null);
        liveViewPreference.setTitle("# points (affects performance)");
        liveViewPreference.setSummary("" + this.getPreferenceManager().getSharedPreferences()
                .getInt(LIVEVIEW_PERFORMANCE_PREFERENCE_KEY, 50));
        liveViewPreference.setKey(LIVEVIEW_PERFORMANCE_PREFERENCE_KEY);
        liveViewCatPreference.addPreference(liveViewPreference);
    }

    public void setUpSpectralFilterSettings() {
        // Inline preferences
        PreferenceCategory spectralFilterCat = new PreferenceCategory(this);
        spectralFilterCat.setTitle("Spectral Filter");
        root.addPreference(spectralFilterCat);

        // Change Spectral View Max Value
        max50 = new SpectralViewSelection(this, null);
        max50.setTitle("Set max value ");
        max50.setDialogTitle("Set max value in �T:");
        max50.setSummary(("50Hz: "
                + this.getPreferenceManager().getSharedPreferences()
                .getInt(SPECTRALVIEW_MAX50_KEY, 5)
                + " �T / "
                + "100Hz: "
                + this.getPreferenceManager().getSharedPreferences()
                .getInt(SPECTRALVIEW_MAX100_KEY, 5) + " �T"));

        spectralFilterCat.addPreference(max50);

    }

    public void setUpMapViewSettings() {
        // Inline preferences
        PreferenceCategory mapViewPrefCat = new PreferenceCategory(this);
        mapViewPrefCat.setTitle("Map View");
        root.addPreference(mapViewPrefCat);

        // Toggle Calibration
        CheckBoxPreference mapViewPreference = new CheckBoxPreference(this);
        mapViewPreference.setTitle("Show Satellite Map");
        mapViewPreference.setKey(SATTELITEMAP_PREFERENCE_KEY);
        mapViewPreference.setChecked(false);
        mapViewPreference.setSummaryOn("Satellite Map is activated");
        mapViewPreference.setSummaryOff("Satellite Map is deactivated");
        mapViewPrefCat.addPreference(mapViewPreference);
    }

    public void setUpAbout() {
        PreferenceCategory impressum = new PreferenceCategory(this);
        impressum.setTitle("Impressum");
        root.addPreference(impressum);

        Preference setAboutButton = new Preference(this);

        setAboutButton.setKey(ABOUT_PREFERENCE_KEY);
        setAboutButton.setTitle("About this App");
        setAboutButton
                .setSummary("This app was developed by\nPatrick Leidenberger and Philipp Bachmann\n@ ETH Zurich 2011-2012.");
        impressum.addPreference(setAboutButton);
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className, IBinder service) {
            syncService = ((BackgroundService.LocalBinder) service)
                    .getService();
        }

        public void onServiceDisconnected(ComponentName className) {
            syncService = null;
        }
    };

}
