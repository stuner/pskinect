package com.ifh.magnetometerapp.api14.Activities;

import android.app.Activity;
import android.content.*;
import android.graphics.*;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Vibrator;
import android.util.Log;
import android.view.*;
import android.widget.Toast;
import com.ifh.magnetometerapp.api14.Background.BackgroundService;
import com.ifh.magnetometerapp.api14.Background.MeasurementPoint;

import java.util.Iterator;
import java.util.LinkedList;

public class MapViewOld extends Activity {

    protected static final Activity MACTIVITY = null;
    private IntentFilter mapViewBroadcastFilter = new IntentFilter(
            BackgroundService.FFT_BROADCAST);
    private MapDrawingPanel drawPanel;
    private BackgroundService syncService;

    // To get the GPS position
    private LocationManager locManager;
    private LocationListener locListener = new MyLocationListener();

    private boolean gpsEnabled = false;
    private boolean networkEnabled = false;
    private String currentProvider;
    private long systemToGpsTimeOffset = Long.MAX_VALUE;
    private double gpsAccuracy;

    public MeasurementPoint currentMeasurement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Set layout
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        // Initialize DrawingPanel
        drawPanel = new MapDrawingPanel(this);
        setContentView(drawPanel);

        // Bind BackgroundService to this Activity
        bindService(new Intent(this, BackgroundService.class),
                serviceConnection, Context.BIND_AUTO_CREATE);

        locManager = (LocationManager) this
                .getSystemService(Context.LOCATION_SERVICE);


    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mapViewBroadcastReciever, mapViewBroadcastFilter);
        // exceptions will be thrown if provider is not permitted.
        try {
            gpsEnabled = locManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
        try {
            networkEnabled = locManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (gpsEnabled) {
            locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,
                    0, locListener);
            currentProvider = LocationManager.GPS_PROVIDER;
        } else if (networkEnabled) {
            locManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                    1000, 0, locListener);
            currentProvider = LocationManager.NETWORK_PROVIDER;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onDestroy() {

        super.onStop();
        Log.d("IFH MapView", "onPause start");
        locManager.removeUpdates(locListener);

        unregisterReceiver(mapViewBroadcastReciever);
        unbindService(serviceConnection);
        Log.d("IFH MapView", "onPause stop");
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className, IBinder service) {
            Log.d("IFH MapView", "onServiceConnected start");

            syncService = ((BackgroundService.LocalBinder) service)
                    .getService();
            // if (syncService.connectionStatus == 2)
            // {
            // byte[] out = { 32 };
            // syncService.sendMessage(out);
            // }

            Log.d("IFH MapView", "onServiceConnected stop");
        }

        public void onServiceDisconnected(ComponentName className) {
            locManager.removeUpdates(locListener);

            syncService = null;
        }
    };

    private final BroadcastReceiver mapViewBroadcastReciever = new BroadcastReceiver() {
        // Get Broadcast from BackgroundService
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("IFH MapView", "onReceive start");

            double[] frequency = intent.getExtras().getDoubleArray(
                    BackgroundService.FFT_FREQUENCY);
            // Get the amplitude of the fft.
            double[] spectrum = intent.getExtras().getDoubleArray(
                    BackgroundService.FFT_VALUES);
            // Get the sensor axis to which the data belongs.
            char axis = intent.getExtras().getChar(BackgroundService.FFT_AXIS);

            int index050Hz = 0;
            int index100Hz = 0;
            double freqDistance050Hz = Double.MAX_VALUE;
            double freqDistance100Hz = Double.MAX_VALUE;
            double freq050Hz = 50.0;
            double freq100Hz = 100.0;

            for (int varI = 0; varI < frequency.length; varI++) {
                if (Math.abs(frequency[varI] - freq050Hz) < freqDistance050Hz) {
                    index050Hz = varI;
                    freqDistance050Hz = Math.abs(frequency[varI] - freq050Hz);
                }
                if (Math.abs(frequency[varI] - freq100Hz) < freqDistance100Hz) {
                    index100Hz = varI;
                    freqDistance100Hz = Math.abs(frequency[varI] - freq100Hz);
                }
            }

            // If the values are not at a limit of the frequency array, add
            // three
            // FFT values around the frequency index together and display this.
            double[] temp = new double[2];
            temp[0] = (float) spectrum[index050Hz];
            if (index050Hz > 0) {
                temp[0] += 0.5 * spectrum[index050Hz - 1];
            }
            if (index050Hz < frequency.length - 1) {
                temp[0] += 0.5 * spectrum[index050Hz + 1];
            }

            temp[1] = spectrum[index100Hz];
            if (index100Hz > 0) {
                temp[1] += 0.5 * spectrum[index100Hz - 1];
            }
            if (index100Hz < frequency.length - 1) {
                temp[1] += 0.5 * spectrum[index100Hz + 1];
            }

            if (axis == 'X') {

                double longitude = -1;
                double latitude = -1;
                double altitude = -1;
                double accuracy = -1;
                long time = -1;

                try {


                    // Get the location
                    Location location = locManager
                            .getLastKnownLocation(currentProvider);
                    longitude = location.getLongitude();
                    latitude = location.getLatitude();
                    altitude = location.getAltitude();
                    accuracy = location.getAccuracy();
                    time = location.getTime();


                } catch (Exception e) {

                }

                currentMeasurement = new MeasurementPoint(longitude,
                        latitude, altitude, accuracy, time);
                currentMeasurement.setMagneticFieldX(temp[0]);

            } else if (axis == 'Y') {
                currentMeasurement.setMagneticFieldY(temp[0]);
            } else if (axis == 'Z') {
                currentMeasurement.setMagneticFieldZ(temp[0]);
                drawPanel.addPoint(currentMeasurement);
            }

            Log.d("IFH MapView", "onReceive stop");
        }
    };

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (event.getAction() == MotionEvent.ACTION_UP
                && event.getEventTime() - event.getDownTime() >= 1000) {


            if (syncService.connectionStatus == 2) {
                Log.v("MapView", "start trigger");
                Location location = locManager
                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);

                byte[] out = {64};
                syncService.sendMessage(out);

                // Get instance of Vibrator from current Context
                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

                // Vibrate for 300 milliseconds
                v.vibrate(200);
                Toast.makeText(getApplicationContext(),
                        "Measurement triggered", Toast.LENGTH_SHORT).show();

            }
        }

        return super.onTouchEvent(event);
    }

    class MyLocationListener implements LocationListener {
        public void onLocationChanged(Location location) {
            if (location != null) {
                // This needs to stop getting the location data and save the
                // battery power.
                // locManager.removeUpdates(locListener);

                long currentGpsTime = location.getTime();
                gpsAccuracy = location.getAccuracy();

                long systemTime = System.currentTimeMillis();
                systemToGpsTimeOffset = systemTime - currentGpsTime;
            }
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }

}

class MapDrawingPanel extends View {

    private Paint backgroundColor;
    private Paint borderColor;
    private Paint titleColor;
    private Paint circleColor = new Paint();
    private float titleheight = 20;
    private int screenwidth, screenheight;
    private PointF screenmiddle = new PointF();
    private float borderLength = 300;
    private float borderWidth = 160;
    private float borderStrokewidth = 2;
    private float[] borderline = new float[16];
    private boolean borderSet = false;
    public LinkedList<MeasurementPoint> points = new LinkedList<MeasurementPoint>();
    private Context _context;

    // Extrema data of the measured points.
    // This data is needed for the auto scaling of the map.
    private double longitudeMax = -200;
    private double longitudeMin = 200;
    private double latitudeMax = -100;
    private double latitudeMin = 100;
    private double magneticFieldXMax = Double.MIN_VALUE;
    private double magneticFieldXMin = Double.MAX_VALUE;
    private double magneticFieldYMax = Double.MIN_VALUE;
    private double magneticFieldYMin = Double.MAX_VALUE;
    private double magneticFieldZMax = Double.MIN_VALUE;
    private double magneticFieldZMin = Double.MAX_VALUE;
    private double magneticFieldSMax = Double.MIN_VALUE;
    private double magneticFieldSMin = Double.MAX_VALUE;

    private double gpsAccuracy = 10000;

    public int numOfPoints = 2000;

    private Bitmap bmp;

    public MapDrawingPanel(Context context) {
        super(context);
        Log.d("IFH MapView", "MapDrawingPanel start");

        _context = context;
        // Get available screen-size
        Display display = ((WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        screenwidth = display.getWidth();
        screenheight = display.getHeight();
        screenmiddle.x = screenwidth / 2;
        screenmiddle.y = screenheight / 2;
        setBorder(screenheight - 20, screenwidth - 20);
        Log.v("", "" + screenwidth);
        Log.v("", "" + screenheight);
        Log.v("", "" + screenmiddle.x);
        Log.v("", "" + screenmiddle.y);

        // Set Background Color
        backgroundColor = new Paint();
        backgroundColor.setColor(Color.BLACK);

        // Set Title Color
        titleColor = new Paint();
        titleColor.setColor(Color.BLACK);
        titleColor.setTextSize(titleheight);
        titleColor.setAntiAlias(true);

        // Set Border Color
        borderColor = new Paint();
        borderColor.setColor(Color.WHITE);
        borderColor.setStrokeWidth(borderStrokewidth);
        borderColor.setAntiAlias(true);
        borderColor.setTextSize(20);

        // Set circle color
        circleColor.setTextSize(20);
        circleColor.setAntiAlias(true);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // Draw Border
        if (borderSet) {
            canvas.drawLines(borderline, borderColor);
        }

        // Draw Points
        if (points.size() != 0) {
            float x;
            float y;

            double longitudeRange = longitudeMax - longitudeMin;
            if (longitudeRange == 0) {
                longitudeRange = 1.0;
            }
            double latitudeRange = latitudeMax - latitudeMin;
            if (latitudeRange == 0) {
                latitudeRange = 1.0;
            }

            Iterator<MeasurementPoint> it = points.listIterator();
            while (it.hasNext()) {
                MeasurementPoint currentPoint = it.next();
                // Calculate the position of the display from the coordinates.
                x = (float) (borderline[0] + ((currentPoint.getLongitude() - longitudeMin) / longitudeRange)
                        * borderWidth);
                y = (float) (borderline[1] + ((currentPoint.getLatitude() - latitudeMin) / latitudeRange)
                        * borderLength);

                // Make the color of the point according to the field strength.
                // Map it 0 .. 120 uT to 140 .. 0 of the H of the the HSV color
                // space.
                // h = 0 represents red
                // h = 140 represents green
                float[] hsv = new float[3];
                hsv[1] = 1;
                hsv[2] = 1;
                final double hueMax = 140.0;
                hsv[0] = (float) (hueMax - ((float) currentPoint.magneticFieldS / 120.0)
                        * hueMax);
                // If B > 120 uT print always red.
                if (currentPoint.magneticFieldS > 120.0) {
                    hsv[0] = (float) 0.0;
                }

                int color = Color.HSVToColor(hsv);
                circleColor.setColor(color);

                canvas.drawCircle(x, y, 10, circleColor);
            }
        }

        // Draw Gain Text
        canvas.drawText("Last GPS accuracy: " + String.valueOf(gpsAccuracy)
                + " m", 20, screenheight - 50, borderColor);
        // canvas.drawText("Gain: "+gain, 20, screenheight-20, borderColor);

        invalidate();
    }

    public void setBorder(float length, float width) {
        borderLength = length;
        borderWidth = width;

        borderline[0] = screenmiddle.x - borderStrokewidth / 2 - borderWidth
                / 2;
        borderline[1] = screenmiddle.y - borderStrokewidth / 2 - borderLength
                / 2;

        borderline[2] = screenmiddle.x + borderStrokewidth / 2 + borderWidth
                / 2;
        borderline[3] = screenmiddle.y - borderStrokewidth / 2 - borderLength
                / 2;

        borderline[4] = screenmiddle.x + borderStrokewidth / 2 + borderWidth
                / 2;
        borderline[5] = screenmiddle.y - borderStrokewidth / 2 - borderLength
                / 2;

        borderline[6] = screenmiddle.x + borderStrokewidth / 2 + borderWidth
                / 2;
        borderline[7] = screenmiddle.y + borderStrokewidth / 2 + borderLength
                / 2;

        borderline[8] = screenmiddle.x + borderStrokewidth / 2 + borderWidth
                / 2;
        borderline[9] = screenmiddle.y + borderStrokewidth / 2 + borderLength
                / 2;

        borderline[10] = screenmiddle.x - borderStrokewidth / 2 - borderWidth
                / 2;
        borderline[11] = screenmiddle.y + borderStrokewidth / 2 + borderLength
                / 2;

        borderline[12] = screenmiddle.x - borderStrokewidth / 2 - borderWidth
                / 2;
        borderline[13] = screenmiddle.y + borderStrokewidth / 2 + borderLength
                / 2;

        borderline[14] = screenmiddle.x - borderStrokewidth / 2 - borderWidth
                / 2;
        borderline[15] = screenmiddle.y - borderStrokewidth / 2 - borderLength
                / 2;

        borderSet = true;

        invalidate();
    }

    // Analyze data of measurement point and add data to linked list.
    public void addPoint(MeasurementPoint point) {
        Log.v("Patrick addPoint", "start");
        // Deteced the extrema of the data.

        // Deteced the extrema in the longitude.
        if (point.getLongitude() > longitudeMax) {
            longitudeMax = point.getLongitude();
        }
        if (point.getLongitude() < longitudeMin) {
            longitudeMin = point.getLongitude();
        }
        // Deteced the extrema in the latitude.
        if (point.getLatitude() > latitudeMax) {
            latitudeMax = point.getLatitude();
        }
        if (point.getLatitude() < latitudeMin) {
            latitudeMin = point.getLatitude();
        }

        // Deteced the extrema in x component of the magnetic field.
        if (point.getMagneticFieldX() > magneticFieldXMax) {
            magneticFieldXMax = point.getMagneticFieldX();
        }
        if (point.getMagneticFieldX() < magneticFieldXMin) {
            magneticFieldXMin = point.getMagneticFieldX();
        }

        // Deteced the extrema in y component of the magnetic field.
        if (point.getMagneticFieldY() > magneticFieldYMax) {
            magneticFieldYMax = point.getMagneticFieldY();
        }
        if (point.getMagneticFieldY() < magneticFieldYMin) {
            magneticFieldYMin = point.getMagneticFieldY();
        }

        // Deteced the extrema in z component of the magnetic field.
        if (point.getMagneticFieldZ() > magneticFieldZMax) {
            magneticFieldZMax = point.getMagneticFieldZ();
        }
        if (point.getMagneticFieldZ() < magneticFieldZMin) {
            magneticFieldZMin = point.getMagneticFieldZ();
        }

        // Deteced the extrema in the total component of the magnetic field.
        if (point.getMagneticFieldS() > magneticFieldSMax) {
            magneticFieldSMax = point.getMagneticFieldS();
        }
        if (point.getMagneticFieldS() < magneticFieldSMin) {
            magneticFieldSMin = point.getMagneticFieldS();
        }

        // Extract the last known accuracy.
        gpsAccuracy = point.getAccuracy();

        // Add Point to Linked List
        if (points.size() > numOfPoints) {
            points.removeFirst();
        }
        points.addLast(point);
        Log.v("Patrick addPoint", "stop");
    }

}
