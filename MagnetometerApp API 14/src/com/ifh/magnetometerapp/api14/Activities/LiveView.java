package com.ifh.magnetometerapp.api14.Activities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.*;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.*;
import android.widget.*;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.SeekBar.OnSeekBarChangeListener;
import com.androidplot.Plot.BorderStyle;
import com.androidplot.ui.AnchorPosition;
import com.androidplot.ui.DynamicTableModel;
import com.androidplot.ui.SizeLayoutType;
import com.androidplot.ui.SizeMetrics;
import com.androidplot.xy.*;
import com.ifh.magnetometerapp.api14.Background.BackgroundService;
import com.ifh.magnetometerapp.api14.R;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.util.Date;
import java.util.LinkedList;

public class LiveView extends Activity {

    // Plot widget
    private LinearLayout changeBoundariesLayout;
    private XYPlot livePlot;
    private SimpleXYSeries xAxisSeries = null;
    private SimpleXYSeries yAxisSeries = null;
    private SimpleXYSeries zAxisSeries = null;
    private LineAndPointFormatter xAxisFormat = null;
    private LineAndPointFormatter yAxisFormat = null;
    private LineAndPointFormatter zAxisFormat = null;
    private LinkedList<Number> valuesXAxis;
    private LinkedList<Number> valuesYAxis;
    private LinkedList<Number> valuesZAxis;
    private SeekBar changeBoundarieSeekBar;
    private ActionBar actionBar;

    // Standard widgets
    private EditText editLowerBound;
    private EditText editUpperBound;
    private CheckBox showXAxis;
    private CheckBox showYAxis;
    private CheckBox showZAxis;
    private Button saveBoundarieChanges;
    private DisplayMetrics dm = new DisplayMetrics();
    public int yLowerBoundCalibrated = -75;// 16384;
    public int yUpperBoundCalibrated = 75;// 49150;
    public int yLowerBoundNotCalibrated = 16384;// 16384;
    public int yUpperBoundNotCalibrated = 49152;// 49152;
    public int plotSize = 50;
    public int currentLowerBound = yLowerBoundCalibrated;
    public int currentUpperBound = yUpperBoundCalibrated;
    private boolean changeBoundariesIsOn = false;
    private Context context;

    // Broadcast
    private final IntentFilter plot_update_filter = new IntentFilter(
            BackgroundService.PLOTUPDATE_BROADCAST);
    private BackgroundService syncService;
    private boolean serviceConnected = false;

    // Take screenshot variables
    private Bitmap screenshot;
    private FileOutputStream out;
    private String filename;
    private Date date;

    // Preferences
    private SharedPreferences prefs;
    private Editor prefsEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // set layout
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.live_view);

        context = this;

        getWindowManager().getDefaultDisplay().getMetrics(dm);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefsEditor = prefs.edit();

        // Initialize Widgets
        actionBar = this.getActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle("LIVE VIEW");
        livePlot = (XYPlot) findViewById(R.id.timePlot);

        changeBoundariesLayout = (LinearLayout) findViewById(R.id.liveviewchangeboundarieslayout);
        changeBoundarieSeekBar = (SeekBar) findViewById(R.id.changeboundaries);
        editLowerBound = (EditText) findViewById(R.id.editlowerbound);
        editUpperBound = (EditText) findViewById(R.id.editupperbound);
        saveBoundarieChanges = (Button) findViewById(R.id.saveboundsbtn);
        showXAxis = (CheckBox) findViewById(R.id.xaxischeck);
        showYAxis = (CheckBox) findViewById(R.id.yaxischeck);
        showZAxis = (CheckBox) findViewById(R.id.zaxischeck);
        editLowerBound.setText(String.valueOf(currentLowerBound));
        editUpperBound.setText(String.valueOf(currentUpperBound));
        showXAxis.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if (isChecked) {
                    livePlot.addSeries(xAxisSeries, xAxisFormat);
                    xAxisSeries.setModel(valuesXAxis,
                            SimpleXYSeries.ArrayFormat.Y_VALS_ONLY);
                    prefsEditor.putBoolean(Settings.SHOW_X_AXIS_KEY, true);
                    prefsEditor.commit();
                } else {
                    livePlot.removeSeries(xAxisSeries);
                    prefsEditor.putBoolean(Settings.SHOW_X_AXIS_KEY, false);
                    prefsEditor.commit();
                }

                livePlot.redraw();

            }

        });
        showYAxis.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                if (isChecked) {
                    livePlot.addSeries(yAxisSeries, yAxisFormat);
                    yAxisSeries.setModel(valuesYAxis,
                            SimpleXYSeries.ArrayFormat.Y_VALS_ONLY);
                    prefsEditor.putBoolean(Settings.SHOW_Y_AXIS_KEY, true);
                    prefsEditor.commit();
                } else {
                    livePlot.removeSeries(yAxisSeries);
                    prefsEditor.putBoolean(Settings.SHOW_Y_AXIS_KEY, false);
                    prefsEditor.commit();
                }

                livePlot.redraw();

            }

        });
        showZAxis.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if (isChecked) {
                    livePlot.addSeries(zAxisSeries, zAxisFormat);
                    zAxisSeries.setModel(valuesZAxis,
                            SimpleXYSeries.ArrayFormat.Y_VALS_ONLY);
                    prefsEditor.putBoolean(Settings.SHOW_Z_AXIS_KEY, true);
                    prefsEditor.commit();
                } else {
                    livePlot.removeSeries(zAxisSeries);
                    prefsEditor.putBoolean(Settings.SHOW_Z_AXIS_KEY, false);
                    prefsEditor.commit();
                }

                livePlot.redraw();

            }

        });
        saveBoundarieChanges.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                changeBoundarieSeekBar.setProgress(changeBoundarieSeekBar
                        .getMax() / 2);

                yLowerBoundCalibrated = Integer.parseInt(editLowerBound
                        .getText().toString());
                yUpperBoundCalibrated = Integer.parseInt(editUpperBound
                        .getText().toString());

                currentLowerBound = yLowerBoundCalibrated;
                currentUpperBound = yUpperBoundCalibrated;

                livePlot.setRangeBoundaries(yLowerBoundCalibrated,
                        yUpperBoundCalibrated, BoundaryMode.FIXED);

                livePlot.redraw();

                showBoundariesChangeSettings();

            }
        });
        changeBoundarieSeekBar.setMax(150);
        changeBoundarieSeekBar.setProgress(changeBoundarieSeekBar.getMax() / 2);
        changeBoundarieSeekBar
                .setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {

                        if (progress < changeBoundarieSeekBar.getMax() - 1) {

                            boolean status = prefs.getBoolean(
                                    Settings.CALIBRATION_PREFERENCE_KEY, true);
                            if (status) {
                                currentLowerBound = yLowerBoundCalibrated
                                        + (progress - changeBoundarieSeekBar
                                        .getMax() / 2);

                                currentUpperBound = yUpperBoundCalibrated
                                        + (changeBoundarieSeekBar.getMax() / 2 - progress);

                                livePlot.setRangeBoundaries(
                                        yLowerBoundCalibrated
                                                + (progress - changeBoundarieSeekBar
                                                .getMax() / 2),
                                        yUpperBoundCalibrated
                                                + (changeBoundarieSeekBar
                                                .getMax() / 2 - progress),
                                        BoundaryMode.FIXED);

                                editLowerBound.setText(String
                                        .valueOf(currentLowerBound));
                                editUpperBound.setText(String
                                        .valueOf(currentUpperBound));
                                livePlot.setRangeLabel("B [�T]");
                            } else {
                                currentLowerBound = yLowerBoundNotCalibrated
                                        + (222 * (progress - changeBoundarieSeekBar
                                        .getMax() / 2));

                                currentUpperBound = yUpperBoundNotCalibrated
                                        + (222 * (changeBoundarieSeekBar
                                        .getMax() / 2 - progress));

                                livePlot.setRangeBoundaries(
                                        yLowerBoundNotCalibrated
                                                + 222
                                                * (progress - changeBoundarieSeekBar
                                                .getMax() / 2),
                                        yUpperBoundNotCalibrated
                                                + 222
                                                * (changeBoundarieSeekBar
                                                .getMax() / 2 - progress),
                                        BoundaryMode.FIXED);

                                editLowerBound.setText(String
                                        .valueOf(currentLowerBound));
                                editUpperBound.setText(String
                                        .valueOf(currentUpperBound));
                                livePlot.setRangeLabel("Int16");
                            }


                            livePlot.getRangeLabelWidget().setHeight(100);
                            livePlot.getRangeLabelWidget().setWidth(20);
                            livePlot.getRangeLabelWidget().setMarginLeft(0);
                            livePlot.redraw();
                        }

                    }

                    public void onStartTrackingTouch(SeekBar seekBar) {


                    }

                    public void onStopTrackingTouch(SeekBar seekBar) {


                    }

                });

        valuesXAxis = new LinkedList<Number>();
        valuesYAxis = new LinkedList<Number>();
        valuesZAxis = new LinkedList<Number>();

        // Bind BackgroundService with this Activity
        getApplicationContext().bindService(
                new Intent(LiveView.this, BackgroundService.class),
                serviceConnection, Context.BIND_AUTO_CREATE);

        initializePlots();

    }

    protected void onResume() {
        super.onResume();
        registerReceiver(plotUpdateReceiver, plot_update_filter);

        if (serviceConnected) {
            byte[] out = {32};
            syncService.sendMessage(out);
        }

        valuesXAxis.clear();
        valuesYAxis.clear();
        valuesZAxis.clear();

        if (prefs.getBoolean(Settings.SHOW_X_AXIS_KEY, true)) {
            livePlot.addSeries(xAxisSeries, xAxisFormat);
            xAxisSeries.setModel(valuesXAxis,
                    SimpleXYSeries.ArrayFormat.Y_VALS_ONLY);
            showXAxis.setChecked(true);
        } else {
            livePlot.removeSeries(xAxisSeries);
            showXAxis.setChecked(false);
        }
        if (prefs.getBoolean(Settings.SHOW_Y_AXIS_KEY, true)) {
            livePlot.addSeries(yAxisSeries, yAxisFormat);
            yAxisSeries.setModel(valuesYAxis,
                    SimpleXYSeries.ArrayFormat.Y_VALS_ONLY);
            showYAxis.setChecked(true);
        } else {
            livePlot.removeSeries(yAxisSeries);
            showYAxis.setChecked(false);
        }
        if (prefs.getBoolean(Settings.SHOW_Z_AXIS_KEY, true)) {
            livePlot.addSeries(zAxisSeries, zAxisFormat);
            zAxisSeries.setModel(valuesZAxis,
                    SimpleXYSeries.ArrayFormat.Y_VALS_ONLY);
            showZAxis.setChecked(true);
        } else {
            livePlot.removeSeries(zAxisSeries);
            showZAxis.setChecked(false);
        }


        boolean status = prefs.getBoolean(Settings.CALIBRATION_PREFERENCE_KEY,
                true);

        if (status) {
            livePlot.setRangeLabel("B [uT]");
            livePlot.setRangeBoundaries(yLowerBoundCalibrated,
                    yUpperBoundCalibrated, BoundaryMode.FIXED);
        } else {
            livePlot.setRangeLabel("Int16");
            livePlot.setRangeBoundaries(yLowerBoundNotCalibrated,
                    yUpperBoundNotCalibrated, BoundaryMode.FIXED);
        }

        int progress = prefs.getInt(Settings.LIVEVIEW_PROGRESS_KEY, changeBoundarieSeekBar.getMax() / 2);
        changeBoundarieSeekBar.setProgress(progress);

        plotSize = prefs.getInt(Settings.LIVEVIEW_PERFORMANCE_PREFERENCE_KEY, 50);

        livePlot.redraw();

    }

    @Override
    protected void onStop() {
        // unregister receivers
        unregisterReceiver(plotUpdateReceiver);

        if (syncService.connectionStatus == 2) {
            byte[] out = {16};
            syncService.sendMessage(out);
        }

        prefsEditor.putInt(Settings.LIVEVIEW_PROGRESS_KEY, changeBoundarieSeekBar.getProgress());
        prefsEditor.commit();

        super.onStop();
    }

    protected void onPause() {


        super.onPause();
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className, IBinder service) {
            syncService = ((BackgroundService.LocalBinder) service)
                    .getService();


            if (syncService.connectionStatus == 2) {
                byte[] out = {32};
                syncService.sendMessage(out);
                serviceConnected = true;
            }

        }

        public void onServiceDisconnected(ComponentName className) {
            syncService = null;
            serviceConnected = false;
        }
    };

    private final BroadcastReceiver plotUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            double value = intent.getExtras().getDouble(
                    BackgroundService.PLOTUPDATE_VALUES);
            char axis = intent.getExtras().getChar(
                    BackgroundService.PLOTUPDATE_AXIS);
            switch (axis) {
                case 'X':

                    if (valuesXAxis.size() > plotSize) {
                        valuesXAxis.removeFirst();
                    }
                    if (syncService.offsetCalibrationData) {
                        value -= syncService.offsetX;
                    }
                    valuesXAxis.addLast(value);
                    xAxisSeries.setModel(valuesXAxis,
                            SimpleXYSeries.ArrayFormat.Y_VALS_ONLY);
                    break;
                case 'Y':
                    if (valuesYAxis.size() > plotSize) {
                        valuesYAxis.removeFirst();
                    }
                    if (syncService.offsetCalibrationData) {
                        value -= syncService.offsetY;
                    }
                    valuesYAxis.addLast(value);
                    yAxisSeries.setModel(valuesYAxis,
                            SimpleXYSeries.ArrayFormat.Y_VALS_ONLY);
                    break;
                case 'Z':
                    if (valuesZAxis.size() > plotSize) {
                        valuesZAxis.removeFirst();
                    }
                    if (syncService.offsetCalibrationData) {
                        value -= syncService.offsetZ;
                    }
                    valuesZAxis.addLast(value);
                    zAxisSeries.setModel(valuesZAxis,
                            SimpleXYSeries.ArrayFormat.Y_VALS_ONLY);

                    livePlot.redraw();
                    break;
                default:
                    break;

            }

        }
    };

    public void initializePlots() {
        // Turn the above arrays into XYSeries:
        xAxisSeries = new SimpleXYSeries("x-Axis");
        yAxisSeries = new SimpleXYSeries("y-Axis");
        zAxisSeries = new SimpleXYSeries("z-Axis");

        // Create a formatter to use for drawing a series using
        // LineAndPointRenderer:
        /*
         * LineAndPointFormatter liveSeriesFormat = new LineAndPointFormatter(
		 * Color.rgb(0, 0, 128), // line color Color.rgb(100, 149, 237), //
		 * point color Color.argb(0, 0, 0, 0)); // fill color (optional)
		 */
        xAxisFormat = new LineAndPointFormatter(getResources().getColor(R.color.red), // line
                // color
                Color.argb(0, 255, 0, 0), // point color
                Color.argb(0, 0, 0, 0)); // fill color (optional)
        yAxisFormat = new LineAndPointFormatter(getResources().getColor(R.color.green), // line
                // color
                Color.argb(0, 0, 255, 0), // point color
                Color.argb(0, 0, 0, 0)); // fill color (optional)
        zAxisFormat = new LineAndPointFormatter(getResources().getColor(R.color.blue), // line
                // color
                Color.argb(0, 0, 0, 255), // point color
                Color.argb(0, 0, 0, 0)); // fill color (optional)

        // liveSeriesFormat.setVertexPaint(null);
       /* xAxisFormat.getLinePaint().setStrokeWidth(
                Math.round(dm.densityDpi
                        * ((float) getApplicationContext().getResources()
                        .getInteger(R.integer.plot_line_size_inc_4))
                        / 10000));
        yAxisFormat.getLinePaint().setStrokeWidth(
                Math.round(dm.densityDpi
                        * ((float) getApplicationContext().getResources()
                        .getInteger(R.integer.plot_line_size_inc_4))
                        / 10000));
        zAxisFormat.getLinePaint().setStrokeWidth(
                Math.round(dm.densityDpi
                        * ((float) getApplicationContext().getResources()
                        .getInteger(R.integer.plot_line_size_inc_4))
                        / 10000));*/

        // Add series1 to the xyplot:
        livePlot.addSeries(xAxisSeries, xAxisFormat);
        livePlot.addSeries(yAxisSeries, yAxisFormat);
        livePlot.addSeries(zAxisSeries, zAxisFormat);

        // Reduce the number of range labels
        livePlot.setTicksPerRangeLabel(1);
        livePlot.setTicksPerDomainLabel(20);

        livePlot.setDomainLabel("time");

        livePlot.setRangeStep(XYStepMode.SUBDIVIDE, 11);

        livePlot.setPlotMargins(10, 10, 10, 10);
        livePlot.setPlotPadding(10, 10, 10, 10);


        // Axis Labels
        livePlot.getRangeLabelWidget().getLabelPaint().setTextSize(20);
        livePlot.getRangeLabelWidget().setWidth(20);
        livePlot.getRangeLabelWidget().setMarginLeft(0);
        livePlot.getRangeLabelWidget().setHeight(100);
        livePlot.getDomainLabelWidget().setHeight(20);
        livePlot.getDomainLabelWidget().setWidth(100);
        livePlot.getDomainLabelWidget().getLabelPaint().setTextSize(20);
        livePlot.getDomainLabelWidget().setMarginLeft(50);
        livePlot.getTitleWidget().getLabelPaint().setTextSize(20);
        livePlot.getTitleWidget().setWidth(100);
        livePlot.getTitleWidget().setHeight(20);
        livePlot.getGraphWidget().setRangeLabelWidth(50);
        livePlot.getGraphWidget().setRangeLabelWidth(50);
        livePlot.getGraphWidget().getRangeLabelPaint().setTextSize(15);
        livePlot.getGraphWidget().getDomainLabelPaint().setTextSize(15);
        livePlot.getGraphWidget().getRangeOriginLabelPaint().setTextSize(15);
        livePlot.getGraphWidget().getDomainOriginLabelPaint().setTextSize(15);

        livePlot.position(
                livePlot.getDomainLabelWidget(),
                0,
                XLayoutStyle.ABSOLUTE_FROM_CENTER,
                -10,
                YLayoutStyle.ABSOLUTE_FROM_BOTTOM,
                AnchorPosition.BOTTOM_MIDDLE);

        // Legend Widget
        livePlot.getLegendWidget().setTableModel(new DynamicTableModel(2, 2));
        livePlot.getLegendWidget().setSize(
                new SizeMetrics(80, SizeLayoutType.ABSOLUTE, 200,
                        SizeLayoutType.ABSOLUTE));
        Paint bgPaint = new Paint();
        bgPaint.setColor(Color.BLACK);
        bgPaint.setStyle(Paint.Style.FILL);
        bgPaint.setAlpha(140);
        livePlot.getLegendWidget().setBackgroundPaint(bgPaint);
        livePlot.getLegendWidget().setPadding(12, 1, 1, 1);
        livePlot.getLegendWidget().getTextPaint().setTextSize(20);
        livePlot.getLegendWidget().setIconSizeMetrics(
                new SizeMetrics(20, SizeLayoutType.ABSOLUTE, 20,
                        SizeLayoutType.ABSOLUTE));

        // By default, AndroidPlot displays developer guides to aid in
        // laying out your plot.
        // To get rid of them call disableAllMarkup():

        livePlot.getGraphWidget().setMarginTop(10);
        livePlot.getGraphWidget().setMarginLeft(10);
        livePlot.getGraphWidget().setMarginRight(2);
        livePlot.setBorderStyle(BorderStyle.SQUARE, null, null);
        livePlot.disableAllMarkup();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.live_view_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.screenshot_menu:
                takeScreenshot();
                return true;
            case R.id.staticboundaries:
                showBoundariesChangeSettings();
                return true;
            case R.id.menu_settings:
                Intent intent = new Intent(LiveView.this, Settings.class);
                startActivity(intent);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void takeScreenshot() {
        // Get instance of Vibrator from current Context
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        // Vibrate for 300 milliseconds
        v.vibrate(300);

        File exportsDirectory = new File(BackgroundService.SCREENSHOTS_PATH);
        exportsDirectory.mkdirs();


        livePlot.setDrawingCacheEnabled(true);

        screenshot = livePlot.getDrawingCache();

        date = new Date();

        filename = BackgroundService.SCREENSHOTS_PATH + "Screenshot_"
                + DateFormat.getDateInstance().format(new Date()) + "_"
                + date.getHours() + "." + date.getMinutes() + "."
                + date.getSeconds() + ".png";


        try {
            out = new FileOutputStream(filename);
            screenshot.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.flush();
            out.close();
            Toast.makeText(this,
                    "Screenshot succesfully saved to: " + filename,
                    Toast.LENGTH_LONG).show();
            Uri uri = Uri.fromFile(exportsDirectory);
            Intent scanFileIntent = new Intent(
                    Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri);
            sendBroadcast(scanFileIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showBoundariesChangeSettings() {

        float weight1, weight2;

        if (changeBoundariesIsOn) {
            weight1 = 0;
            weight2 = 100;
            changeBoundariesIsOn = false;
        } else {
            weight1 = 30;
            weight2 = 70;
            changeBoundariesIsOn = true;
        }

        ViewGroup.LayoutParams param1 = livePlot.getLayoutParams();
        ViewGroup.LayoutParams param2 = changeBoundariesLayout
                .getLayoutParams();

        LinearLayout.LayoutParams param3 = new LinearLayout.LayoutParams(
                param1.width, param1.height, weight1);
        livePlot.setLayoutParams(param3);

        LinearLayout.LayoutParams param4 = new LinearLayout.LayoutParams(
                param2.width, param2.height, weight2);
        changeBoundariesLayout.setLayoutParams(param4);

    }
}
