package com.ifh.magnetometerapp.api14.Activities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.*;
import android.content.SharedPreferences.Editor;
import android.graphics.*;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.*;
import android.widget.Toast;
import com.ifh.magnetometerapp.api14.Background.BackgroundService;
import com.ifh.magnetometerapp.api14.R;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.util.Date;

public class SpectralFilter extends Activity {

    protected static final Activity MACTIVITY = null;
    private IntentFilter mapViewBroadcastFilter = new IntentFilter(
            BackgroundService.FFT_BROADCAST);
    private SpectrumDrawingPanel drawPanel;
    private BackgroundService syncService;
    private double[] spectrumX;
    private double[] spectrumY;
    private double[] spectrumZ;
    private double[] frequency;
    private SharedPreferences prefs;
    private Editor prefsEditor;

    // Take screenshot variables
    private Bitmap screenshot;
    private FileOutputStream out;
    private String filename;
    private Date date;
    private ActionBar actionBar;
    private WindowManager windowManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Set layout
        // getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        // WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onCreate(savedInstanceState);

        windowManager = this.getWindowManager();

        // Get shared preferences (f.e. calibration status)
        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        // ActionBar
        actionBar = this.getActionBar();
        actionBar.setTitle("SPECTRAL FILTER");
        actionBar.setDisplayShowHomeEnabled(false);

        // Initialize DrawingPanel
        drawPanel = new SpectrumDrawingPanel(this);
        setContentView(drawPanel);


    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mapViewBroadcastReciever, mapViewBroadcastFilter);
        // Check Maximum Value from Settings
        int max50HZ = prefs
                .getInt(Settings.SPECTRALVIEW_MAX50_KEY, 5);
        int max100HZ = prefs.getInt(Settings.SPECTRALVIEW_MAX100_KEY,
                5);
        drawPanel.maxValue50 = max50HZ;
        drawPanel.maxValue100 = max100HZ;

        Log.v("philipba", "onResume");
        drawPanel.invalidate();
        // Bind BackgroundService to this Activity
        bindService(new Intent(this, BackgroundService.class),
                serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        // unregister receivers
        unregisterReceiver(mapViewBroadcastReciever);
        unbindService(serviceConnection);
        super.onStop();
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className, IBinder service) {
            syncService = ((BackgroundService.LocalBinder) service)
                    .getService();
            Log.v("philipba", "syncservice connected");
            /*if(syncService.valuesFFTX!=null)
            {
				spectrumX=syncService.valuesFFTX.clone();
				spectrumY=syncService.valuesFFTY.clone();
				spectrumZ=syncService.valuesFFTZ.clone();
				frequency=syncService.freqValuesX.clone();
				updateScreen();
			}

			drawPanel.invalidate();*/


        }

        public void onServiceDisconnected(ComponentName className) {
            syncService = null;
        }
    };

    // Receive Data from BackgroundService

    private final BroadcastReceiver mapViewBroadcastReciever = new BroadcastReceiver() {
        // Get Broadcast from BackgroundService
        @Override
        public void onReceive(Context context, Intent intent) {
            intent.getExtras();

            // Get the frequency axis form the fft.
            frequency = intent
                    .getDoubleArrayExtra(BackgroundService.FFT_FREQUENCY);
            // Get the amplitude of the fft.
            double[] spectrum = intent.getExtras().getDoubleArray(
                    BackgroundService.FFT_VALUES);
            // Get the sensor axis to which the data belongs.
            char axis = intent.getExtras().getChar(BackgroundService.FFT_AXIS);

            switch (axis) {
                case 'X':
                    spectrumX = spectrum;
                    break;
                case 'Y':
                    spectrumY = spectrum;
                    break;
                case 'Z':
                    spectrumZ = spectrum;

                    updateScreen();
                    break;
                default:
                    break;
            }

        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.spectralfilter_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.screenshot_spectral:
                takeScreenshot();
                return true;
            case R.id.settings_spectral:
                Intent intent = new Intent(SpectralFilter.this, Settings.class);
                startActivity(intent);
                return true;
            case R.id.trigger_spectral:
                triggerMeasurement();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void takeScreenshot() {
        // Get instance of Vibrator from current Context
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        // Vibrate for 300 milliseconds
        v.vibrate(300);

        File exportsDirectory = new File(BackgroundService.SCREENSHOTS_PATH);
        exportsDirectory.mkdirs();


        drawPanel.setDrawingCacheEnabled(true);

        screenshot = drawPanel.getDrawingCache();

        date = new Date();

        filename = BackgroundService.SCREENSHOTS_PATH + "Screenshot_"
                + DateFormat.getDateInstance().format(new Date()) + "_"
                + date.getHours() + "." + date.getMinutes() + "."
                + date.getSeconds() + ".png";


        try {
            out = new FileOutputStream(filename);
            screenshot.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.flush();
            out.close();
            Toast.makeText(this,
                    "Screenshot succesfully saved to: " + filename,
                    Toast.LENGTH_LONG).show();
            Uri uri = Uri.fromFile(exportsDirectory);
            Intent scanFileIntent = new Intent(
                    Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri);
            sendBroadcast(scanFileIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void triggerMeasurement() {

        if (syncService.connectionStatus == 2) {
            Log.v("MapView", "start trigger");

            byte[] out = {64};
            syncService.sendMessage(out);

            // Get instance of Vibrator from current Context
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

            // Vibrate for 300 milliseconds
            v.vibrate(200);
            Toast.makeText(getApplicationContext(), "Measurement triggered", Toast.LENGTH_SHORT)
                    .show();

        } else {
            Toast.makeText(getApplicationContext(), "No connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void updateScreen() {
        Log.v("philipba", "update screen");
        float[] temp = new float[8];

        // Find the frequency index, that is closest to 50 and 100 Hz.
        int index050Hz = 0;
        int index100Hz = 0;
        double freqDistance050Hz = Double.MAX_VALUE;
        double freqDistance100Hz = Double.MAX_VALUE;
        double freq050Hz = 50.0;
        double freq100Hz = 100.0;

        for (int varI = 0; varI < frequency.length; varI++) {
            if (Math.abs(frequency[varI] - freq050Hz) < freqDistance050Hz) {
                index050Hz = varI;
                freqDistance050Hz = Math.abs(frequency[varI] - freq050Hz);
            }
            if (Math.abs(frequency[varI] - freq100Hz) < freqDistance100Hz) {
                index100Hz = varI;
                freqDistance100Hz = Math.abs(frequency[varI] - freq100Hz);
            }
        }

        // If the values are not at a limit of the frequency array, add three
        // FFT values around the frequency index together and display this.
        temp[0] = (float) spectrumX[index050Hz];
        temp[1] = (float) spectrumY[index050Hz];
        temp[2] = (float) spectrumZ[index050Hz];
        if (index050Hz > 0) {
            temp[0] += 0.5 * (float) spectrumX[index050Hz - 1];
            temp[1] += 0.5 * (float) spectrumY[index050Hz - 1];
            temp[2] += 0.5 * (float) spectrumZ[index050Hz - 1];
        }
        if (index050Hz < frequency.length - 1) {
            temp[0] += 0.5 * (float) spectrumX[index050Hz + 1];
            temp[1] += 0.5 * (float) spectrumY[index050Hz + 1];
            temp[2] += 0.5 * (float) spectrumZ[index050Hz + 1];
        }
        temp[3] = (float) Math.sqrt(Math.pow(temp[0], 2) + Math.pow(temp[1], 2)
                + Math.pow(temp[2], 2));

        temp[4] = (float) spectrumX[index100Hz];
        temp[5] = (float) spectrumY[index100Hz];
        temp[6] = (float) spectrumZ[index100Hz];
        if (index100Hz > 0) {
            temp[4] += 0.5 * (float) spectrumX[index100Hz - 1];
            temp[5] += 0.5 * (float) spectrumY[index100Hz - 1];
            temp[6] += 0.5 * (float) spectrumZ[index100Hz - 1];
        }
        if (index100Hz < frequency.length - 1) {
            temp[4] += 0.5 * (float) spectrumX[index100Hz + 1];
            temp[5] += 0.5 * (float) spectrumY[index100Hz + 1];
            temp[6] += 0.5 * (float) spectrumZ[index100Hz + 1];
        }
        temp[7] = (float) Math.sqrt(Math.pow(temp[4], 2) + Math.pow(temp[5], 2)
                + Math.pow(temp[6], 2));

        // Check Maximum Value from Settings
        int max50HZ = prefs
                .getInt(Settings.SPECTRALVIEW_MAX50_KEY, 5);
        int max100HZ = prefs.getInt(Settings.SPECTRALVIEW_MAX100_KEY,
                5);
        drawPanel.maxValue50 = max50HZ;
        drawPanel.maxValue100 = max100HZ;

        for (int i = 0; i < 8; i++) {
            if (i < 4) {
                if (drawPanel.maxValue50 <= temp[i]) {
                    drawPanel.maxValue50 = (int) Math.ceil(temp[i] + temp[i]
                            * 0.3);
                }
            } else {
                if (drawPanel.maxValue100 <= temp[i]) {
                    drawPanel.maxValue100 = (int) Math.ceil(temp[i] + temp[i]
                            * 0.3);
                }
            }
        }
        prefsEditor = prefs.edit();
        prefsEditor.putInt(Settings.SPECTRALVIEW_MAX50_KEY,
                drawPanel.maxValue50);
        prefsEditor.putInt(Settings.SPECTRALVIEW_MAX100_KEY,
                drawPanel.maxValue100);
        prefsEditor.commit();

        drawPanel.setBars(temp);
    }
}

class SpectrumDrawingPanel extends View {

    private Drawable background;
    private Paint paintBG50, paintBG100;
    private Rect boundBG, bound50, bound100;
    private float screenwidth, screenheight;
    private RectF rect50HzX;
    private RectF rect50HzY;
    private RectF rect50HzZ;
    private RectF rect50HzS;
    private RectF rect100HzX;
    private RectF rect100HzY;
    private RectF rect100HzZ;
    private RectF rect100HzS;
    private float barOffset, barMaxHeight, bar50Middle, bar100Middle;
    private Paint paintText50, paintText100, paintTitle;
    private Paint paint50barX;
    private Paint paint50barY;
    private Paint paint50barZ;
    private Paint paint50barS;
    private Paint paint100barX;
    private Paint paint100barY;
    private Paint paint100barZ;
    private Paint paint100barS;
    private Paint paint50value, paint100value;

    private float bar50left, bar50right, bar50bottom, bar50top;
    private float bar100left, bar100right, bar100bottom, bar100top;
    private float text50Y, text100Y, titleY;
    private String textTitle = "Magnetic Field Strength [�T]";
    private static final String TEXT_50 = "50 Hz";
    private static final String TEXT_100 = "100 Hz";
    private float value50x = 0, value50y = 0, value50z = 0, value50s = 0,
            value100x = 0, value100y = 0, value100z = 0, value100s = 0;
    public int maxValue50 = 120, maxValue100 = 120;

    public SpectrumDrawingPanel(Context context) {
        super(context);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // Draw Background
        // background.draw(canvas);
        canvas.drawRect(bound50, paintBG50);
        canvas.drawRect(bound100, paintBG100);

        // Draw Rects
        canvas.drawRect(rect50HzX, paint50barX);
        canvas.drawRect(rect50HzY, paint50barY);
        canvas.drawRect(rect50HzZ, paint50barZ);
        canvas.drawRect(rect50HzS, paint50barS);
        canvas.drawRect(rect100HzX, paint100barX);
        canvas.drawRect(rect100HzY, paint100barY);
        canvas.drawRect(rect100HzZ, paint100barZ);
        canvas.drawRect(rect100HzS, paint100barS);

        // Draw Text
        canvas.drawText(TEXT_50, bar50Middle - paintText50.measureText(TEXT_50)
                / 2, text50Y, paintText50);
        canvas.drawText(TEXT_100,
                bar100Middle - paintText50.measureText(TEXT_100) / 2, text100Y,
                paintText100);
        canvas.drawText(textTitle,
                screenwidth / 2 - paintTitle.measureText(textTitle) / 2, titleY
                + paintTitle.getTextSize() / 2, paintTitle);

        // Value Level
        /*
         * canvas.drawLine(rect50Hz.right, rect50Hz.top, 0.4f * screenwidth,
		 * rect50Hz.top, paint50value); canvas.drawLine(rect100Hz.left,
		 * rect100Hz.top, 0.6f * screenwidth, rect100Hz.top, paint100value);
		 */
        canvas.drawText(Float.toString(value50x), rect50HzX.left
                + (rect50HzX.right - rect50HzX.left) / 2, rect50HzX.top
                - paint50value.getTextSize() / 4, paint50value);
        canvas.drawText(Float.toString(value50y), rect50HzY.left
                + (rect50HzY.right - rect50HzY.left) / 2, rect50HzY.top
                - paint50value.getTextSize() / 4, paint50value);
        canvas.drawText(Float.toString(value50z), rect50HzZ.left
                + (rect50HzZ.right - rect50HzZ.left) / 2, rect50HzZ.top
                - paint50value.getTextSize() / 4, paint50value);
        canvas.drawText(Float.toString(value50s), rect50HzS.left
                + (rect50HzS.right - rect50HzS.left) / 2, rect50HzS.top
                - paint50value.getTextSize() / 4, paint50value);
        canvas.drawText(Float.toString(value100x), rect100HzX.left
                + (rect100HzX.right - rect100HzX.left) / 2, rect100HzX.top
                - paint100value.getTextSize() / 4, paint100value);
        canvas.drawText(Float.toString(value100y), rect100HzY.left
                + (rect100HzY.right - rect100HzY.left) / 2, rect100HzY.top
                - paint100value.getTextSize() / 4, paint100value);
        canvas.drawText(Float.toString(value100z), rect100HzZ.left
                + (rect100HzZ.right - rect100HzZ.left) / 2, rect100HzZ.top
                - paint100value.getTextSize() / 4, paint100value);
        canvas.drawText(Float.toString(value100s), rect100HzS.left
                + (rect100HzS.right - rect100HzS.left) / 2, rect100HzS.top
                - paint100value.getTextSize() / 4, paint100value);

        // Draw Maximum
        canvas.drawText(Float.toString(maxValue50), rect50HzY.right,
                bound50.top - paint50value.getTextSize() / 4, paint50value);
        canvas.drawText(Float.toString(maxValue100), rect100HzY.right,
                bound100.top - paint100value.getTextSize() / 4, paint100value);
    }

    public void setBorder(float length, float width) {

        invalidate();

    }

    public void initializeCanvas() {

        // Background
        boundBG = new Rect(0, 0, (int) screenwidth, (int) screenheight);
        // background = new GradientDrawable(
        // GradientDrawable.Orientation.TOP_BOTTOM, new int[] {
        // Color.WHITE, Color.GRAY });
        // background.setShape(GradientDrawable.LINEAR_GRADIENT);
        // background.setGradientRadius((float) (Math.sqrt(2) * 60));
        // background=new Drawable();
        // background.setBounds(boundBG);
        this.setBackgroundColor(Color.WHITE);

        // Blocks for 50 and 100 Hz
        bar50left = 0.05f * screenwidth;
        bar50right = 0.45f * screenwidth;
        bar50bottom = 0.90f * screenheight;
        bar50top = 0.1f * screenheight;

        bar100left = 0.55f * screenwidth;
        bar100right = 0.95f * screenwidth;
        bar100bottom = 0.90f * screenheight;
        bar100top = 0.1f * screenheight;

        bar50Middle = (bar50right - bar50left) / 2 + bar50left;
        bar100Middle = (bar100right - bar100left) / 2 + bar100left;

        rect50HzX = new RectF(bar50left, barOffset,
                (float) (bar50left + (bar50right - bar50left) / 4.0) - 2,
                bar50bottom);
        rect50HzY = new RectF(
                (float) (bar50left + (bar50right - bar50left) / 4.0) + 2,
                barOffset,
                (float) (bar50left + (bar50right - bar50left) * 2.0 / 4.0) - 2,
                bar50bottom);
        rect50HzZ = new RectF(
                (float) (bar50left + (bar50right - bar50left) * 2.0 / 4.0) + 2,
                barOffset,
                (float) (bar50left + (bar50right - bar50left) * 3.0 / 4.0) - 2,
                bar50bottom);
        rect50HzS = new RectF(
                (float) (bar50left + (bar50right - bar50left) * 3.0 / 4.0) + 2,
                barOffset, bar50right, bar50bottom);
        rect100HzX = new RectF(bar100left, barOffset,
                (float) (bar100left + (bar100right - bar100left) / 4.0) - 2,
                bar100bottom);
        rect100HzY = new RectF(
                (float) (bar100left + (bar100right - bar100left) / 4.0) + 2,
                barOffset,
                (float) (bar100left + (bar100right - bar100left) * 2.0 / 4.0) - 2,
                bar100bottom);
        rect100HzZ = new RectF(
                (float) (bar100left + (bar100right - bar100left) * 2.0 / 4.0) + 2,
                barOffset,
                (float) (bar100left + (bar100right - bar100left) * 3.0 / 4.0) - 2,
                bar100bottom);
        rect100HzS = new RectF(
                (float) (bar100left + (bar100right - bar100left) * 3.0 / 4.0) + 2,
                barOffset, bar100right, bar100bottom);

        paint50barX = new Paint();
        paint50barY = new Paint();
        paint50barZ = new Paint();
        paint50barS = new Paint();
        paint100barX = new Paint();
        paint100barY = new Paint();
        paint100barZ = new Paint();
        paint100barS = new Paint();
        paint50barX.setColor(getResources().getColor(R.color.lightred));
        paint50barY.setColor(getResources().getColor(R.color.lightgreen));
        paint50barZ.setColor(getResources().getColor(R.color.lightblue));
        paint50barS.setColor(getResources().getColor(R.color.lightorange));
        paint100barX.setColor(getResources().getColor(R.color.lightred));
        paint100barY.setColor(getResources().getColor(R.color.lightgreen));
        paint100barZ.setColor(getResources().getColor(R.color.lightblue));
        paint100barS.setColor(getResources().getColor(R.color.lightorange));

        // Bar Background Shape
        bound50 = new Rect((int) (bar50left), (int) (bar50top),
                (int) (bar50right), (int) (bar50bottom));

        bound100 = new Rect((int) (bar100left), (int) (bar100top),
                (int) (bar100right), (int) (bar100bottom));

        paintBG50 = new Paint();
        paintBG100 = new Paint();
        paintBG50.setStrokeWidth(0);
        paintBG100.setStrokeWidth(0);
        paintBG50.setColor(getResources().getColor(R.color.lightgray));
        paintBG100.setColor(getResources().getColor(R.color.lightgray));

        // Text Paint
        text50Y = 0.98f * screenheight;
        paintText50 = new Paint();
        paintText50.setColor(getResources().getColor(R.color.black));
        paintText50.setTextSize(35);
        paintText50.setAntiAlias(true);

        text100Y = 0.98f * screenheight;
        paintText100 = new Paint();
        paintText100.setColor(getResources().getColor(R.color.black));
        paintText100.setTextSize(35);
        paintText100.setAntiAlias(true);

        titleY = 0.03f * screenheight;
        paintTitle = new Paint();
        paintTitle.setColor(getResources().getColor(R.color.black));
        paintTitle.setTextSize(40);
        paintTitle.setAntiAlias(true);
        paintTitle.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        int size = 40;
        while (paintTitle.measureText(textTitle) > screenwidth) {
            size--;
            paintTitle.setTextSize(size);
        }

        paint50value = new Paint();
        paint50value.setColor(getResources().getColor(R.color.black));
        paint50value.setTextSize(20);
        paint50value.setStrokeWidth(3);
        paint50value.setAntiAlias(true);
        paint50value.setTextAlign(Paint.Align.CENTER);

        paint100value = new Paint();
        paint100value.setColor(getResources().getColor(R.color.black));
        paint100value.setTextSize(20);
        paint100value.setStrokeWidth(3);
        paint100value.setAntiAlias(true);
        paint100value.setTextAlign(Paint.Align.CENTER);

    }

    public void setBars(float[] values) {

        rect50HzX.top = barOffset - barMaxHeight * (values[0] / maxValue50);
        rect50HzY.top = barOffset - barMaxHeight * (values[1] / maxValue50);
        rect50HzZ.top = barOffset - barMaxHeight * (values[2] / maxValue50);
        rect50HzS.top = barOffset - barMaxHeight * (values[3] / maxValue50);
        rect100HzX.top = barOffset - barMaxHeight * (values[4] / maxValue100);
        rect100HzY.top = barOffset - barMaxHeight * (values[5] / maxValue100);
        rect100HzZ.top = barOffset - barMaxHeight * (values[6] / maxValue100);
        rect100HzS.top = barOffset - barMaxHeight * (values[7] / maxValue100);
        value50x = Math.round(values[0] * 100f) / 100f;
        value50y = Math.round(values[1] * 100f) / 100f;
        value50z = Math.round(values[2] * 100f) / 100f;
        value50s = Math.round(values[3] * 100f) / 100f;
        value100x = Math.round(values[4] * 100f) / 100f;
        value100y = Math.round(values[5] * 100f) / 100f;
        value100z = Math.round(values[6] * 100f) / 100f;
        value100s = Math.round(values[7] * 100f) / 100f;
        invalidate();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        screenwidth = MeasureSpec.getSize(widthMeasureSpec);
        screenheight = MeasureSpec.getSize(heightMeasureSpec);

        Log.v("philipba", "measure");

        barOffset = 0.9f * screenheight;
        barMaxHeight = 13f / 20f * screenheight;

        initializeCanvas();
    }

}
