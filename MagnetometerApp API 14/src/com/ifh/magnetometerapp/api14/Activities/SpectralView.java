package com.ifh.magnetometerapp.api14.Activities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.*;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;
import com.androidplot.Plot.BorderStyle;
import com.androidplot.ui.AnchorPosition;
import com.androidplot.ui.DynamicTableModel;
import com.androidplot.ui.SizeLayoutType;
import com.androidplot.ui.SizeMetrics;
import com.androidplot.xy.*;
import com.ifh.magnetometerapp.api14.Background.BackgroundService;
import com.ifh.magnetometerapp.api14.R;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.LinkedList;

public class SpectralView extends Activity {

    // Request ID's
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private static final int REQUEST_SAVE_MEASURE = 3;
    private static final int REQUEST_OPTIONS_DIALOG = 4;

    // Plot widget
    private XYPlot livePlot;
    private SimpleXYSeries spectrumLinePlotX = null;
    private SimpleXYSeries spectrumLinePlotY = null;
    private SimpleXYSeries spectrumLinePlotZ = null;
    private SimpleXYSeries spectrumLinePlotS = null;
    DisplayMetrics dm = new DisplayMetrics();
    LinkedList<Number> spectrumX = new LinkedList<Number>();
    LinkedList<Number> spectrumY = new LinkedList<Number>();
    LinkedList<Number> spectrumZ = new LinkedList<Number>();
    LinkedList<Number> spectrumS = new LinkedList<Number>();
    private SeekBar changeBoundariesSeekBar;
    private ActionBar actionBar;

    // Broadcast
    private final IntentFilter FFT_UPDATE_FILTER = new IntentFilter(
            BackgroundService.FFT_BROADCAST);
    private BackgroundService syncService;

    // Take screenshot variables
    private Bitmap screenshot;
    private FileOutputStream out;
    private String filename;
    private Date date;

    // Preferences
    private SharedPreferences prefs;
    private Editor prefsEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // set layout
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.spectrum_view);

        getWindowManager().getDefaultDisplay().getMetrics(dm);

        // Get shared preferences (f.e. calibration status)
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefsEditor = prefs.edit();

        // Initialize Widgets

        actionBar = this.getActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle("SPECTRAL VIEW");
        livePlot = (XYPlot) findViewById(R.id.spectrumPlot);
        changeBoundariesSeekBar = (SeekBar) findViewById(R.id.spectrumview_changeboundaries);
        changeBoundariesSeekBar.setMax(50);
        changeBoundariesSeekBar.setProgress(25);
        changeBoundariesSeekBar
                .setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {

                        if (progress != changeBoundariesSeekBar.getMax()
                                && progress != 0) {
                            livePlot.setRangeBoundaries(
                                    0,
                                    changeBoundariesSeekBar.getMax()
                                            - changeBoundariesSeekBar
                                            .getProgress(),
                                    BoundaryMode.FIXED);
                            //
                            // editLowerBound.setText(String
                            // .valueOf(currentLowerBound));
                            // editUpperBound.setText(String
                            // .valueOf(currentUpperBound));
                            //
                            livePlot.redraw();
                        }

                    }

                    public void onStartTrackingTouch(SeekBar seekBar) {
                    }

                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }

                });

        initializePlots();
        // Bind BackgroundService with this Activity
        getApplicationContext().bindService(
                new Intent(SpectralView.this, BackgroundService.class),
                serviceConnection, Context.BIND_AUTO_CREATE);

    }

    protected void onResume() {
        super.onResume();
        registerReceiver(fftUpdateReceiver, FFT_UPDATE_FILTER);

        int progress = prefs.getInt(Settings.SPECTRALVIEW_PROGRESS_KEY,
                changeBoundariesSeekBar.getMax() / 2);
        changeBoundariesSeekBar.setProgress(progress);

        livePlot.redraw();
    }

    @Override
    protected void onStop() {
        // unregister receivers
        unregisterReceiver(fftUpdateReceiver);

        super.onStop();
    }

    protected void onPause() {

        prefsEditor.putInt(Settings.SPECTRALVIEW_PROGRESS_KEY,
                changeBoundariesSeekBar.getProgress());
        prefsEditor.commit();
        super.onPause();
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className, IBinder service) {
            syncService = ((BackgroundService.LocalBinder) service)
                    .getService();
            Log.v("philipba", "service connected");
            /*if (syncService.valuesFFTX != null) {
                double[] values = syncService.valuesFFTX.clone();
				double[] frequency = syncService.freqValuesX.clone();
				spectrumX.clear();
				for (int varI = 0; varI < values.length; varI++) {
					spectrumX.add(frequency[varI]);
					spectrumX.add(values[varI]);
				}
				spectrumLinePlotX.setModel(spectrumX,
						SimpleXYSeries.ArrayFormat.XY_VALS_INTERLEAVED);

				values = syncService.valuesFFTY.clone();
				frequency = syncService.freqValuesY.clone();
				spectrumY.clear();
				for (int varI = 0; varI < values.length; varI++) {
					spectrumY.add(frequency[varI]);
					spectrumY.add(values[varI]);
				}
				spectrumLinePlotY.setModel(spectrumY,
						SimpleXYSeries.ArrayFormat.XY_VALS_INTERLEAVED);

				spectrumZ.clear();
				spectrumS.clear();
				values = syncService.valuesFFTZ.clone();
				frequency = syncService.freqValuesZ.clone();
				for (int varI = 0; varI < values.length; varI++) {
					spectrumZ.add(frequency[varI]);
					spectrumZ.add(values[varI]);
					spectrumS.add(frequency[varI]);
					spectrumS.add(Math.sqrt(values[varI] * values[varI]
							+ spectrumX.get(varI * 2 + 1).doubleValue()
							* spectrumX.get(varI * 2 + 1).doubleValue()
							+ spectrumY.get(varI * 2 + 1).doubleValue()
							* spectrumY.get(varI * 2 + 1).doubleValue()));

					spectrumLinePlotZ.setModel(spectrumZ,
							SimpleXYSeries.ArrayFormat.XY_VALS_INTERLEAVED);
					spectrumLinePlotS.setModel(spectrumS,
							SimpleXYSeries.ArrayFormat.XY_VALS_INTERLEAVED);

					// livePlot.setTicksPerDomainLabel(5);
					livePlot.redraw();

				}
			}*/
        }

        public void onServiceDisconnected(ComponentName className) {
            syncService = null;
        }
    };

    private final BroadcastReceiver fftUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            // double [] values =
            // intent.getExtras().getDoubleArray(BackgroundService.FFT_BROADCAST);
            // double [] frequency =
            // intent.getExtras().getDoubleArray(BackgroundService.FFT_FREQUENCY);
            double[] frequency = intent
                    .getDoubleArrayExtra(BackgroundService.FFT_FREQUENCY);
            // Get the amplitude of the fft.
            double[] values = intent.getExtras().getDoubleArray(
                    BackgroundService.FFT_VALUES);
            // Get the sensor axis to which the data belongs.
            char axis = intent.getExtras().getChar(BackgroundService.FFT_AXIS);

            double yMax = 0;

            final int valuesLength = values.length / 2;
            if (axis == 'X') {
                spectrumX.clear();
                for (int varI = 0; varI < valuesLength; varI++) {
                    spectrumX.add(frequency[varI]);
                    spectrumX.add(values[varI]);
                }
                spectrumLinePlotX.setModel(spectrumX,
                        SimpleXYSeries.ArrayFormat.XY_VALS_INTERLEAVED);
            } else if (axis == 'Y') {
                spectrumY.clear();
                for (int varI = 0; varI < valuesLength; varI++) {
                    spectrumY.add(frequency[varI]);
                    spectrumY.add(values[varI]);
                }
                spectrumLinePlotY.setModel(spectrumY,
                        SimpleXYSeries.ArrayFormat.XY_VALS_INTERLEAVED);
            } else if (axis == 'Z') {
                spectrumZ.clear();
                spectrumS.clear();
                for (int varI = 0; varI < valuesLength; varI++) {
                    spectrumZ.add(frequency[varI]);
                    spectrumZ.add(values[varI]);
                    spectrumS.add(frequency[varI]);
                    spectrumS.add(Math.sqrt(values[varI] * values[varI]
                            + spectrumX.get(varI * 2 + 1).doubleValue()
                            * spectrumX.get(varI * 2 + 1).doubleValue()
                            + spectrumY.get(varI * 2 + 1).doubleValue()
                            * spectrumY.get(varI * 2 + 1).doubleValue()));

                    if (spectrumS.getLast().doubleValue() > yMax) {
                        yMax = spectrumS.getLast().doubleValue();
                    }
                }
                spectrumLinePlotZ.setModel(spectrumZ,
                        SimpleXYSeries.ArrayFormat.XY_VALS_INTERLEAVED);
                spectrumLinePlotS.setModel(spectrumS,
                        SimpleXYSeries.ArrayFormat.XY_VALS_INTERLEAVED);

                livePlot.setDomainBoundaries(0, syncService.samplingFrequency / 2,
                        BoundaryMode.FIXED);
                livePlot.setRangeBoundaries(0, 2 * yMax,
                        BoundaryMode.FIXED);
                livePlot.setDomainStep(XYStepMode.INCREMENT_BY_VAL, 10);

                livePlot.setTicksPerDomainLabel(5);

                livePlot.setDomainValueFormat(new DecimalFormat("0.0"));
                livePlot.setRangeValueFormat(new DecimalFormat("0.00"));
                livePlot.redraw();

            }

            // liveValSeries.setModel(spectrum,
            // SimpleXYSeries.ArrayFormat.Y_VALS_ONLY);

            // livePlot.setDomainStep(XYStepMode.SUBDIVIDE,values.length);
        }
    };

    public void initializePlots() {
        // Turn the above arrays into XYSeries:
        spectrumLinePlotX = new SimpleXYSeries("x-Axis");
        spectrumLinePlotY = new SimpleXYSeries("y-Axis");
        spectrumLinePlotZ = new SimpleXYSeries("z-Axis");
        spectrumLinePlotS = new SimpleXYSeries("total");

        // Create a formatter to use for drawing a series using
        // LineAndPointRenderer:
        /*
         * LineAndPointFormatter liveSeriesFormat = new LineAndPointFormatter(
		 * Color.rgb(0, 0, 128), // line color Color.rgb(100, 149, 237), //
		 * point color Color.argb(0, 0, 0, 0)); // fill color (optional)
		 */
        LineAndPointFormatter linePlotFormatX = new LineAndPointFormatter(
                Color.rgb(255, 0, 0), // line color
                Color.argb(0, 0, 255, 0), // point color
                Color.argb(0, 100, 149, 237)); // fill color (optional)
        LineAndPointFormatter linePlotFormatY = new LineAndPointFormatter(
                Color.rgb(0, 255, 0), // line color
                Color.argb(0, 0, 255, 0), // point color
                Color.argb(0, 100, 149, 237)); // fill color (optional)
        LineAndPointFormatter linePlotFormatZ = new LineAndPointFormatter(
                Color.rgb(0, 0, 255), // line color
                Color.argb(0, 0, 255, 0), // point color
                Color.argb(0, 100, 149, 237)); // fill color (optional)
        LineAndPointFormatter linePlotFormatS = new LineAndPointFormatter(
                Color.rgb(255, 234, 0), // line color
                Color.argb(0, 0, 255, 0), // point color
                Color.argb(0, 100, 149, 237)); // fill color (optional)

        // liveSeriesFormat.setVertexPaint(null);
        /*linePlotFormatX.getLinePaint().setStrokeWidth(
                Math.round(dm.densityDpi
                        * ((float) getApplicationContext().getResources()
                        .getInteger(R.integer.plot_line_size_inc_4))
                        / 10000));
        linePlotFormatY.getLinePaint().setStrokeWidth(
                Math.round(dm.densityDpi
                        * ((float) getApplicationContext().getResources()
                        .getInteger(R.integer.plot_line_size_inc_4))
                        / 10000));
        linePlotFormatZ.getLinePaint().setStrokeWidth(
                Math.round(dm.densityDpi
                        * ((float) getApplicationContext().getResources()
                        .getInteger(R.integer.plot_line_size_inc_4))
                        / 10000));
        linePlotFormatS.getLinePaint().setStrokeWidth(
                Math.round(dm.densityDpi
                        * ((float) getApplicationContext().getResources()
                        .getInteger(R.integer.plot_line_size_inc_4))
                        / 10000));*/

        // Add series1 to the xyplot:
        livePlot.addSeries(spectrumLinePlotX, linePlotFormatX);
        livePlot.addSeries(spectrumLinePlotY, linePlotFormatY);
        livePlot.addSeries(spectrumLinePlotZ, linePlotFormatZ);
        livePlot.addSeries(spectrumLinePlotS, linePlotFormatS);

        // Reduce the number of range labels
        livePlot.setTicksPerRangeLabel(1);
        livePlot.setRangeStep(XYStepMode.SUBDIVIDE, 6);
        livePlot.setDomainStep(XYStepMode.INCREMENT_BY_VAL, 45);

        livePlot.setDomainLabel("frequency [Hz]");
        livePlot.setRangeLabel("magnetic Field [�T]");

        livePlot.setPlotMargins(10, 10, 10, 10);
        livePlot.setPlotPadding(10, 10, 10, 10);

        livePlot.setRangeBoundaries(0,
                changeBoundariesSeekBar.getProgress() + 1, BoundaryMode.GROW);

        // Axis Labels
        livePlot.getDomainLabelWidget().getLabelPaint().setTextSize(20);
        livePlot.getRangeLabelWidget().getLabelPaint().setTextSize(20);
        livePlot.getDomainLabelWidget().setHeight(20);
        livePlot.getRangeLabelWidget().setHeight(200);
        livePlot.getDomainLabelWidget().setWidth(2000);
        livePlot.getRangeLabelWidget().setWidth(25);
        livePlot.getRangeLabelWidget().setMarginLeft(0);
        // livePlot.getDomainLabelWidget().setWidth(10, )
        // livePlot.getMeasuredWidth()/2;
        livePlot.getTitleWidget().getLabelPaint().setTextSize(20);
        livePlot.getTitleWidget().setWidth(200);
        livePlot.getTitleWidget().setHeight(20);
        livePlot.getGraphWidget().getRangeLabelPaint().setTextSize(15);
        livePlot.getGraphWidget().getDomainLabelPaint().setTextSize(15);
        livePlot.getGraphWidget().getRangeOriginLabelPaint().setTextSize(15);
        livePlot.getGraphWidget().getDomainOriginLabelPaint().setTextSize(15);
        livePlot.position(livePlot.getDomainLabelWidget(), 0,
                XLayoutStyle.ABSOLUTE_FROM_CENTER, -10,
                YLayoutStyle.ABSOLUTE_FROM_BOTTOM, AnchorPosition.BOTTOM_MIDDLE);

        // Legend Widget
        livePlot.getLegendWidget().setTableModel(new DynamicTableModel(2, 2));
        livePlot.getLegendWidget().setSize(
                new SizeMetrics(80, SizeLayoutType.ABSOLUTE, 200,
                        SizeLayoutType.ABSOLUTE));
        Paint bgPaint = new Paint();
        bgPaint.setColor(Color.BLACK);
        bgPaint.setStyle(Paint.Style.FILL);
        bgPaint.setAlpha(140);
        livePlot.getLegendWidget().setBackgroundPaint(bgPaint);
        livePlot.getLegendWidget().setPadding(10, 1, 1, 1);
        livePlot.position(livePlot.getLegendWidget(), 20,
                XLayoutStyle.ABSOLUTE_FROM_RIGHT, 35,
                YLayoutStyle.ABSOLUTE_FROM_TOP, AnchorPosition.RIGHT_TOP);
        livePlot.getLegendWidget().getTextPaint().setTextSize(20);
        livePlot.getLegendWidget().setIconSizeMetrics(
                new SizeMetrics(20, SizeLayoutType.ABSOLUTE, 20,
                        SizeLayoutType.ABSOLUTE));
        // By default, AndroidPlot displays developer guides to aid in
        // laying out your plot.
        // To get rid of them call disableAllMarkup():
        livePlot.getGraphWidget().setMarginTop(10);
        livePlot.getGraphWidget().setMarginLeft(10);
        livePlot.getGraphWidget().setMarginRight(2);
        livePlot.setBorderStyle(BorderStyle.SQUARE, null, null);
        livePlot.disableAllMarkup();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.spectral_view_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.screenshot_spectral:
                takeScreenshot();
                return true;
            case R.id.menu_settings:
                Intent intent = new Intent(SpectralView.this, Settings.class);
                startActivity(intent);
                return true;
            case R.id.trigger_spectral:
                triggerMeasurement();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void takeScreenshot() {
        // Get instance of Vibrator from current Context
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        // Vibrate for 300 milliseconds
        v.vibrate(300);

        File exportsDirectory = new File(BackgroundService.SCREENSHOTS_PATH);
        exportsDirectory.mkdirs();

        livePlot.setDrawingCacheEnabled(true);

        screenshot = livePlot.getDrawingCache();

        date = new Date();

        filename = BackgroundService.SCREENSHOTS_PATH + "Screenshot_"
                + DateFormat.getDateInstance().format(new Date()) + "_"
                + date.getHours() + "." + date.getMinutes() + "."
                + date.getSeconds() + ".png";

        try {
            out = new FileOutputStream(filename);
            screenshot.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.flush();
            out.close();
            Toast.makeText(this,
                    "Screenshot succesfully saved to: " + filename,
                    Toast.LENGTH_LONG).show();
            Uri uri = Uri.fromFile(exportsDirectory);
            Intent scanFileIntent = new Intent(
                    Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri);
            sendBroadcast(scanFileIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void triggerMeasurement() {

        if (syncService.connectionStatus == 2) {
            Log.v("MapView", "start trigger");

            byte[] out = {64};
            syncService.sendMessage(out);

            // Get instance of Vibrator from current Context
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

            // Vibrate for 300 milliseconds
            v.vibrate(200);
            Toast.makeText(getApplicationContext(), "Measurement triggered", Toast.LENGTH_SHORT)
                    .show();

        } else {
            Toast.makeText(getApplicationContext(), "No connection", Toast.LENGTH_SHORT).show();
        }
    }

}
