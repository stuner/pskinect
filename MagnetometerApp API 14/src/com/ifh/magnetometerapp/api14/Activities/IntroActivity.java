package com.ifh.magnetometerapp.api14.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import com.ifh.magnetometerapp.api14.R;

public class IntroActivity extends Activity {
    private final int SPLASH_DISPLAY_LENGTH = 1100;
    private LinearLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.intro);

        layout = (LinearLayout) findViewById(R.id.intro_layout);
    }

    @Override
    protected void onResume() {
        super.onResume();
        final Animation animationFadeIn = AnimationUtils.loadAnimation(this,
                R.anim.fadein_animation);
        animationFadeIn.setAnimationListener(new AnimationListener() {

            public void onAnimationEnd(Animation arg0) {
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        // Finish the splash activity so it can't be returned to.
                        IntroActivity.this.finish();
                        // Create an Intent that will start the main activity.
                        Intent mainIntent = new Intent(IntroActivity.this,
                                MainView.class);
                        IntroActivity.this.startActivity(mainIntent);
                    }
                }, SPLASH_DISPLAY_LENGTH);
            }

            public void onAnimationRepeat(Animation arg0) {


            }

            public void onAnimationStart(Animation arg0) {


            }

        });
        layout.startAnimation(animationFadeIn);
    }
}
