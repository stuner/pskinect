package com.ifh.magnetometerapp.api14.Activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.DialogPreference;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import com.ifh.magnetometerapp.api14.Background.BackgroundService;
import com.ifh.magnetometerapp.api14.R;

public class SpectralViewSelection extends DialogPreference {

    private BackgroundService syncService;

    private Context context;
    private NumberPicker numbPicker50, numbPicker100;
    private TextView title50, title100;
    private SharedPreferences prefs;
    private Editor editor;
    private int value50, value100;
    private LinearLayout mainLayout;

    public SpectralViewSelection(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        editor = prefs.edit();

        this.setDialogLayoutResource(R.layout.spectralviewselection);

    }

    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);
        numbPicker50 = (NumberPicker) view.findViewById(R.id.numberPicker50);
        numbPicker100 = (NumberPicker) view.findViewById(R.id.numberPicker100);
        value50 = prefs.getInt(Settings.SPECTRALVIEW_MAX50_KEY, 5);
        value100 = prefs.getInt(Settings.SPECTRALVIEW_MAX100_KEY, 5);
        numbPicker50.setMinValue(1);
        numbPicker50.setMaxValue(120);
        numbPicker50.setValue(value50);
        numbPicker100.setMinValue(1);
        numbPicker100.setMaxValue(120);
        numbPicker100.setValue(value100);

    }

    @Override
    protected View onCreateDialogView() {
        return super.onCreateDialogView();
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {

        if (!positiveResult)
            return;

        editor.putInt(Settings.SPECTRALVIEW_MAX50_KEY, numbPicker50.getValue());
        editor.putInt(Settings.SPECTRALVIEW_MAX100_KEY,
                numbPicker100.getValue());
        editor.commit();
        setTheSummary("50Hz: " + numbPicker50.getValue() + " µT / " + "100Hz: "
                + numbPicker100.getValue() + " µT");

        super.onDialogClosed(positiveResult);
    }

    public void setTheSummary(String summary) {
        setSummary(summary);
    }

}
