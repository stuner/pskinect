package com.ifh.magnetometerapp.api14.Activities;

import android.app.AlertDialog.Builder;
import android.content.*;
import android.content.DialogInterface.OnClickListener;
import android.os.IBinder;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import com.ifh.magnetometerapp.api14.Background.BackgroundService;

public class GainSelection extends DialogPreference {

    private BackgroundService syncService;

    private Context context;
    private SeekBar gainLevel;
    private TextView title;
    private TextView value;
    private String stringTitle;
    private int maxProgress;

    public GainSelection(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        context.getApplicationContext().bindService(
                new Intent(context, BackgroundService.class),
                serviceConnection, Context.BIND_AUTO_CREATE);
        stringTitle = "Change Gain";
        maxProgress = 255;
    }

    @Override
    protected View onCreateDialogView() {
        return super.onCreateDialogView();
    }

    protected void onPrepareDialogBuilder(Builder builder) {

        LinearLayout layout = new LinearLayout(context);
        layout.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        layout.setMinimumWidth(400);
        layout.setOrientation(LinearLayout.VERTICAL);
        // layout.setPadding(20, 20, 20, 20);

        title = new TextView(context);
        title.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        title.setText(stringTitle);
        title.setGravity(Gravity.CENTER_HORIZONTAL);

        layout.addView(title);

        gainLevel = new SeekBar(context);
        gainLevel.setMax(maxProgress);
        gainLevel.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        gainLevel.setProgress(syncService.gain);
        gainLevel.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                double gain = (getGain(progress) / getGain(BackgroundService.GAIN_DEFAULT)) * 1000.0;

                String temp = String.valueOf((Math.round(gain)) / 1000.0);
                value.setText(temp + " times default gain");
            }

            public void onStartTrackingTouch(SeekBar seekBar) {


            }

            public void onStopTrackingTouch(SeekBar seekBar) {


            }

        });

        layout.addView(gainLevel);

        value = new TextView(context);
        value.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        String temp = String.valueOf(getGain(gainLevel.getProgress()) / getGain(BackgroundService.GAIN_DEFAULT));
        value.setText(temp + " times default gain");

        value.setGravity(Gravity.CENTER_HORIZONTAL);

        layout.addView(value);

        builder.setView(layout);

        builder.setNegativeButton("Cancel", new OnClickListener() {

            public void onClick(DialogInterface arg0, int arg1) {


            }

        });
        builder.setNeutralButton("Default", new OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {

                gainLevel.setProgress(BackgroundService.GAIN_DEFAULT);
                syncService.gain = BackgroundService.GAIN_DEFAULT;
                if (syncService.connectionStatus == 2) {
                    setGain();
                }
            }

        });
        builder.setPositiveButton("OK", new OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {

                if (syncService.connectionStatus == 2) {
                    setGain();
                }
            }

        });

        super.onPrepareDialogBuilder(builder);
    }


    public void setGain() {
        try {
            int p = gainLevel.getProgress();
            syncService.gain = p;

            byte out[] = {(byte) Settings.SET_GAIN_ID,
                    (byte) gainLevel.getProgress()};

            syncService.sendMessage(out);

            Toast.makeText(context, "Gain succesfully set", Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            Toast.makeText(context, "Setting Gain failed!", Toast.LENGTH_SHORT).show();
        }
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className, IBinder service) {
            syncService = ((BackgroundService.LocalBinder) service)
                    .getService();
        }

        public void onServiceDisconnected(ComponentName className) {
            syncService = null;
        }
    };

    public double getGain(int progress) {

        double resistance = (double) (progress) * 10000.0 / 255.0 + 75;
        double gain = 49400 / resistance + 1;
        Log.v("philipba", "gain " + gain);
        return gain;
    }
}
