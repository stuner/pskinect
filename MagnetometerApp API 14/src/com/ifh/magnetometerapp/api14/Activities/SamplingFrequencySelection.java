package com.ifh.magnetometerapp.api14.Activities;

import android.app.AlertDialog.Builder;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import com.ifh.magnetometerapp.api14.Background.BackgroundService;

import java.text.DecimalFormat;

public class SamplingFrequencySelection extends DialogPreference {

    private BackgroundService syncService;

    private Context context;
    private SeekBar samplingFrequencyDigital;
    private TextView title;
    private TextView value;
    private int id;
    private String stringTitle;
    private int maxProgress;
    private int minProgress;
    private double frequency;

    public SamplingFrequencySelection(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        context.getApplicationContext().bindService(
                new Intent(context, BackgroundService.class),
                serviceConnection, Context.BIND_AUTO_CREATE);
        stringTitle = "Change Sampling Frequency";
        // Set the minimum and maximum digital value for the
        // sampling frequency.
        minProgress = 7;
        maxProgress = 255;
    }

    protected void onPrepareDialogBuilder(Builder builder) {

        LinearLayout layout = new LinearLayout(context);
        layout.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.FILL_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        layout.setMinimumWidth(400);
        layout.setOrientation(LinearLayout.VERTICAL);
        // layout.setPadding(20, 20, 20, 20);

        title = new TextView(context);
        title.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.FILL_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        title.setText(stringTitle);
        title.setGravity(Gravity.CENTER_HORIZONTAL);

        layout.addView(title);

        samplingFrequencyDigital = new SeekBar(context);
        samplingFrequencyDigital.setMax(maxProgress - minProgress);
        samplingFrequencyDigital.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        samplingFrequencyDigital.setProgress(syncService.uControllerBurstSamplingRate - minProgress);
        samplingFrequencyDigital.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                DecimalFormat decFormat = new DecimalFormat("###.###");
                frequency = BackgroundService.U_CONTROLLER_CPU_FREQ
                        / (double) BackgroundService.U_CONTROLLER_TIMER0_PRESCALER
                        / (progress + minProgress + 1.0);
                value.setText(frequency + " Hz");
            }

            public void onStartTrackingTouch(SeekBar seekBar) {


            }

            public void onStopTrackingTouch(SeekBar seekBar) {


            }

        });

        layout.addView(samplingFrequencyDigital);

        value = new TextView(context);
        value.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.FILL_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        DecimalFormat decFormat = new DecimalFormat("###.###");
        value.setText(decFormat.format(BackgroundService.U_CONTROLLER_CPU_FREQ
                / (double) BackgroundService.U_CONTROLLER_TIMER0_PRESCALER
                / (samplingFrequencyDigital.getProgress() + minProgress + 1.0)) + " Hz");

        value.setGravity(Gravity.CENTER_HORIZONTAL);

        layout.addView(value);

        builder.setView(layout);

        super.onPrepareDialogBuilder(builder);
    }

    protected void onDialogClosed(boolean positiveResult) {
        if (positiveResult) {
            if (syncService.connectionStatus == 2) {
                setSamplingFrequency();
                syncService.samplingFrequency = frequency;
            }
        }

    }

    public void setSamplingFrequency() {
        try {

            int samplingFrequency = samplingFrequencyDigital.getProgress() + minProgress;

            syncService.uControllerBurstSamplingRate = samplingFrequency;

//			byte d1 = (byte) ((p & 0xFF00) >> 8);
            byte d2 = (byte) (samplingFrequency & 0x00FF);

            byte out[] = {(byte) Settings.SET_SAMPLINGFREQUENCY_ID,
                    d2};

            syncService.sendMessage(out);

            Toast.makeText(context, "Sampling Frequency succesfully set", Toast.LENGTH_SHORT)
                    .show();

        } catch (Exception e) {
            Toast.makeText(context, "Setting Sampling Frequency failed!", Toast.LENGTH_SHORT)
                    .show();
        }
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className, IBinder service) {
            syncService = ((BackgroundService.LocalBinder) service)
                    .getService();
        }

        public void onServiceDisconnected(ComponentName className) {
            syncService = null;
        }
    };

}
