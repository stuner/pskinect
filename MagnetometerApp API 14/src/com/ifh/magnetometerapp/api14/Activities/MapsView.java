package com.ifh.magnetometerapp.api14.Activities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.*;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Toast;
import com.androidnatic.maps.HeatMapOverlay;
import com.androidnatic.maps.model.HeatPoint;
import com.google.android.maps.*;
import com.ifh.magnetometerapp.api14.Background.BackgroundService;
import com.ifh.magnetometerapp.api14.Background.MeasurementPoint;
import com.ifh.magnetometerapp.api14.Background.MyMapView;
import com.ifh.magnetometerapp.api14.R;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MapsView extends MapActivity {

    // To get the GPS position
    private LocationManager locManager;
    private LocationListener locListener = new MyLocationListener();

    private boolean gpsEnabled = false;
    private boolean networkEnabled = false;
    private String currentProvider;
    private long systemToGpsTimeOffset = Long.MAX_VALUE;
    private double gpsAccuracy;
    private HeatMapOverlay heatMapOverlay;
    public MyMapView mapView;
    private ArrayList<HeatPoint> points;
    private OverlayItem item;
    private boolean providerSet = false;

    public MeasurementPoint currentMeasurement;
    public HeatPoint currentHeatPoint;
    private Location currentLocation = null;

    protected static final Activity MACTIVITY = null;
    private IntentFilter mapViewBroadcastFilters = new IntentFilter(
            BackgroundService.FFT_BROADCAST);
    private BackgroundService syncService;
    private boolean serviceConnected = false;
    private MapController mapController;
    private int pointRadius = 10;
    private ActionBar actionBar;

    // Take screenshot variables
    private Bitmap screenshot;
    private FileOutputStream out;
    private String filename;
    private Date date;

    private SharedPreferences prefs;

    @Override
    protected boolean isRouteDisplayed() {

        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.mapview);

        mapView = (MyMapView) findViewById(R.id.mapview);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        // ActionBar
        actionBar = this.getActionBar();
        actionBar.setTitle("MAP VIEW");
        actionBar.setDisplayShowHomeEnabled(false);

        locManager = (LocationManager) this
                .getSystemService(Context.LOCATION_SERVICE);

        heatMapOverlay = new HeatMapOverlay(pointRadius, mapView);
        mapView.getOverlays().add(heatMapOverlay);
        mapView.setBuiltInZoomControls(true);
        mapController = mapView.getController();
        mapView.setOnChangeListener(new MapViewChangeListener());

        // Bind BackgroundService to this Activity
        bindService(new Intent(this, BackgroundService.class),
                serviceConnection, Context.BIND_AUTO_CREATE);

    }

    @Override
    protected void onDestroy() {

        super.onStop();
        Log.d("IFH MapView", "onPause start");
        locManager.removeUpdates(locListener);

        unregisterReceiver(mapViewBroadcastRecievers);
        unbindService(serviceConnection);
        Log.d("IFH MapView", "onPause stop");
    }

    @Override
    protected void onResume() {

        super.onResume();

        Log.v("philipba", "resume");

        registerReceiver(mapViewBroadcastRecievers, mapViewBroadcastFilters);
        // exceptions will be thrown if provider is not permitted.
        getGPSStatus();

        // Check if Satellite is enabled
        boolean sat = prefs.getBoolean(Settings.SATTELITEMAP_PREFERENCE_KEY,
                false);
        mapView.setSatellite(sat);

        if (!serviceConnected) {
            points = new ArrayList<HeatPoint>();

        } else {
            if (syncService.mapList != null) {
                points = syncService.mapList;
                updateMapOverlay();
            } else {
                points = new ArrayList<HeatPoint>();
            }
        }

    }

    @Override
    protected void onPause() {
        syncService.mapList = points;
        super.onPause();

    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className, IBinder service) {

            syncService = ((BackgroundService.LocalBinder) service)
                    .getService();
            serviceConnected = true;

            if (syncService.mapList != null) {
                points = syncService.mapList;
            } else {
                points = new ArrayList<HeatPoint>();
            }

        }

        public void onServiceDisconnected(ComponentName className) {
            locManager.removeUpdates(locListener);
            serviceConnected = false;
            syncService = null;
        }
    };

    private final BroadcastReceiver mapViewBroadcastRecievers = new BroadcastReceiver() {
        // Get Broadcast from BackgroundService
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.v("IFH MapView", "onReceive start");

            double[] frequency = intent.getExtras().getDoubleArray(
                    BackgroundService.FFT_FREQUENCY);
            // Get the amplitude of the fft.
            double[] spectrum = intent.getExtras().getDoubleArray(
                    BackgroundService.FFT_VALUES);
            // Get the sensor axis to which the data belongs.
            char axis = intent.getExtras().getChar(BackgroundService.FFT_AXIS);

            int index050Hz = 0;
            int index100Hz = 0;
            double freqDistance050Hz = Double.MAX_VALUE;
            double freqDistance100Hz = Double.MAX_VALUE;
            double freq050Hz = 50.0;
            double freq100Hz = 100.0;

            for (int varI = 0; varI < frequency.length; varI++) {
                if (Math.abs(frequency[varI] - freq050Hz) < freqDistance050Hz) {
                    index050Hz = varI;
                    freqDistance050Hz = Math.abs(frequency[varI] - freq050Hz);
                }
                if (Math.abs(frequency[varI] - freq100Hz) < freqDistance100Hz) {
                    index100Hz = varI;
                    freqDistance100Hz = Math.abs(frequency[varI] - freq100Hz);
                }
            }

            // If the values are not at a limit of the frequency array, add
            // three
            // FFT values around the frequency index together and display this.
            double[] temp = new double[2];
            temp[0] = (float) spectrum[index050Hz];
            if (index050Hz > 0) {
                temp[0] += 0.5 * spectrum[index050Hz - 1];
            }
            if (index050Hz < frequency.length - 1) {
                temp[0] += 0.5 * spectrum[index050Hz + 1];
            }

            temp[1] = spectrum[index100Hz];
            if (index100Hz > 0) {
                temp[1] += 0.5 * spectrum[index100Hz - 1];
            }
            if (index100Hz < frequency.length - 1) {
                temp[1] += 0.5 * spectrum[index100Hz + 1];
            }

            if (axis == 'X') {

                double longitude = -1;
                double latitude = -1;
                double altitude = -1;
                double accuracy = -1;
                long time = -1;

                try {

                    // Get the location
                    currentLocation = locManager
                            .getLastKnownLocation(currentProvider);
                    longitude = currentLocation.getLongitude();
                    latitude = currentLocation.getLatitude();
                    altitude = currentLocation.getAltitude();
                    accuracy = currentLocation.getAccuracy();
                    time = currentLocation.getTime();

                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Error getting location", Toast.LENGTH_LONG).show();
                }
                currentMeasurement = new MeasurementPoint(longitude, latitude,
                        altitude, accuracy, time);
                currentHeatPoint = new HeatPoint();
                currentMeasurement.setMagneticFieldX(temp[0]);

            } else if (axis == 'Y') {
                currentMeasurement.setMagneticFieldY(temp[0]);
            } else if (axis == 'Z') {
                currentMeasurement.setMagneticFieldZ(temp[0]);

                currentHeatPoint.intensity = (int) currentMeasurement
                        .getMagneticFieldS();
                currentHeatPoint.lat = (float) currentMeasurement.getLatitude();
                currentHeatPoint.lon = (float) currentMeasurement
                        .getLongitude();
                points.add(currentHeatPoint);

                try {
                    int lat = (int) (currentLocation.getLatitude() * 1E6);
                    int lng = (int) (currentLocation.getLongitude() * 1E6);
                    GeoPoint geo = new GeoPoint(lat, lng);
                    mapController.setCenter(geo);
                    mapController.setZoom(zoomLevel((double) pointRadius));
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Localization not possible", Toast.LENGTH_LONG).show();
                }

                updateMapOverlay();
            }

            Log.d("IFH MapView", "onReceive stop");
        }
    };

    public static byte zoomLevel(double distance) {
        byte zoom = 1;
        double E = 40075000;
        Log.i("Astrology", "result: "
                + (Math.log(E / distance) / Math.log(2) + 1));
        zoom = (byte) Math.round(Math.log(E / distance) / Math.log(2) + 1);
        // to avoid exeptions
        if (zoom > 21)
            zoom = 21;
        if (zoom < 1)
            zoom = 1;

        return zoom;
    }

    class MyLocationListener implements LocationListener {
        public void onLocationChanged(Location location) {

        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.mapview_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.screenshot_spectral:
                takeScreenshot();
                return true;
            case R.id.settings_spectral:
                Intent intent = new Intent(MapsView.this, Settings.class);
                startActivity(intent);
                return true;
            case R.id.trigger_mapview:
                triggerMeasurement();
                return true;
            case R.id.location_mapview:
                try {

                    // Get the location
                    currentLocation = locManager
                            .getLastKnownLocation(currentProvider);
                    int lat = (int) (currentLocation.getLatitude() * 1E6);
                    int lng = (int) (currentLocation.getLongitude() * 1E6);
                    GeoPoint geo = new GeoPoint(lat, lng);
                    mapController.setCenter(geo);
                    mapController.setZoom(zoomLevel((double) 50 * pointRadius));
                } catch (Exception e) {
                    Toast.makeText(this, "Localization not possible", Toast.LENGTH_LONG).show();
                }

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void takeScreenshot() {
        // Get instance of Vibrator from current Context
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        // Vibrate for 300 milliseconds
        v.vibrate(300);

        File exportsDirectory = new File(BackgroundService.SCREENSHOTS_PATH);
        exportsDirectory.mkdirs();

        mapView.setDrawingCacheEnabled(true);

        screenshot = mapView.getDrawingCache();

        date = new Date();

        filename = BackgroundService.SCREENSHOTS_PATH + "Screenshot_"
                + DateFormat.getDateInstance().format(new Date()) + "_"
                + date.getHours() + "." + date.getMinutes() + "."
                + date.getSeconds() + ".png";

        try {
            out = new FileOutputStream(filename);
            screenshot.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.flush();
            out.close();
            Toast.makeText(this,
                    "Screenshot succesfully saved to: " + filename,
                    Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void triggerMeasurement() {

        if (syncService.connectionStatus == 2) {
            Log.v("MapView", "start trigger");

            byte[] out = {64};
            syncService.sendMessage(out);

            // Get instance of Vibrator from current Context
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

            // Vibrate for 300 milliseconds
            v.vibrate(200);
            Toast.makeText(getApplicationContext(), "Measurement triggered", Toast.LENGTH_SHORT)
                    .show();

        } else {
            Toast.makeText(getApplicationContext(), "No connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void updateMapOverlay() {
        if (points.size() > 0) {
            heatMapOverlay.update(points);
        }
        Log.v("philipba", "mapupdate");
    }

    public void getGPSStatus() {

        // Disable previous Requests
        try {
            locManager.removeUpdates(locListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // exceptions will be thrown if provider is not permitted.
        try {
            gpsEnabled = locManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
        try {
            networkEnabled = locManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        } catch (Exception ex) {
            ex.printStackTrace();
            networkEnabled = false;
        }

        if (gpsEnabled) {
            locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    1000, 0, locListener);
            currentProvider = LocationManager.GPS_PROVIDER;
            if (!providerSet) {
                Toast.makeText(this, "GPS provider enabled", Toast.LENGTH_LONG)
                        .show();
            }
        } else if (networkEnabled) {
            locManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                    1000, 0, locListener);
            currentProvider = LocationManager.NETWORK_PROVIDER;
            if (!providerSet) {
                Toast.makeText(this, "Network provider enabled",
                        Toast.LENGTH_LONG).show();
            }
        } else if (!gpsEnabled && !networkEnabled) {
            if (!providerSet) {
                Toast.makeText(this, "Localization not available",
                        Toast.LENGTH_LONG).show();
            }

        }

        providerSet = true;

    }

    public Boolean isMobileDataEnabled() {
        final ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        boolean connected = networkInfo.isConnected();
        Log.v("philipba", " networkConnected " + connected);
        return connected;
    }

    private class MapViewChangeListener implements MyMapView.OnChangeListener {

        public void onChange(MapView view, GeoPoint newCenter,
                             GeoPoint oldCenter, int newZoom, int oldZoom) {
            updateMapOverlay();
        }

    }

}
