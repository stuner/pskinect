package com.ifh.magnetometerapp.api14.Activities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.*;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.ListView;
import com.ifh.magnetometerapp.api14.Background.*;
import com.ifh.magnetometerapp.api14.R;

public class MeasuresView extends Activity {

    private ListView listView;
    private MeasureListItem itemList[];
    private DatabaseAdapter dbAdapter;
    private String tableNames[];
    private MeasuresListItemAdapter adapter;
    private BackgroundService syncService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.measuresview);

        ActionBar actionBar;
        actionBar = this.getActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle("MEASURES");

        listView = (ListView) findViewById(R.id.measures_listview);

        this.bindService(
                new Intent(MeasuresView.this, BackgroundService.class),
                serviceConnection, Context.BIND_AUTO_CREATE);


    }


    public void updateView() {
        dbAdapter = syncService.dbAdapter;

        dbAdapter.open();

        tableNames = dbAdapter.getTableNames();

        if (tableNames == null) {
            tableNames = new String[]{"no measures saved"};
            itemList = new MeasureListItem[tableNames.length];
            itemList[0] = new MeasureListItem(tableNames[0], 0);
        } else {
            itemList = new MeasureListItem[tableNames.length];
            for (int i = 0; i < itemList.length; i++) {
                int temp = dbAdapter.fetchAllEntries(tableNames[i]).getCount();
                itemList[i] = new MeasureListItem(tableNames[i], temp);
            }
        }

        dbAdapter.close();


        adapter = new MeasuresListItemAdapter(this,
                R.layout.measure_listviewitem, itemList);

        this.registerForContextMenu(listView);

        listView.setAdapter(adapter);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenuInfo menuInfo) {

        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Options");
        menu.add(0, 0, 0, "Delete");
        menu.add(0, 0, 0, "Export XML");
        menu.add(0, 0, 0, "Export CSV");
        menu.add(0, 0, 0, "Delete All");
    }

    public boolean onContextItemSelected(MenuItem item) {

        MeasureExporter exporter;
        AdapterView.AdapterContextMenuInfo info;
        try {
            info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

            String name = itemList[info.position].tableName;
            if (item.getTitle() == "Delete") {

                dbAdapter.open();
                dbAdapter.deleteTable(name);
                dbAdapter.close();
                updateView();
            } else if (item.getTitle() == "Export XML") {
                exporter = new MeasureExporter(getApplicationContext(), dbAdapter, name);
                try {
                    exporter.exportTableToXML(name);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (item.getTitle() == "Export CSV") {
                exporter = new MeasureExporter(getApplicationContext(), dbAdapter, name);
                try {
                    exporter.exportTableToCSV(name);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (item.getTitle() == "Delete All") {
                dbAdapter.open();
                String temp[] = dbAdapter.getTableNames();
                for (String s : temp) {
                    dbAdapter.deleteTable(s);
                }
                dbAdapter.close();
                updateView();
            } else {
                return false;
            }
        } catch (ClassCastException e) {
            Log.e("", "bad menuInfo", e);
            return false;
        }
        return true;
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className, IBinder service) {
            syncService = ((BackgroundService.LocalBinder) service)
                    .getService();
            updateView();

        }

        public void onServiceDisconnected(ComponentName className) {
            syncService = null;
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.measuresview_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.settings_measures:
                Intent intent = new Intent(MeasuresView.this, Settings.class);
                startActivity(intent);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
