package com.ifh.magnetometerapp.api14.Activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.DialogPreference;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.NumberPicker;
import com.ifh.magnetometerapp.api14.Background.BackgroundService;
import com.ifh.magnetometerapp.api14.R;

public class LiveViewNumberPicker extends DialogPreference {

	private BackgroundService syncService;

	private Context context;
	private NumberPicker numbPicker;
	private SharedPreferences prefs;
	private Editor editor;
	private int value;

	public LiveViewNumberPicker(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		prefs = PreferenceManager.getDefaultSharedPreferences(context);
		editor = prefs.edit();

		this.setDialogLayoutResource(R.layout.liveviewnumberpicker);

	}

	@Override
	protected void onBindDialogView(View view) {
		super.onBindDialogView(view);
		numbPicker = (NumberPicker) view.findViewById(R.id.numberPickerLiveView);
		value = prefs.getInt(Settings.LIVEVIEW_PERFORMANCE_PREFERENCE_KEY, 50);
		numbPicker.setMinValue(10);
		numbPicker.setMaxValue(200);
		numbPicker.setValue(value);

	}

	@Override
	protected View onCreateDialogView() {
		return super.onCreateDialogView();
	}

	@Override
	protected void onDialogClosed(boolean positiveResult) {

		if (!positiveResult)
			return;

		editor.putInt(Settings.LIVEVIEW_PERFORMANCE_PREFERENCE_KEY, numbPicker.getValue());
		editor.commit();
		setTheSummary(""+numbPicker.getValue());

		super.onDialogClosed(positiveResult);
	}

	public void setTheSummary(String summary) {
		setSummary(summary);
	}

}
