package com.ifh.magnetometerapp.api14.Activities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.*;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.*;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.androidplot.Plot.BorderStyle;
import com.androidplot.ui.AnchorPosition;
import com.androidplot.ui.DynamicTableModel;
import com.androidplot.ui.SizeLayoutType;
import com.androidplot.ui.SizeMetrics;
import com.androidplot.xy.*;
import com.ifh.magnetometerapp.api14.Background.BackgroundService;
import com.ifh.magnetometerapp.api14.R;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.LinkedList;

public class WaveformAnalysisView extends Activity {

    // Request ID's
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private static final int REQUEST_SAVE_MEASURE = 3;
    private static final int REQUEST_OPTIONS_DIALOG = 4;

    // Plot widget
    private double maxValue = 0;
    private double minValue = 0;
    private CheckBox showXAxis;
    private CheckBox showYAxis;
    private CheckBox showZAxis;
    private CheckBox showTotal;
    private LinearLayout toggleAxis;
    private MultitouchPlot livePlot;
    private SimpleXYSeries linePlotX = null;
    private SimpleXYSeries linePlotY = null;
    private SimpleXYSeries linePlotZ = null;
    private SimpleXYSeries linePlotS = null;
    DisplayMetrics dm = new DisplayMetrics();
    LineAndPointFormatter linePlotFormatX;
    LineAndPointFormatter linePlotFormatY;
    LineAndPointFormatter linePlotFormatZ;
    LineAndPointFormatter linePlotFormatS;
    LinkedList<Number> plotX = new LinkedList<Number>();
    LinkedList<Number> plotY = new LinkedList<Number>();
    LinkedList<Number> plotZ = new LinkedList<Number>();
    LinkedList<Number> plotS = new LinkedList<Number>();
    private ActionBar actionBar;
    private boolean changeBoundariesIsOn = false;

    // Broadcast
    private final IntentFilter WAVEFORM_UPDATE_FILTER = new IntentFilter(
            BackgroundService.WAVEFORM_BROADCAST);
    private BackgroundService syncService;

    // Take screenshot variables
    private Bitmap screenshot;
    private FileOutputStream out;
    private String filename;
    private Date date;

    // Preferences
    private SharedPreferences prefs;
    private Editor prefsEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // set layout
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.waveformanalysis_view);

        getWindowManager().getDefaultDisplay().getMetrics(dm);

        // Get shared preferences (f.e. calibration status)
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefsEditor = prefs.edit();

        // Initialize Widgets

        actionBar = this.getActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle("WAVEFORM ANALYSIS");
        livePlot = (MultitouchPlot) findViewById(R.id.waveformPlot);
        toggleAxis = (LinearLayout) findViewById(R.id.waveformtoggleaxis);
        showXAxis = (CheckBox) findViewById(R.id.waveform_xaxischeck);
        showYAxis = (CheckBox) findViewById(R.id.waveform_yaxischeck);
        showZAxis = (CheckBox) findViewById(R.id.waveform_zaxischeck);
        showTotal = (CheckBox) findViewById(R.id.waveform_totalcheck);

        showXAxis.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if (isChecked) {
                    livePlot.addSeries(linePlotX, linePlotFormatX);
                    linePlotX.setModel(plotX,
                            SimpleXYSeries.ArrayFormat.XY_VALS_INTERLEAVED);
                } else {
                    livePlot.removeSeries(linePlotX);
                }

                livePlot.redraw();

            }

        });
        showYAxis.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                if (isChecked) {
                    livePlot.addSeries(linePlotY, linePlotFormatY);
                    linePlotY.setModel(plotY,
                            SimpleXYSeries.ArrayFormat.XY_VALS_INTERLEAVED);
                } else {
                    livePlot.removeSeries(linePlotY);
                }

                livePlot.redraw();

            }

        });
        showZAxis.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if (isChecked) {
                    livePlot.addSeries(linePlotZ, linePlotFormatZ);
                    linePlotZ.setModel(plotZ,
                            SimpleXYSeries.ArrayFormat.XY_VALS_INTERLEAVED);
                } else {
                    livePlot.removeSeries(linePlotZ);
                }

                livePlot.redraw();

            }

        });
        showTotal.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if (isChecked) {
                    livePlot.addSeries(linePlotS, linePlotFormatS);
                    linePlotS.setModel(plotS,
                            SimpleXYSeries.ArrayFormat.XY_VALS_INTERLEAVED);
                } else {
                    livePlot.removeSeries(linePlotS);
                }

                livePlot.redraw();

            }

        });


        // Bind BackgroundService with this Activity
        getApplicationContext().bindService(
                new Intent(WaveformAnalysisView.this, BackgroundService.class),
                serviceConnection, Context.BIND_AUTO_CREATE);

        for (int i = 0; i < 50; i++) {
            plotX.add(0);
            plotY.add(0);
            plotZ.add(0);
            plotS.add(0);
        }

        initializePlots();

    }

    protected void onResume() {
        super.onResume();
        registerReceiver(fftUpdateReceiver, WAVEFORM_UPDATE_FILTER);

        livePlot.redraw();
    }

    @Override
    protected void onStop() {
        // unregister receivers
        unregisterReceiver(fftUpdateReceiver);
        super.onStop();
    }

    protected void onPause() {
        super.onPause();
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className, IBinder service) {
            syncService = ((BackgroundService.LocalBinder) service)
                    .getService();
        }

        public void onServiceDisconnected(ComponentName className) {
            syncService = null;
        }
    };

    private final BroadcastReceiver fftUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            double[] values = intent.getExtras().getDoubleArray(
                    BackgroundService.WAVEFORM_VALUES);
            // Get the sensor axis to which the data belongs.
            char axis = intent.getExtras().getChar(
                    BackgroundService.WAVEFORM_AXIS);

            // Range of x axis: 0 .. 1000 Hz
            /*
             * livePlot.setDomainBoundaries(0, syncService.samplingFrequency /
			 * 2, BoundaryMode.FIXED);
			 * livePlot.setDomainStep(XYStepMode.INCREMENT_BY_VAL, 25);
			 */
            maxValue = 0;
            minValue = 0;

            livePlot.setDomainValueFormat(new DecimalFormat("0.0000"));
            livePlot.setRangeValueFormat(new DecimalFormat("0.00"));

            final int valuesLength = values.length;
            if (axis == 'X') {
                plotX.clear();
                for (int i = 0; i < values.length; i++) {


                    plotX.add(i / syncService.samplingFrequency);
                    plotX.add(values[i]);

                    if (values[i] > maxValue) {
                        maxValue = values[i];
                    }
                    if (values[i] < minValue) {
                        minValue = values[i];
                    }
                }
                linePlotX.setModel(plotX,
                        SimpleXYSeries.ArrayFormat.XY_VALS_INTERLEAVED);
            } else if (axis == 'Y') {
                plotY.clear();
                for (int i = 0; i < values.length; i++) {

                    plotY.add(i / syncService.samplingFrequency);
                    plotY.add(values[i]);
                    if (values[i] > maxValue) {
                        maxValue = values[i];
                    }
                    if (values[i] < minValue) {
                        minValue = values[i];
                    }
                }
                linePlotY.setModel(plotY,
                        SimpleXYSeries.ArrayFormat.XY_VALS_INTERLEAVED);
            } else if (axis == 'Z') {
                plotZ.clear();
                plotS.clear();
                for (int i = 0; i < valuesLength; i++) {

                    plotZ.add(i / syncService.samplingFrequency);
                    plotZ.add(values[i]);
                    plotS.add(i / syncService.samplingFrequency);
                    plotS.add(Math.sqrt(values[i] * values[i]
                            + plotX.get(i * 2 + 1).doubleValue()
                            * plotX.get(i * 2 + 1).doubleValue()
                            + plotY.get(i * 2 + 1).doubleValue()
                            * plotY.get(i * 2 + 1).doubleValue()));
                    if (values[i] > maxValue) {
                        maxValue = values[i];
                    }
                    if (values[i] < minValue) {
                        minValue = values[i];
                    }
                    if (plotS.getLast().doubleValue() > maxValue) {
                        maxValue = plotS.getLast().doubleValue();
                    }
                    if (plotS.getLast().doubleValue() < minValue) {
                        minValue = plotS.getLast().doubleValue();
                    }


                }


                if (Math.abs(minValue) > Math.abs(maxValue)) {
                    minValue--;
                    maxValue = -minValue;
                } else {
                    maxValue++;
                    minValue = -maxValue;
                }


                linePlotZ.setModel(plotZ,
                        SimpleXYSeries.ArrayFormat.XY_VALS_INTERLEAVED);
                linePlotS.setModel(plotS,
                        SimpleXYSeries.ArrayFormat.XY_VALS_INTERLEAVED);

                livePlot.setRangeBoundaries(minValue, maxValue, BoundaryMode.FIXED);
                livePlot.setDomainBoundaries(0, plotS.get(plotS.size() - 2), BoundaryMode.FIXED);

                // livePlot.setTicksPerDomainLabel(5);
                livePlot.redraw();

            }

            // liveValSeries.setModel(spectrum,
            // SimpleXYSeries.ArrayFormat.Y_VALS_ONLY);

            // livePlot.setDomainStep(XYStepMode.SUBDIVIDE,values.length);
        }
    };

    public void initializePlots() {
        // Turn the above arrays into XYSeries:
        linePlotX = new SimpleXYSeries("x-Axis");
        linePlotY = new SimpleXYSeries("y-Axis");
        linePlotZ = new SimpleXYSeries("z-Axis");
        linePlotS = new SimpleXYSeries("total");

        // Create a formatter to use for drawing a series using
        // LineAndPointRenderer:
        /*
         * LineAndPointFormatter liveSeriesFormat = new LineAndPointFormatter(
		 * Color.rgb(0, 0, 128), // line color Color.rgb(100, 149, 237), //
		 * point color Color.argb(0, 0, 0, 0)); // fill color (optional)
		 */
        linePlotFormatX = new LineAndPointFormatter(
                Color.rgb(255, 0, 0), // line color
                Color.argb(0, 0, 255, 0), // point color
                Color.argb(0, 100, 149, 237)); // fill color (optional)
        linePlotFormatY = new LineAndPointFormatter(
                Color.rgb(0, 255, 0), // line color
                Color.argb(0, 0, 255, 0), // point color
                Color.argb(0, 100, 149, 237)); // fill color (optional)
        linePlotFormatZ = new LineAndPointFormatter(
                Color.rgb(0, 0, 255), // line color
                Color.argb(0, 0, 255, 0), // point color
                Color.argb(0, 100, 149, 237)); // fill color (optional)
        linePlotFormatS = new LineAndPointFormatter(
                Color.rgb(255, 234, 0), // line color
                Color.argb(0, 0, 255, 0), // point color
                Color.argb(0, 100, 149, 237)); // fill color (optional)

        // liveSeriesFormat.setVertexPaint(null);
        /*linePlotFormatX.getLinePaint().setStrokeWidth(
                Math.round(dm.densityDpi
                        * ((float) getApplicationContext().getResources()
                        .getInteger(R.integer.plot_line_size_inc_4))
                        / 10000));
        linePlotFormatY.getLinePaint().setStrokeWidth(
                Math.round(dm.densityDpi
                        * ((float) getApplicationContext().getResources()
                        .getInteger(R.integer.plot_line_size_inc_4))
                        / 10000));
        linePlotFormatZ.getLinePaint().setStrokeWidth(
                Math.round(dm.densityDpi
                        * ((float) getApplicationContext().getResources()
                        .getInteger(R.integer.plot_line_size_inc_4))
                        / 10000));
        linePlotFormatS.getLinePaint().setStrokeWidth(
                Math.round(dm.densityDpi
                        * ((float) getApplicationContext().getResources()
                        .getInteger(R.integer.plot_line_size_inc_4))
                        / 10000));*/

        livePlot.setRangeBoundaries(0, 50, BoundaryMode.FIXED);

        linePlotX.setModel(plotX,
                SimpleXYSeries.ArrayFormat.Y_VALS_ONLY);
        linePlotY.setModel(plotY,
                SimpleXYSeries.ArrayFormat.Y_VALS_ONLY);
        linePlotZ.setModel(plotZ,
                SimpleXYSeries.ArrayFormat.Y_VALS_ONLY);
        linePlotS.setModel(plotS,
                SimpleXYSeries.ArrayFormat.Y_VALS_ONLY);
        // Add series1 to the xyplot:
        livePlot.addSeries(linePlotX, linePlotFormatX);
        livePlot.addSeries(linePlotY, linePlotFormatY);
        livePlot.addSeries(linePlotZ, linePlotFormatZ);
        livePlot.addSeries(linePlotS, linePlotFormatS);

        Log.v("philipba", "range domain");
        // Reduce the number of range labels
        livePlot.setTicksPerRangeLabel(1);
        livePlot.setTicksPerDomainLabel(2);
        livePlot.setRangeStep(XYStepMode.SUBDIVIDE, 6);
        livePlot.setDomainStep(XYStepMode.SUBDIVIDE, 12);

        Log.v("philipba", "labels");
        livePlot.setDomainLabel("time [s]");
        livePlot.setRangeLabel("magnetic Field [µT]");

        Log.v("philipba", "margins");
        livePlot.setPlotMargins(10, 10, 10, 10);
        livePlot.setPlotPadding(10, 10, 10, 10);

        Log.v("philipba", "labeling");

        // Axis Labels
        livePlot.getDomainLabelWidget().getLabelPaint().setTextSize(20);
        livePlot.getRangeLabelWidget().getLabelPaint().setTextSize(20);
        livePlot.getDomainLabelWidget().setHeight(20);
        livePlot.getRangeLabelWidget().setHeight(200);
        livePlot.getDomainLabelWidget().setWidth(2000);
        livePlot.getRangeLabelWidget().setWidth(25);
        livePlot.getRangeLabelWidget().setMarginLeft(0);
        // livePlot.getDomainLabelWidget().setWidth(10, )
        // livePlot.getMeasuredWidth()/2;
        livePlot.getTitleWidget().getLabelPaint().setTextSize(20);
        livePlot.getTitleWidget().setWidth(200);
        livePlot.getTitleWidget().setHeight(20);
        livePlot.getGraphWidget().getRangeLabelPaint().setTextSize(15);
        livePlot.getGraphWidget().getDomainLabelPaint().setTextSize(15);
        livePlot.getGraphWidget().getRangeOriginLabelPaint().setTextSize(15);
        livePlot.getGraphWidget().getDomainOriginLabelPaint().setTextSize(15);
        livePlot.position(livePlot.getDomainLabelWidget(), 0,
                XLayoutStyle.ABSOLUTE_FROM_CENTER, -10,
                YLayoutStyle.ABSOLUTE_FROM_BOTTOM, AnchorPosition.BOTTOM_MIDDLE);

        // Legend Widget
        livePlot.getLegendWidget().setTableModel(new DynamicTableModel(2, 2));
        livePlot.getLegendWidget().setSize(
                new SizeMetrics(80, SizeLayoutType.ABSOLUTE, 200,
                        SizeLayoutType.ABSOLUTE));
        Paint bgPaint = new Paint();
        bgPaint.setColor(Color.BLACK);
        bgPaint.setStyle(Paint.Style.FILL);
        bgPaint.setAlpha(140);
        livePlot.getLegendWidget().setBackgroundPaint(bgPaint);
        livePlot.getLegendWidget().setPadding(10, 1, 1, 1);
        livePlot.position(livePlot.getLegendWidget(), 20,
                XLayoutStyle.ABSOLUTE_FROM_RIGHT, 35,
                YLayoutStyle.ABSOLUTE_FROM_TOP, AnchorPosition.RIGHT_TOP);
        livePlot.getLegendWidget().getTextPaint().setTextSize(20);
        livePlot.getLegendWidget().setIconSizeMetrics(
                new SizeMetrics(20, SizeLayoutType.ABSOLUTE, 20,
                        SizeLayoutType.ABSOLUTE));
        // By default, AndroidPlot displays developer guides to aid in
        // laying out your plot.
        // To get rid of them call disableAllMarkup():
        livePlot.getGraphWidget().setMarginTop(10);
        livePlot.getGraphWidget().setMarginLeft(10);
        livePlot.getGraphWidget().setMarginRight(2);
        livePlot.setBorderStyle(BorderStyle.SQUARE, null, null);
        livePlot.disableAllMarkup();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.waveform_view_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.screenshot_spectral:
                takeScreenshot();
                return true;
            case R.id.menu_settings:
                Intent intent = new Intent(WaveformAnalysisView.this,
                        Settings.class);
                startActivity(intent);
                return true;
            case R.id.trigger_spectral:
                triggerMeasurement();
                return true;
            case R.id.staticboundaries:
                showBoundariesChangeSettings();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void takeScreenshot() {
        // Get instance of Vibrator from current Context
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        // Vibrate for 300 milliseconds
        v.vibrate(300);

        File exportsDirectory = new File(BackgroundService.SCREENSHOTS_PATH);
        exportsDirectory.mkdirs();

        livePlot.setDrawingCacheEnabled(true);

        screenshot = livePlot.getDrawingCache();

        date = new Date();

        filename = BackgroundService.SCREENSHOTS_PATH + "Screenshot_"
                + DateFormat.getDateInstance().format(new Date()) + "_"
                + date.getHours() + "." + date.getMinutes() + "."
                + date.getSeconds() + ".png";

        try {
            out = new FileOutputStream(filename);
            screenshot.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.flush();
            out.close();
            Toast.makeText(this,
                    "Screenshot succesfully saved to: " + filename,
                    Toast.LENGTH_LONG).show();
            Uri uri = Uri.fromFile(exportsDirectory);
            Intent scanFileIntent = new Intent(
                    Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri);
            sendBroadcast(scanFileIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void triggerMeasurement() {

        if (syncService.connectionStatus == 2) {
            Log.v("MapView", "start trigger");

            byte[] out = {64};
            syncService.sendMessage(out);

            // Get instance of Vibrator from current Context
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

            // Vibrate for 300 milliseconds
            v.vibrate(200);
            Toast.makeText(getApplicationContext(), "Measurement triggered", Toast.LENGTH_SHORT)
                    .show();

        } else {
            Toast.makeText(getApplicationContext(), "No connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void showBoundariesChangeSettings() {

        float weight1, weight2;

        if (changeBoundariesIsOn) {
            weight1 = 0;
            weight2 = 100;
            changeBoundariesIsOn = false;
        } else {
            weight1 = 30;
            weight2 = 70;
            changeBoundariesIsOn = true;
        }

        ViewGroup.LayoutParams param1 = livePlot.getLayoutParams();
        ViewGroup.LayoutParams param2 = toggleAxis
                .getLayoutParams();

        LinearLayout.LayoutParams param3 = new LinearLayout.LayoutParams(
                param1.width, param1.height, weight1);
        livePlot.setLayoutParams(param3);

        LinearLayout.LayoutParams param4 = new LinearLayout.LayoutParams(
                param2.width, param2.height, weight2);
        toggleAxis.setLayoutParams(param4);

    }
}
