package com.ifh.magnetometerapp.api14.Activities;

import android.app.ActionBar;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.*;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.*;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
import com.ifh.magnetometerapp.api14.Background.*;
import com.ifh.magnetometerapp.api14.R;

import java.util.Calendar;

public class MainView extends Activity {

	// Service
	private boolean serviceConnected = false;
	private BackgroundService syncService;

	// Bluetooth
	private BluetoothAdapter mBluetoothAdapter = null;
	private BluetoothService mChatService = null;
	private boolean btEnabled = false;

	// Broadcast
	private IntentFilter intentFilter = new IntentFilter(
			BackgroundService.BTCONNECTION_BROADCAST);

	// Back-Button
	private static final long DOUBLE_PRESS_INTERVAL = 1500;
	private long lastPressTime;

	// Widgets Definition
	private ProgressDialog offsetCalProgressDialog;
	private ListView listView;
	private ActionBar actionBar;
	private TextView status, device;
	private MenuItem search, offsetCal;
	private Button measure;
	private TextView measureStatus;
	private ListItemAdapter adapter;
	private ListItem listItems[] = new ListItem[] {
			new ListItem(R.drawable.liveview_icon, "LIVE VIEW", null),
			new ListItem(R.drawable.mapview_icon, "MAP VIEW", null),
			new ListItem(R.drawable.spectralfilter_icon, "SPECTRAL FILTER",
					null),
			new ListItem(R.drawable.spectralview_icon, "SPECTRAL VIEW", null),
			new ListItem(R.drawable.waveformanalysis_icon, "WAVEFORM ANALYSIS",
					null),
			new ListItem(R.drawable.measuresview_icon, "MEASURES", null) };

	// Lifecycle
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.main);

		Thread.setDefaultUncaughtExceptionHandler(new TopExceptionHandler(this));

		adapter = new ListItemAdapter(this, R.layout.mainview_listviewitem,
				listItems);

		// Initialize Widgets
		offsetCalProgressDialog = new ProgressDialog(this);
		actionBar = this.getActionBar();
		actionBar.setDisplayShowTitleEnabled(false);
		listView = (ListView) findViewById(R.id.main_listview);
		measure = (Button) findViewById(R.id.mainview_measurebutton);
		measureStatus = (TextView) findViewById(R.id.mainview_measurestatus);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Intent intent;
				switch (arg2) {
				case 0:
					intent = new Intent(MainView.this, LiveView.class);
					startActivity(intent);
					break;
				case 1:
					intent = new Intent(MainView.this, MapsView.class);
					startActivity(intent);
					break;
				case 2:
					intent = new Intent(MainView.this, SpectralFilter.class);
					startActivity(intent);
					break;
				case 3:
					intent = new Intent(MainView.this, SpectralView.class);
					startActivity(intent);
					break;
				case 4:
					intent = new Intent(MainView.this,
							WaveformAnalysisView.class);
					startActivity(intent);
					break;
				case 5:
					if (syncService.measureRunning) {
						AlertDialog alertDialog = new AlertDialog.Builder(
								MainView.this).create();
						alertDialog.setMessage("Stop running measure first.");

						alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "OK",
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
										
									}
									
								});
						alertDialog.show();
					} else {
						intent = new Intent(MainView.this, MeasuresView.class);
						startActivity(intent);
						break;
					}
				}
			}
		});
		measure.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				if (syncService.connectionStatus == 0
						|| syncService.connectionStatus == 1) {
					AlertDialog alertDialog = new AlertDialog.Builder(
							MainView.this).create();
					alertDialog.setMessage("No device is connected");

					alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL,"OK",
							new DialogInterface.OnClickListener() {

								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
									
								}
								
							});
					alertDialog.show();
				} else if (!syncService.measureRunning) {
					Calendar c = Calendar.getInstance();

					String name1 = "measure_trigger_" + c.getTimeInMillis();
					String name2 = "measure_continous_" + c.getTimeInMillis();
					syncService.startMeasurement(name1, name2);
					measureStatus.setText("RUNNING");
					measureStatus.setTextColor(getApplicationContext()
							.getResources().getColor(R.color.green));
					measure.setText("STOP");
				} else {
					syncService.stopMeasurement();
					measureStatus.setText("STOPPED");
					measureStatus.setTextColor(getApplicationContext()
							.getResources().getColor(R.color.lightred));
					measure.setText("START");
				}
			}
		});
		// Start BackgroundService and bind it with this Activity
		startService(new Intent(BackgroundService.MY_SERVICE));

		checkBluetoothAvailability();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onStart() {
		super.onStart();

	}

	protected void onResume() {
		super.onResume();
		// Initialize Reciever (messages from BackgroundService about
		// BluetoothConnectionStatus)
		registerReceiver(btStatusReceiver, intentFilter);
		bindService(new Intent(this, BackgroundService.class),
				serviceConnection, Context.BIND_AUTO_CREATE);
		if (serviceConnected) {
			getBluetoothStatus();
		}
	}

	@Override
	protected void onStop() {

		Log.v("philipba", "main view on stop");
		super.onStop();
	}

	@Override
	protected void onDestroy() {

		Log.v("philipba", "main view on destroy");
		syncService.stopBluetooth();
		unregisterReceiver(btStatusReceiver);
		unbindService(serviceConnection);
		stopService(new Intent(BackgroundService.MY_SERVICE));

		super.onDestroy();
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == BackgroundService.REQUEST_CONNECT_DEVICE) {
			// When DeviceListActivity returns with a device to connect
			if (resultCode == Activity.RESULT_OK) {
				// Get the device MAC address
				String address = data
						.getStringExtra(DeviceListDialog.EXTRA_DEVICE_ADDRESS);
				syncService.connectToDevice(address);
			}
		} else if (requestCode == BackgroundService.REQUEST_ENABLE_BT) {
			// When the request to enable Bluetooth returns
			if (resultCode != Activity.RESULT_OK) {
				// User did not enable Bluetooth or an error occured
				Toast.makeText(this, R.string.bt_not_enabled_leaving,
						Toast.LENGTH_SHORT).show();
				// finish();
			}

		}
	}

	// Bluetooth
	public void checkBluetoothAvailability() {
		// Get local Bluetooth adapter

		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		// If the adapter is null, then Bluetooth is not supported
		if (mBluetoothAdapter == null) {
			Toast.makeText(this, "Bluetooth is not available",
					Toast.LENGTH_LONG).show();
			// finish();
			return;
		}

		// If BT is not on, request that it be enabled.
		// setupChat() will then be called during onActivityResult
		if (!mBluetoothAdapter.isEnabled()) {
			Intent enableIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_ENABLE);
			enableIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			getApplication().startActivity(enableIntent);
			// Otherwise, setup the chat session
		} else {
			btEnabled = true;
		}

	}

	private void getBluetoothStatus() {
		if (syncService.connectionStatus == 0) {
			status.setText("NOT CONNECTED");
			device.setText("");
			status.setTextColor(Color.WHITE);
			search.setEnabled(true);
			search.setVisible(true);
			offsetCal.setEnabled(false);
			offsetCal.setVisible(false);
		} else if (syncService.connectionStatus == 1) {
			status.setText("CONNECTING...");
			device.setText("");
			status.setTextColor(Color.WHITE);
			search.setEnabled(false);
			search.setVisible(false);
			offsetCal.setEnabled(false);
			offsetCal.setVisible(false);
		} else if (syncService.connectionStatus == 2) {
			status.setText("CONNECTED");
			status.setTextColor(getResources().getColor(R.color.lightgreen));
			String temp[] = syncService.mConnectedDeviceName.split(" ");
			device.setText("DEVICE " + temp[temp.length - 1]);
			search.setEnabled(false);
			search.setVisible(false);
			offsetCal.setEnabled(true);
			offsetCal.setVisible(true);
			if (syncService.offsetCalibrationData) {
				offsetCal.setIcon(this.getResources().getDrawable(
						R.drawable.offsetcal_icon_green));
			} else {
				offsetCal.setIcon(this.getResources().getDrawable(
						R.drawable.offsetcal_icon));
			}
		}
	}

	public void searchDevice() {
		if (mChatService == null) {
			Intent anIntent = new Intent(this, DeviceListDialog.class);
			startActivityForResult(anIntent,
					BackgroundService.REQUEST_CONNECT_DEVICE);
		}
	}

	private final BroadcastReceiver btStatusReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			getBluetoothStatus();
			if (offsetCalProgressDialog.isShowing()) {
				offsetCalProgressDialog.dismiss();
			}
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu men) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.mainview_menu, men);

		// ActionBar bt-status text
		View view = men.findItem(R.id.menu_status).getActionView();
		status = (TextView) view.findViewById(R.id.status_text);
		status.setTextColor(Color.WHITE);
		LinearLayout.LayoutParams myParams = new LinearLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		myParams.setMargins(0, 0, 5, 0);
		status.setLayoutParams(myParams);

		// ActionBar device text
		view = men.findItem(R.id.menu_device).getActionView();
		device = (TextView) view.findViewById(R.id.device_text);
		device.setTextColor(Color.WHITE);

		// ActionBar search device button and offset calibration button
		search = men.findItem(R.id.menu_bt);
		offsetCal = men.findItem(R.id.menu_offsetcal);
		offsetCal.setEnabled(false);
		offsetCal.setVisible(false);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.menu_bt:
			checkBluetoothAvailability();
			if (this.btEnabled) {
				searchDevice();
			}
			return true;
		case R.id.menu_settings:
			Intent intent = new Intent(MainView.this, Settings.class);
			startActivity(intent);
			return true;
		case R.id.menu_offsetcal:
			if (!syncService.offsetCalibrationData) {
				AlertDialog alertDialog = new AlertDialog.Builder(MainView.this)
						.create();
				alertDialog
						.setMessage("To calibrate the Magnetometer put it in a flat and stable position and press START");

				alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL,"START",
						new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								syncService.offsetCalibration();
								offsetCalProgressDialog = ProgressDialog.show(
										MainView.this, "",
										"Calibrating offset...");
							}
						});
				alertDialog.show();
			} else {
				AlertDialog alertDialog = new AlertDialog.Builder(MainView.this)
						.create();
				alertDialog
						.setMessage("The magnetometer is already calibrated. Do you want to recalibrate?");

				alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,"YES",
						new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								syncService.offsetCalibration();
								offsetCalProgressDialog = ProgressDialog.show(
										MainView.this, "",
										"Calibrating offset...");
							}
						});
				alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE,"NO",
						new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								
							}
						});
				alertDialog.show();
			}

			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private ServiceConnection serviceConnection = new ServiceConnection() {

		public void onServiceConnected(ComponentName className, IBinder service) {
			syncService = ((BackgroundService.LocalBinder) service)
					.getService();
			serviceConnected = true;

			if (syncService.measureRunning) {
				measureStatus.setText("RUNNING");
				measureStatus.setTextColor(getApplicationContext()
						.getResources().getColor(R.color.green));
				measure.setText("STOP");
			} else {
				measureStatus.setText("STOPPED");
				measureStatus.setTextColor(getApplicationContext()
						.getResources().getColor(R.color.lightred));
				measure.setText("START");
			}
		}

		public void onServiceDisconnected(ComponentName className) {
			syncService = null;
			serviceConnected = false;
		}
	};

	@Override
	public void onBackPressed() {
		long pressTime = System.currentTimeMillis();
		if (pressTime - lastPressTime <= DOUBLE_PRESS_INTERVAL) {
			finish();
		}
		lastPressTime = pressTime;
		Toast.makeText(this, "Press back button again to exit",
				Toast.LENGTH_SHORT).show();
	}

}