﻿using System;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace KinectWPFOpenCV
{
    public static class ImageHelpers
    {
        public static System.Drawing.Bitmap ToBitmap(this BitmapSource bitmapsource)
        {
            System.Drawing.Bitmap bitmap;
            using (var outStream = new MemoryStream())
            {
                // from System.Media.BitmapImage to System.Drawing.Bitmap
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapsource));
                enc.Save(outStream);
                bitmap = new System.Drawing.Bitmap(outStream);
                return bitmap;
            }
        }

        [DllImport("gdi32")]
        private static extern int DeleteObject(IntPtr o);

        public static BitmapSource ToSource(this Bitmap bmp)
        {
            if (bmp == null)
            {
                return null;
            }
            IntPtr hBitmap = bmp.GetHbitmap();
            BitmapSource bs =
                System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                    hBitmap,
                    IntPtr.Zero,
                    System.Windows.Int32Rect.Empty,
                    System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());
            DeleteObject(hBitmap);
            bmp.Dispose();
            return bs;
        }

        public static Bitmap drawRectangles(this Bitmap drawOn, Rectangle[] rectArray, System.Drawing.Color color)
        {
            //PresentationSource source = PresentationSource.FromVisual(drawOn);
            //double dpiX = 96.0 * source.CompositionTarget.TransformToDevice.M11;
            //double dpiY = 96.0 * source.CompositionTarget.TransformToDevice.M22;
            double dpiX = 96, dpiY = 96;

            RenderTargetBitmap renderbitmap = new RenderTargetBitmap(drawOn.Width, drawOn.Height, dpiX, dpiY, PixelFormats.Default);
            DrawingVisual drawvis = new DrawingVisual();
            DrawingContext dc = drawvis.RenderOpen();
            var pen = new System.Windows.Media.Pen(System.Windows.Media.Brushes.Red, 2);

            dc.DrawImage(
                ((Bitmap)drawOn.Clone()).ToSource(),
                new Rect(0, 0, renderbitmap.Width, renderbitmap.Height),
                null
            );

            foreach (Rectangle rect in rectArray)
            {
                dc.DrawRectangle(null, pen, new Rect(rect.X, rect.Y, rect.Width, rect.Height));
            }
            dc.Close();
            renderbitmap.Render(drawvis);
            return renderbitmap.ToBitmap();
        }
    }
}
