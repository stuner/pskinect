﻿using AForge;
using AForge.Imaging;
using AForge.Imaging.Filters;
using System.Drawing;

namespace FindSensor
{
    /// <summary>
    /// Beinhaltet die Funktionen, um nach Farbe zu Filtern, Rechtecke zu erkennen
    /// und dies graphisch darzustellen.
    /// Verwendet aForge für Bilderkennung
    /// </summary>
    class ColorLayer
    {
        private BlobCounterBase bc = new BlobCounter();

        private ColorFiltering colorFilter = new ColorFiltering();
        private Grayscale grayFilter = new Grayscale(0.2125, 0.7154, 0.0721);
        private OtsuThreshold thresholdFilter = new OtsuThreshold();
        private SobelEdgeDetector edgeFilter = new SobelEdgeDetector();
        
        public Bitmap filteredColorBitmap { private set; get; }
        public Bitmap filteredDepthBitmap { private set; get; }

        public int minObjSize;
        public int maxObjSize;

        private int CorrectColorComponent(double cComponent)
        {
            return CorrectColorComponent((int)cComponent);
        }

        private int CorrectColorComponent(int cComponent)
        {
            if (cComponent > 255) cComponent = 255;
            else if (cComponent < 0) cComponent = 0;
            return cComponent;
        }

        public void setColors(double r, double g, double b, double tol_percent)
        {
            double colorToleranceRange = (255 * tol_percent / 100);

            colorFilter.Red = new IntRange((int)(r - colorToleranceRange), (int)(r + colorToleranceRange));
            colorFilter.Green = new IntRange((int)(g - colorToleranceRange), (int)(g + colorToleranceRange));
            colorFilter.Blue = new IntRange((int)(b - colorToleranceRange), (int)(b + colorToleranceRange));
        }

        public void setColors(System.Drawing.Color c, double tol_percent)
        {
            setColors(c.R, c.G, c.B, tol_percent);
        }

        public ColorLayer(double r, double g, double b, int minSize,int maxSize,  double tol_percent)
        {
            setColors(r, g, b, tol_percent);
            minObjSize = minSize;
            maxObjSize = maxSize;
            bc.FilterBlobs = true;
            bc.CoupledSizeFiltering = true;
        }

        public Rectangle[] findLocation(Bitmap colorBmp)
        {
            filteredColorBitmap = colorFilter.Apply(colorBmp);
            filteredColorBitmap = grayFilter.Apply(filteredColorBitmap);
            thresholdFilter.ApplyInPlace(filteredColorBitmap);

            return blobDetection(filteredColorBitmap);
        }

        public ColorFiltering getColorFilterInstance()
        {
            return colorFilter;
        }

        private Rectangle[] blobDetection(Bitmap bm)
        {
            // create an instance of blob counter algorithm
            bc.MinHeight = minObjSize;
            bc.MinWidth = minObjSize;
            bc.MaxHeight = maxObjSize;
            bc.MaxWidth = maxObjSize;
            bc.ObjectsOrder = ObjectsOrder.XY;
            
            // process binary image
            bc.ProcessImage(bm);

            Rectangle[] rects = bc.GetObjectsRectangles();

            return rects;
        }
    }
}
