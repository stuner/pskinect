﻿namespace FindSensor
{
    using System;

    public class Point3D
    {
        public Point3D(double X, double Y, double Z)
        {
            this.X = X;
            this.Y = Y;
            this.Z = Z;
        }

        public Point3D(int X, int Y, int Z)
        {
            this.X = (double)X;
            this.Y = (double)Y;
            this.Z = (double)Z;
        }

        public Point3D()
        {
            this.invalid = true;
        }

        public bool invalid { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }

        public string toString()
        {
            if (invalid)
                return "Invalid point";
            return "x : " + X + " y: " + Y + " z: " + Z;
        }

        public string toStringCm()
        {
            if (invalid)
                return "Invalid point";
            return String.Format("x: {0:#00}cm ", X / 10) +
                String.Format("y: {0:#00}cm ", Y / 10) +
                String.Format("z: {0:#00}cm ", Z / 10) +
                String.Format("abs: {0:#00}cm ", Abs() / 10);
        }

        public double Abs()
        {
            return Math.Sqrt(X * X + Y * Y + Z * Z);
        }
    }
}
