﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindSensor
{
    class MagField
    {
        public double MagX { get; set; } 
        public double MagY { get; set; } 
        public double MagZ { get; set; } 
        public String Timestamp { get; set; } 
    }
}
