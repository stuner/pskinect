﻿namespace FindSensor
{
    using AForge.Imaging.Filters;
    using Coding4Fun.Kinect.Wpf;
    using KinectWPFOpenCV;
    using Microsoft.Kinect;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    public partial class MainWindow : Window
    {
        #region variables

        private KinectFrameProcessor frameProcessor;

        private MovingAverage movingAverage = new MovingAverage(5);

        private System.Drawing.Color colorPickerColor;

        private ColorLayer colorLayer = new ColorLayer(255, 255, 255, 10, 300, 20);

        private int masterCounter = 0;

        //jedes frameFilter-te Bild wird analysiert
        private int frameFilter = 5;

        //Abstand in die x-Richtung zwischen dem Kinect und dem Ursprung des Koordinatensystems
        private int upSetY = 0;

        //Abstand in die z-Richtung zwischen dem Kinect und dem Ursprung des Koordinatensystems
        private int upSetZ = 0;

        private int newElevationAngle = 0;

        private bool stopping = false;

        private SensorMain sensorMain = new SensorMain();

        /// <summary>
        /// Active Kinect sensor
        /// </summary>
        private KinectSensor sensor;

        private Bitmap rawColorBitmap;

        #endregion

        #region Measurement event handlers

        /// <summary>
        /// Event handler for Kinect sensor's ColorFrameReady and DepthFrameReady event + "Main function" der Bildanalyse
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void AllFramesReady(object sender, AllFramesReadyEventArgs e)
        {
            //Modulozähler => jedes frameFilter-te Bild wird analysiert
            if (masterCounter++ % frameFilter == 0)
            {
                using (ColorImageFrame colorFrame = e.OpenColorImageFrame())
                using (DepthImageFrame depthFrame = e.OpenDepthImageFrame())
                {
                    // Übergabe der Daten an Bildanalyse
                    if (frameProcessor.setFrameData(colorFrame, depthFrame, upSetY, upSetZ, sensor.ElevationAngle, colorLayer))
                    {
                        // Eigentliche Analyse der Bilddaten
                        frameProcessor.processFrame();

                        // we need a copy for the color picker
                        rawColorBitmap = frameProcessor.rawColorBitmap;

                        // Bitmaps für Anzeige aktualisieren
                        FilterImage.Source = frameProcessor.colorsFiltered.ToSource();
                        ResultImage.Source = frameProcessor.colorsWithRectangles.ToSource();
                        DepthImage.Source = frameProcessor.rawDepthBitmap;

                        // Messwerte mit Grid abgleichen
                        if (frameProcessor.detectedObjects.Count() == 1 && frameProcessor.detectedObjects.First().Abs() != 0)
                        {
                            Point3D currentPoint = movingAverage.add(frameProcessor.detectedObjects.First());
                            currentPoint.Z = 0;
                            sensorMain.LogPosition(currentPoint);
                        }
                    }
                }
            }
        }

        #endregion

        #region Interface-Funktionen

        /// <summary>
        /// Initializes a new instance of the MainWindow class. (Run only once)
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            colorPickerColor = System.Drawing.Color.FromArgb(255, 255, 255);
            applyPickedColor();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
        }

        private void findAndInitSensor()
        {
            foreach (var potentialSensor in KinectSensor.KinectSensors)
            {
                if (potentialSensor.Status == KinectStatus.Connected)
                {
                    this.sensor = potentialSensor;
                    break;
                }
            }

            if (null != this.sensor)
            {
                // Turn on the color stream to receive color frames
                this.sensor.ColorStream.Enable(ColorImageFormat.RgbResolution640x480Fps30);

                // Turn on the depth stream to receive depth frames
                this.sensor.DepthStream.Enable(DepthImageFormat.Resolution640x480Fps30);

                // Add an event handler to be called whenever there is new color frame data
                this.sensor.AllFramesReady += this.AllFramesReady;

                frameProcessor = new KinectFrameProcessor(sensor);

                Task.Factory.StartNew(() =>
                {
                    // Start the sensor!
                    try
                    {
                        this.sensor.Start();

                        while (!stopping)
                        {
                            if (this.sensor != null)
                            {
                                try
                                {
                                    this.sensor.ElevationAngle = newElevationAngle;
                                }
                                catch { }
                            } 
                            System.Threading.Thread.Sleep(500);
                        }
                    }
                    catch (IOException)
                    {
                        this.sensor = null;
                    }
                });
            }
        }

        /// <summary>
        /// Execute startup tasks at initialisation (Run only once)
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            findAndInitSensor();

            colorLayer.minObjSize = (int)this.minObjSizeSlider.Value;
            colorLayer.maxObjSize = (int)this.maxObjSizeSlider.Value;

            colorPickerColor = System.Drawing.Color.FromArgb(255, 0x9C, 0xCB, 0x8B);
            ColorSampleText.Text = colorToHex(colorPickerColor);
            ColorSampleRect.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, colorPickerColor.R, colorPickerColor.G, colorPickerColor.B));
            applyPickedColor();

            sensorMain.Start();
        }

        /// <summary>
        /// Execute shutdown tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            stopping = true;
        }

        #endregion

        #region  Event handlers

        private int textToIntRange(string text, int min, int max)
        {
            int i;
            try
            {
                i = int.Parse(text);
                i = Math.Max(Math.Min(i, max), min);
            }
            catch
            {
                i = 0;
            }
            return i;
        }

        private void elevationAngleChanged(object sender, RoutedEventArgs e)
        {
            try
            {
                newElevationAngle = textToIntRange(textElevationAngle.Text, -27, 27);
            }
            catch { }
        }

        private void upsetYChanged(object sender, RoutedEventArgs e)
        {
            upSetY = textToIntRange(upsetY.Text, -100000000, 100000000);
        }

        private void upsetZChanged(object sender, RoutedEventArgs e)
        {
            upSetZ = textToIntRange(upsetZ.Text, -100000000, 100000000);
        }

        private static String colorToHex(System.Drawing.Color c)
        {
            return "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
        }

        private void ColorImage_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            System.Windows.Controls.Image image = (System.Windows.Controls.Image)sender;
            System.Windows.Point pos = e.GetPosition(e.Source as FrameworkElement);

            colorPickerColor = getRawColorFromPosition(image, pos);
            this.ColorSampleText.Text = colorToHex(colorPickerColor);
            this.ColorSampleRect.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, colorPickerColor.R, colorPickerColor.G, colorPickerColor.B));
        }

        private System.Drawing.Color getRawColorFromPosition(System.Windows.Controls.Image clickedOn, System.Windows.Point pos)
        {
            int x_, y_;

            if (rawColorBitmap == null || clickedOn == null)
                return System.Drawing.Color.White;

            x_ = (int)Math.Min(rawColorBitmap.Width * pos.X / clickedOn.ActualWidth, rawColorBitmap.Width - 1);
            y_ = (int)Math.Min(rawColorBitmap.Height * pos.Y / clickedOn.ActualHeight, rawColorBitmap.Height - 1);

            return rawColorBitmap.GetPixel(x_, y_);
        }

        private void ColorImage_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            System.Windows.Controls.Image image = (System.Windows.Controls.Image)sender;
            System.Windows.Point pos = e.GetPosition(e.Source as FrameworkElement);

            System.Drawing.Color pix = getRawColorFromPosition(image, pos);
            ColorSamplePreviewText.Text = colorToHex(pix);
            ColorSamplePreviewRect.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(pix.A, pix.R, pix.G, pix.B));
        }

        private void CopyButtonClicked(object sender, RoutedEventArgs e)
        {
            applyPickedColor();
        }

        private void applyPickedColor()
        {
            colorLayer.setColors(colorPickerColor, ColorSampleToleranceSlider.Value);

            this.low_Red.Text = colorLayer.getColorFilterInstance().Red.Min.ToString();
            this.low_Green.Text = colorLayer.getColorFilterInstance().Green.Min.ToString();
            this.low_Blue.Text = colorLayer.getColorFilterInstance().Blue.Min.ToString();

            this.high_Red.Text = colorLayer.getColorFilterInstance().Red.Max.ToString();
            this.high_Green.Text = colorLayer.getColorFilterInstance().Green.Max.ToString();
            this.high_Blue.Text = colorLayer.getColorFilterInstance().Blue.Max.ToString();
        }

        private void minObjSizeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            colorLayer.minObjSize = (int)this.minObjSizeSlider.Value;
        }

        private void maxObjSizeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            colorLayer.maxObjSize = (int)this.maxObjSizeSlider.Value;
        }
    }

        #endregion
}
