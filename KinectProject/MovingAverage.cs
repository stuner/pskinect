using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindSensor
{
    class MovingAverage
    {
        private Point3D[] points;
        private int bufLen;
        private int position = 0;

        public MovingAverage(int bufLen)
        {
            this.bufLen = bufLen;
            points = new Point3D[bufLen];
        }

        public Point3D add(Point3D p)
        {
            points[position++ % bufLen] = p;
            return getAverage();
        }

        public Point3D getAverage()
        {
            if (position < bufLen)
            {
                return points.Take(position).Average();
            }
            else
            {
                return points.Average();
            }
        }
    }
}
