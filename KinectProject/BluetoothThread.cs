﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Threading;
using InTheHand.Net;
using InTheHand.Net.Sockets;
using InTheHand.Windows.Forms;
using System.Net.Sockets;
using InTheHand.Net.Bluetooth;
using System.Net;
using System.Windows.Forms;

namespace FindSensor
{

    class BluetoothThread
    {
        //buffer for reading stream
        char[] buffer= new char[10];

<<<<<<< HEAD
=======
        BluetoothListener bluetoothListener;
>>>>>>> 2ea4a31cd1c451d2e68b4366ac9c640615c24a86

        public void Start()
        {
      

            // opens window with bluetooth device selection and gets deviceadress
            var dlg = new SelectBluetoothDeviceDialog();
            DialogResult result = dlg.ShowDialog();
            if (result != DialogResult.OK)
            {
                return;
            }
            BluetoothDeviceInfo device = dlg.SelectedDevice;
            BluetoothAddress addr = device.DeviceAddress;

            System.Diagnostics.Debug.WriteLine(addr);

            //set up bluetoothclient and enddevice
            Guid serviceClass;
            serviceClass = BluetoothService.SerialPort;
            var ep = new BluetoothEndPoint(addr, serviceClass);
            var cli = new BluetoothClient();

            System.Diagnostics.Debug.WriteLine(device.Authenticated);

            //set authentificaton pin
            cli.SetPin(addr, "0005");

            System.Diagnostics.Debug.WriteLine(cli.Connected);

            cli.Connect(addr,serviceClass);

            System.Diagnostics.Debug.WriteLine(cli.Connected);

            //get datastream
            Stream peerStream = cli.GetStream();

            //
            StreamWriter sw = new StreamWriter(peerStream);
            StreamReader sr = new StreamReader(peerStream);

            //test burstmode
            sw.Write("@");

            sw.Flush();

            sr.ReadBlock(buffer,0,9);

            sw.Close();

            sr.Close();


            cli.Close();
            cli.Dispose();
            ep = null;
            
        }



    }
}
