﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindSensor
{
    class SensorMain
    {
        public SensorMain()
        {

        }

        public void Start()
        {
            var btTrhead = new BluetoothThread();
            btTrhead.Start();
        }

        public void Stop()
        {

        }

        public void LogPosition(Point3D currentPosition)
        {

        }

        public MagField GetCurrentMagField()
        {
            return new MagField();
        }

        public MagDataHistory GetHistory()
        {
            return new MagDataHistory();
        }
    }
}
