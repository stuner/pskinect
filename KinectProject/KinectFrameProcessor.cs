﻿using Coding4Fun.Kinect.Wpf;
using KinectWPFOpenCV;
using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace FindSensor
{
    /// <summary>
    /// Bearbeitet und speichert die Rohdaten des Kinect Sensors
    /// </summary>
    class KinectFrameProcessor
    {
        #region internal state variables
        private bool dataSet;
        private bool processed;
        private long capturedAt;
        #endregion


        # region variables and data fields for calculation
        private int upSetY;
        private int upSetZ;
        private int elevationAngle;
        private ColorLayer colorLayer;


        private int minDepth;
        private int maxDepth;
        private DepthImagePixel[] rawDepthPixels;
        private byte[] rawDepthColorPixels;
        #endregion


        # region output variables
        public WriteableBitmap rawDepthBitmap { get; private set; }
        public Bitmap depthFiltered { get; private set; }

        public Bitmap rawColorBitmap { get; private set; }
        public Bitmap colorsWithRectangles { get; private set; }
        public Bitmap colorsFiltered { get; private set; }

        public Rectangle[] detectedRectangles { get; private set; }
        public IEnumerable<Point3D> detectedObjects { get; private set; }
        #endregion

        public KinectFrameProcessor(KinectSensor sensor)
        {
            processed = false;
            dataSet = false;

            // allocate space for depth data
            rawDepthPixels = new DepthImagePixel[sensor.DepthStream.FramePixelDataLength];
            rawDepthBitmap = new WriteableBitmap(sensor.DepthStream.FrameWidth, sensor.DepthStream.FrameHeight, 96.0, 96.0, PixelFormats.Gray8, null);
            rawDepthColorPixels = new byte[sensor.DepthStream.FramePixelDataLength];
        }

        private void convertDepthPixelsToColorBitmap()
        {
            int colorPixelIndex = 0;
            for (int i = 0; i < this.rawDepthPixels.Length; ++i)
            {
                int depth = rawDepthPixels[i].Depth;

                int intensity = (byte)((depth * -255 + (minDepth * 255)) / (minDepth - maxDepth));

                if (intensity > 255)
                    intensity = 255;

                if (intensity < 0)
                    intensity = 0;

                //byte intensity = (byte)(depth >> 4);
                //byte intensity = (byte)(depth >= minDepth && depth <= maxDepth ? depth : 0);

                rawDepthColorPixels[colorPixelIndex++] = (byte)intensity;
            }

            rawDepthBitmap.WritePixels(
                new Int32Rect(0, 0, rawDepthBitmap.PixelWidth, rawDepthBitmap.PixelHeight),
                rawDepthColorPixels,
                rawDepthBitmap.PixelWidth,
                0);
        }

        // Daten für processFrame speichern
        public bool setFrameData(ColorImageFrame colorFrame, DepthImageFrame depthFrame, int upSetY, int upSetZ, int elevationAngle, ColorLayer colorLayer)
        {
            if (colorFrame == null || depthFrame == null)
                return false;

            capturedAt = DateTime.UtcNow.Ticks;
            
            this.upSetY = upSetY;
            this.upSetZ = upSetZ;
            this.elevationAngle = elevationAngle;
            this.colorLayer = colorLayer;

            // reset state and output
            processed = false;
            dataSet = true;
            detectedObjects = null;
            detectedRectangles = null;

            // copy color data
            rawColorBitmap = colorFrame.ToBitmapSource().ToBitmap();
            
            // copy depth data
            depthFrame.CopyDepthImagePixelDataTo(rawDepthPixels);
            minDepth = depthFrame.MinDepth;
            maxDepth = depthFrame.MaxDepth;
            
            return true;
        }

        // Objekte finden, Bitmaps aktualisieren
        public void processFrame()
        {
            // Mehrfachausführung / Ausführung ohne Argumente verhindern
            if (processed == true || dataSet == false)
            {
                throw new InvalidOperationException();
            }

            // process depth data to bitmap
            convertDepthPixelsToColorBitmap();

            // Filter Color, find Position of Objects/Rectangles
            detectedRectangles = colorLayer.findLocation(rawColorBitmap);

            // LINQExtension verwenden um Mitte des Rechtecks zu erkennen, dann Koordinaten transformieren
            detectedObjects = detectedRectangles.to3dPoints(transformation);

            // Bitmaps für Darstellung speichern
            colorsFiltered = colorLayer.filteredColorBitmap;
            colorsWithRectangles = rawColorBitmap.drawRectangles(detectedRectangles, System.Drawing.Color.Red);
            depthFiltered = colorLayer.filteredDepthBitmap;

            processed = true;
        }

        private Point3D transformation(System.Drawing.Point point)
        {
            int CAM_PIXEL_WIDTH = rawColorBitmap.Width;
            int CAM_PIXEL_HEIGHT = rawColorBitmap.Height;

            // Camera opening angles
            const double CAM_WIDTH_ANGLE = 32.2;
            const double CAM_HEIGHT_ANGLE = 21.5;

            int zc = rawDepthPixels[point.X + point.Y * CAM_PIXEL_WIDTH].Depth;

            //Umformung von xc in Meter, ab dem Kinect gemessen :
            //den Ursprung von xc und yc wird an der Hälfte der Breite bzw. der Höhe gesetzt und in double umgewandelt
            double x = CAM_PIXEL_WIDTH / 2 - point.X;
            double y = CAM_PIXEL_HEIGHT / 2 - point.Y;
            double z = zc;

            // x und y beschreiben xc und yc den Abstand zur Axis des Kinects
            x = (x / CAM_PIXEL_WIDTH) * 2 * zc * System.Math.Tan(CAM_WIDTH_ANGLE * Math.PI / 180);
            y = (y / CAM_PIXEL_HEIGHT) * 2 * zc * System.Math.Tan(CAM_HEIGHT_ANGLE * Math.PI / 180);

            // Jetzt bestimmen wir die Koordinaten des Punktes bezüglich eines willkürlich gewählten Koordinaten Systems (xp,yp,zp)
            double xp = x;
            double yp = z;
            double zp = -y;

            // Jetzt kommt die Drehung dazu um den Winkel alpha (ElevationAngle of the Kinect)
            double cosAlpha = System.Math.Cos(elevationAngle * Math.PI / 180);
            double sinAlpha = System.Math.Sin(elevationAngle * Math.PI / 180);

            yp = yp * cosAlpha + zp * sinAlpha;
            zp = zp * cosAlpha - yp * sinAlpha;

            return new Point3D(-xp, yp - upSetY, -zp + upSetZ);
        }
    }
}
