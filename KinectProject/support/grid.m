n = 20;

dn = 5;
tol = 2;

[X, Y] = meshgrid(-n/2:n/2-1, -n/2:n/2-1);

size(X)


G = [reshape(X, [1, n ^ 2]); reshape(Y, [1, n ^ 2])];

delta = [
    abs(rem(abs(G(1, :) - dn/2), dn) - dn/2)
    abs(rem(abs(G(2, :) - dn/2), dn) - dn/2)
   ];

delta_abs = sqrt(sum(delta .^ 2, 1))

G_ = G(:, delta_abs < tol)

figure(1)
scatter(G_(1,:), G_(2,:), '.')

col_ind = [
    abs(rem(abs(G(1, :) - dn/2), dn) - dn/2)
    abs(rem(abs(G(2, :) - dn/2), dn) - dn/2)
   ];

figure(2)
scatter(G(1, :), G(2, :), 5, sqrt(sum(delta_abs .^ 2, 1)))

G_grid_coors = G ./ dn
texts = arrayfun(@(a, b) sprintf('%.0f, %.0f', a, b), G_grid_coors(1,:), G_grid_coors(2,:), 'UniformOutput', false)
text(G(1,:), G(2,:), texts)
