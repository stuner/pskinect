﻿namespace FindSensor
{
    using System;

    public class Distance3D
    {
        public Distance3D(double X, double Y, double Z)
        {
            this.X = X;
            this.Y = Y;
            this.Z = Z;
        }

        public Distance3D(int X, int Y, int Z)
        {
            this.X = (double)X;
            this.Y = (double)Y;
            this.Z = (double)Z;
        }

        public bool invalid { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }

        public string toString()
        {
            return "dx : " + X + " dy: " + Y + " dz: " + Z;
        }

        public string toStringCm()
        {
            return String.Format("dx: {0:#00}cm ", X / 10) +
                String.Format("dy: {0:#00}cm ", Y / 10) +
                String.Format("dz: {0:#00}cm ", Z / 10) +
                String.Format("abs: {0:#00}cm ", Abs() / 10);
        }

        public double Abs()
        {
            return Math.Sqrt(X * X + Y * Y + Z * Z);
        }
    }
}
