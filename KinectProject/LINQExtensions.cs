﻿namespace FindSensor
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;

	public static class LINQExtensions
	{
        public static IEnumerable<Point3D> to3dPoints(this IEnumerable<Rectangle> rects, Func<System.Drawing.Point, Point3D> transformationFunc)
        {
            var centers = from r in rects
                          select
                              new System.Drawing.Point(r.X + r.Width / 2, r.Y + r.Height / 2);

            return from p in centers
                         select
                             transformationFunc(p);
        }

        public static string toString(this IEnumerable<Point3D> points)
        {
            if (points.Count() == 0)
                return ""; 
            
            return string.Join("\r\n", from p in points
                                         select
                                             p.toStringCm());
        }

		public static double StdDev(this IEnumerable<int> values)
		{
			double ret = 0;
			int count = values.Count() - 1;
			if (count > 1)
			{
				//Compute the Average
				double avg = values.Average();

				//Perform the Sum of (value-avg)^2
                double sum = values.Select(d => (d - avg) * (d - avg)).Sum();

				//Put it all together
				ret = Math.Sqrt(sum / count);
			}
			return ret;
		}

		public static double StdDev(this IEnumerable<double> values)
		{
			double ret = 0;
			int count = values.Count() - 1;
			if (count > 1)
			{
				//Compute the Average
				double avg = values.Average();

				//Perform the Sum of (value-avg)^2
                double sum = values.Select(d => (d - avg) * (d - avg)).Sum();

				//Put it all together
                ret = Math.Sqrt(sum / count);
			}
			return ret;
		}

		public static Tuple<double[], double[], double[]> extractCoordinatesAsArrays(this IEnumerable<Point3D> values) {
			return Tuple.Create(
				(from p in values select p.X).ToArray(),
				(from p in values select p.Y).ToArray(),
				(from p in values select p.Z).ToArray()
			);
		}

		/// <summary>
		/// delete the measured values which are out of range (range average + 2*standard deviation)
		/// and return an array of int with corrected averages
		/// </summary>
		/// <param name="valuesX"></param>
		/// <param name="valuesY"></param>
		/// <param name="valuesZ"></param>
		/// <returns></returns>
		public static Point3D correctedAverage(this IEnumerable<Point3D> valuesP3)
		{
			if (valuesP3.Count() < 1)
			{
				return new Point3D();
			}

			var values = valuesP3.extractCoordinatesAsArrays();
			var dev = new Point3D(
				values.Item1.StdDev(),
				values.Item2.StdDev(),
				values.Item3.StdDev()
			);
			var avg = new Point3D(
				values.Item1.Average(),
				values.Item2.Average(),
				values.Item3.Average()
			);

			var corr = Tuple.Create(
				new List<double>(),
				new List<double>(),
				new List<double>()
			);

			var max = new Point3D(
				avg.X + 2 * dev.X,
				avg.Y + 2 * dev.Y,
				avg.Z + 2 * dev.Z
			);

			var min = new Point3D(
				avg.X - 2 * dev.X,
				avg.Y - 2 * dev.Y,
				avg.Z - 2 * dev.Z
			);

			foreach (var p in valuesP3)
			{
				if (p.X < max.X && p.X > min.X && p.Y < max.Y && p.Y > min.Y && p.Z < max.Z && p.Z > min.Z)
				{
					corr.Item1.Add(p.X);
					corr.Item2.Add(p.Y);
					corr.Item3.Add(p.Z);
				}
			}

            if (corr.Item1.Count() < 1)
			{
				return new Point3D();
			}

            Point3D ret = new Point3D(corr.Item1.Average(), corr.Item2.Average(), corr.Item3.Average());

			return ret;
		}

        public static Point3D Average(this IEnumerable<Point3D> valuesP3)
        {
            if (valuesP3.Count() < 1)
            {
                return new Point3D();
            }

            var values = valuesP3.extractCoordinatesAsArrays();
            
            return new Point3D(
                values.Item1.Average(),
                values.Item2.Average(),
                values.Item3.Average()
            );
        }
	}
}
